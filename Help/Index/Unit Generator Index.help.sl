# Unit Generator Index

## Analysis

- `Changed`
- `Fft`, `Ifft`
- `Amplitude`, `PeakFollower`
- `Pitch`, `ZeroCrossing`
- `Slope`

## Buffers

- `BufFrames`, `BufRd`, `BufWr`

## Butterworth Filters

- `Bpf`, `Brf`, `Hpf`, `Lpf`

## Comparing

- `Max`, `Min`

## Converting

- `AmpDb`, `DbAmp`
- `CpsMidi`, `MidiCps`
- `CpsOct`, `OctCps`
- `MidiRatio`, `RatioMidi`
- `K2A`

## Delay

- `AllpassC`, `AllpassL`, `AllpassN`
- `CombC`, `CombL`, `CombN`
- `Delay1`, `Delay2`
- `DelayC`, `DelayL`, `DelayN`
- `DelayTap`, `DelayWrite`

## Demand Rate

- `Demand`, `Duty`
- `Dbrown`, `Dbrowni`, `Dwhite`
- `Drand`, `Dseq`, `Dseries`
- `Dswitch`, `Dswitch1`

## Dynamics

- `AmpComp`
- `Compander`, `Limiter`, `Normalizer`

## Envelopes

- `Decay`, `Decay2`
- `EnvGen`
- `Line`, `XLine`

## Filters

- `Fos`, `Sos`

## Fixed Filters

- `Bpz2`, `Brz2`, `Hpz1`, `Hpz2`, `Lpz1`, `Lpz2`

## Gates

- `CoinGate`

## Granular

- `GrainBuf`
- `GrainFm`

## Input and Output

- `In`, `Out`
- `ControlIn`, `ControlOut`
- `AudioIn`
- `LocalIn`, `LocalOut`

## Math Operators

- `Abs`, `AbsDif`, `AmClip`
- `Clip`, `Fold`, `Wrap`
- `Integrator`
- `LinExp`, `LinLin`
- `ModDif`
- `Neg`
- `Pow`
- `RunningMax`, `RunningMin`, `RunningSum`
- `Slope`, `Sqrt`, `Sum`

## Noise Generators

- `Dust`, `Dust2`
- `BrownNoise`, `ClipNoise`, `GrayNoise`, `PinkNoise`, `WhiteNoise`

## Panning

- `Balance2`
- `BfDecode1`, `BfEncode1`
- `EqPan`, `EqPan2`, `Pan2`
- `PanAz`
- `Splay`, `Splay2
- `Mix`
- `DecodeB2`, `PanB2`
- `Rotate2`

## Resonant Filters

- `Formlet`, `Resonz`, `Rhpf`, `Rlpf`, `Ringz`
- `Dfm1`, `MoogFf`

## Rounding and Truncating

- `Ceiling`, `Floor`

## Smoothing Filters

- `Lag`, `Lag2`, `Lag3`, `LagUd`

## Testing

- `InRange`, `InRect`
- `LeastChange`, `MostChange`

## Trigonometric Functions

- `ArcCos`, `ArcSin`, `ArcTan`
- `Cos`, `Sin`, `Tan`

## Triggers

- `Schmidt`

## Voice Synthesis

- `Formant`, `Formlet`, `Vosim`

* * *

See also: Ugen

Guides: Unit Generators
