# asLargerPowerOfTwo

- _asLargerPowerOfTwo(anInteger)_

Answers a power of two that is not less than _anInteger_.

```
>>> 300.asLargerPowerOfTwo
512

>>> 512.asLargerPowerOfTwo
512
```

* * *

See also: asPowerOfTwo, asSmallerPowerOfTwo, bitShift, highBit, isPowerOfTwo

Categories: Truncation, Rounding
