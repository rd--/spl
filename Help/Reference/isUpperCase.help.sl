# isUpperCase

- _isUpperCase(aCharacter | aString)_

Answer `true` if _aString_ has only upper case characters.

```
>>> 'ASCII'.isUpperCase
true

>>> 'A'.asCharacter.isUpperCase
true
```

* * *

See also: asUpperCase, isLowerCase

References:
_Smalltalk_
5.3.4.10

Categories: Testing, Text
