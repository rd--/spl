# roundToTowardsZero

- _roundToTowardsZero(aNumber, anotherNumber)_

Answer the nearest number to _aNumber_ that is a multiple of _anotherNumber_, rounding towards zero.

```
>>> 0.9.roundToTowardsZero(1)
0

>>> -0.9.roundToTowardsZero(1)
0
```

* * *

See also: rounded, roundTo
