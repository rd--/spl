# asUpperCase

- _asUpperCase(aCharacter | aString)_

Answer a `String` made up from _aString_ whose characters are all uppercase.

```
>>> 'ascii'.asUpperCase
'ASCII'

>>> 'a'.asCharacter.asUpperCase
'A'.asCharacter
```

* * *

See also: asLowerCase, isUpperCase

References:
_Smalltalk_
5.3.4.4

Categories: Converting
