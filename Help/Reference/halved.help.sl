# halved

- _halved(aNumber)_

Answer _aNumber_ divided by two.

At `SmallFloat`:

```
>>> 4.halved
2

>>> -2.halved
-1
```

At `Complex`:

```
>>> 2J0.halved
1J0

>>> 4J2.halved
2J1

>>> 6.4J6.halved
3.2J3
```

Threads over lists:

```
>>> 2:2:8.halved
[1 2 3 4]
```

* * *

See also: /, Number

References:
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/minusco)
