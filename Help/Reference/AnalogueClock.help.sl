# AnalogueClock

A simple analogue clock to demonstrate animated frames and vector graphics.

~~~spl smallKansas
AnalogueClock()
.openIn(system.smallKansas, nil)
~~~

* * *

See also: DigitalClock, SmallKansas, WorldMenu

Categories: Kansas
