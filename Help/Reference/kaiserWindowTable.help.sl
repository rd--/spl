# kaiserWindowTable

- _kaiserWindowTable(anInteger, alpha=3)_

Answer a `List` describing a _Kaiser window_ of the indicated size.

~~~spl svg=A
128.kaiserWindowTable(3).linePlot
~~~

![](sw/spl/Help/Image/kaiserWindowTable-A.svg)

* * *

See also: hammingWindowTable, hannWindowTable, kaiserWindow, welchWindowTable

Guides: Window Functions
