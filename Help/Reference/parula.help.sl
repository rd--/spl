# parula

- _parula(anInteger)_

Answer the _parula_ colormap as a three-column matrix with _anInteger_ number of rows.
Each row in the matrix contains the red, green, and blue intensities for a specific color.
The intensities are in the range _(0,1)_.

Plot the _parula_ colour gradient sampled at sixteen steps:

~~~spl svg=A
16.parula.asContinuousColourGradient
~~~

![](sw/spl/Help/Image/parula-A.svg)

* * *

See also: asContinuousColourGradient, ColourGradient, colourGradients

Guides: Colour Guides

References:
_Mathworks_
[1](https://au.mathworks.com/help/matlab/ref/parula.html)
