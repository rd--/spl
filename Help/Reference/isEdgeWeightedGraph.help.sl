# isEdgeWeightedGraph

- _isEdgeWeightedGraph(aGraph)_

Answer `true` if _aGraph_ is an edge weighted graph.

* * *

See also: edgeWeights, Graph, weightedAdjacencyGraph

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/EdgeWeightedGraphQ.html)
