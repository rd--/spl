# after

- _after(aCollection, anObject)_

Answer the element after _anObject_.
Raise an error if _anObject_ is not in the _aCollection_, or if there are no elements after it.

```
>>> 1:9.after(4)
5
```

* * *

See also: afterIfAbsent

References:
_Smalltalk_
5.7.8.3
