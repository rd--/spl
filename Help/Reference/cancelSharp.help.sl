# cancelSharp

- _cancelSharp(aNumber)_

Answer _aNumber - 0.1_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 4.cancelSharp
3.9
```

Where supported `cancelSharp` is displayed as ♮.

* * *

See also: degreeToKey, flat, Scale, sharp

Unicode: U+266E ♮ Music Natural Sign
