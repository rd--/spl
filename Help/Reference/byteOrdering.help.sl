# byteOrdering

- _byteOrdering(aSystem)_

Answer -1 if _aSystem_ is Little-endian, else +1.

```
>>> system.byteOrdering
-1
```

* * *

See also: isBigEndian, isLittleEndian

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/ByteOrdering.html),
_W_
[1](https://en.wikipedia.org/wiki/Endianness)
