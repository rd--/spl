# hours

- _hours(aDuration)_

Answer the number of complete hours in the _aDuration_.

```
>>> 180.minutes.hours
3

>>> 3.days.hours
72
```

* * *

See also: Duration, milliseconds, minutes, seconds

References:
_Smalltalk_
5.8.2.11

Categories: Time, Type
