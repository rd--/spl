# origin

- _origin(aUrl)_

Answer a `String` having the canonical form of the origin of the Url.

```
>>> 'http://w3c.org/standards'
>>> .asUrl
>>> .origin
'http://w3c.org'
```

* * *

See also: Location, Url

Categories: Network
