# addFirst

- _addFirst(aSequence, newObject)_

Add _newObject_ to the beginning of _aSequence_.
Answer _newObject_.

```
>>> let l = [3];
>>> l.addFirst(2);
>>> l.addFirst(1) = 1 & { l = [1 2 3] }
true
```

* * *

See also: add, addLast

References:
_Smalltalk_
5.7.18.12

Categories: Adding
