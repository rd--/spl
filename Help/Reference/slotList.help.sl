# slotList

- _slotList(anObject)_

Answer an `Association` `List` giving the name and value of each slot at _anObject_.

```
>>> 2/3.slotList
['numerator' -> 2, 'denominator' -> 3]
```

* * *

See also: Association, equalByAtNamedSlots, List, slotNameList

Categories: Reflection
