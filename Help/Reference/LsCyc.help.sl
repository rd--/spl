# LsCyc

- _LsCyc(α)_ ⟹ _LsSeq(α, Infinity)_

Infinite _LsSeq_.

```
>>> LsCyc([LsGeom(1, 3, 3), -1]).next(8)
[1 3 9 -1 1 3 9 -1]
```

* * *

See also: LsGeom, LsSeq

Guides: Patterns and Streams

References:
_Python_
[1](https://docs.python.org/3/library/itertools.html#itertools.cycle)

Categories: Stream
