# isSpace

- _isSpace(aCharacter)_

Answer `true` if _aCharacter_ is a space.

```
>>> ' '.isSpace
true

>>> 32.asCharacter.isSpace
true
```

* * *

See also: Character, isCarriageReturn, isFormFeed, isLineFeed, isSeparator, isTab

Categories: Testing
