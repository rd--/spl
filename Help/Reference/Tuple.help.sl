# Tuple

A `Type` holding an _n_-tuple.

An _n_-tuple is an ordered heterogeneous set of _n_ elements.

There is a syntax for writing tuples, _(p, q) ⇒ [p, q].asTuple_ &etc.

* * *

See also: List

Guides: List Syntax, Tuple Syntax

Categories: Collection, Type
