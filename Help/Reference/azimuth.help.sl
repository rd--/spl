# azimuth

- _azimuth(aPoint)_

Another name for `theta`:

```
>>> SphericalCoordinates(1, 1.pi, 0).azimuth
1.pi
```

* * *

See also: theta

Guides: Geometry Types
