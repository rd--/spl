# hannWindowTable

- _hannWindowTable(anInteger)_

Answer a `List` describing a _Hann window_ of the indicated size.

~~~spl svg=A
128.hannWindowTable.linePlot
~~~

![](sw/spl/Help/Image/hannWindowTable-A.svg)

* * *

See also: hammingWindowTable, hannWindow, kaiserWindowTable, welchWindowTable

Guides: Window Functions

Categories: Windowing
