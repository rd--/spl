# perpendicularBisector

- _perpendicularBisector(aLine)_
- _perpendicularBisector(aPoint, anotherPoint)_

Answer the perpendicular bisector of the line segment connecting two points.

```
>>> [-1 -1].perpendicularBisector([1 1])
InfiniteLine([0, 0], [2 -2])
```

* * *

See also: InfiniteLine, Line, midpoint

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PerpendicularBisector.html)
