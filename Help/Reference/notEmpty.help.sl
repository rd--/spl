# notEmpty

- _notEmpty(aCollection)_

Answers whether the receiver contains any elements.
Negation of _isEmpty_.

At `List`:

```
>>> [].notEmpty
false
```

At `Record`:

```
>>> ().notEmpty
false
```

At `String`:

```
>>> ''.notEmpty
false
```

* * *

See also: isEmpty, size

References:
_Smalltalk_
5.7.1.18

Categories: Testing
