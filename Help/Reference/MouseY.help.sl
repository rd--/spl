# MouseY

- _MouseY(minval=0, maxval=1, warp=0, lag=0.2)_

Cursor unit generator.

Same as `MouseX`, but for Y axis.

* * *

See also: MouseButton, MouseX

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/MouseY.html)

Categories: Ugen
