# indexValueAssociations

- _indexValueAssociations(aSequence)_

Answer a sequence containing the (index -> value) associations of _aSequence_.

```
>>> 3:7.indexValueAssociations
[1 -> 3, 2 -> 4, 3 -> 5, 4 -> 6, 5 -> 7]
```

* * *

See also: associations, Sequence

Categories: Accessing
