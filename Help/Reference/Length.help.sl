# Length

Unit type.
A type representing a unit of measure for lengths.
The metre (m) is the SI unit of length.

There are accessors for `picometres`, `millimetres`, `centimetres`, `metres` and `kilometres`.

* * *

See also: Angle, asMetres, Duration, Frequency, metres

Categories: Unit, Type
