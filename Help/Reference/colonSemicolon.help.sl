# :; (colonSemicolon)

:; is a syntax token, it is not an operator.

:; separates _matrix_ items in `Volume Syntax`:

```
>>> [1 2; 3 4:; 5 6; 7 8]
[[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
```

The name of this syntax token is `colonSemicolon`.

Where supported, `:;` is displayed as ↲.

* * *

See also: ;, :

Guides: Syntax Tokens, Volume Syntax

Unicode: ↲ U+021B2 Downwards Arrow With Tip Leftwards

Categories: Syntax
