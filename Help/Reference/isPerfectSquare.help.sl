# isPerfectSquare

- _isPerfectSquare(anInteger)_

Answer `true` if _anInteger_ is a perfect square.

A perfect square is an `Integer` that is the square of an integer.

The perfect squares (A000290 in OEIS) smaller than 200:

```
>>> 0:200.select(isPerfectSquare:/1)
[
	0 1 4 9 16 25 36 49 64 81
	100 121 144 169 196
]
```

* * *

See also: squared, sqrt

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PerfectSquare.html),
_OEIS_
[1](https://oeis.org/A000290),
_W_
[1](https://en.wikipedia.org/wiki/Square_number)

Categories: Testing, Math
