# isNil

- _isNil(anObject)_

True if _anObject_ is _nil_ else false.

```
>>> nil.isNil
true

>>> ().isNil
false
```

* * *

See also: Nil, notNil

References:
_Smalltalk_
5.3.1.13

Categories: Testing
