# micro

- _micro(aNumber)_

Answer _aNumber_ times 1E-6.
Micro is a unit prefix in the metric system denoting a factor of one millionth.
It comes from the Greek word μικρός (mikrós), meaning "small".
It is part of the International System of Units (SI).

```
>>> 23.micro
0.000023

>>> 1.micro * 1E6
1
```

* * *

See also: centi, deci, deca, hecto, giga, kilo, mega, milli, nano, pico, terra

Guides: SI Units

Unicode: U+03BC μ Greek Small Letter Mu

References:
_W_
[1](https://en.wikipedia.org/wiki/Micro-)
