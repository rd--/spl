# TranscriptViewer

A text viewer that displays the messages written to the `Transcript`.

* * *

See also: SmallKansas, Transcript

Categories: Kansas
