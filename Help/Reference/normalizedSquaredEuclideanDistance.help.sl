# normalizedSquaredEuclideanDistance

- _normalizedSquaredEuclideanDistance(u, v)_

Answer the normalizedSquared Euclidean distance between vectors _u_ and _v_.

Normalized squared Euclidean distance between numeric vectors:

```
>>> [1 2 3].normalizedSquaredEuclideanDistance(
>>> 	[3 5 10]
>>> )
1/4

>>> [1 5 2 3 10]
>>> .normalizedSquaredEuclideanDistance(
>>> 	[4 15 20 5 5]
>>> )
0.5833
```

* * *

See also: euclideanDistance, minkowskiDistance, norm, squaredEuclideanDistance

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/NormalizedSquaredEuclideanDistance.html),

Categories: Distance
