# afterIfAbsent

- _afterIfAbsent(aCollection, anObject, aBlock:/0)_

Answer the element after _anObject_,
or the result of _aBlock()_ if _anObject_ is not present or if there are no elements after it.

```
>>> 1:9.afterIfAbsent(9) { true }
true
```

* * *

See also: after

References:
_Smalltalk_
5.7.8.3
