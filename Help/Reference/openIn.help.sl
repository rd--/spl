# openIn

- openIn(self, smallKansas, event)_

The required method of the trait `SmallKansan`.
It is how the system requests that a small Kansan open itself,
in _smallKansas_,
in response to _event_.

* * *

See also: SmallKansas

Categories: Kansas
