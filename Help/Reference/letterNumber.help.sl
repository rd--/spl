# letterNumber

- _letterNumber(aString, alphabet)_

The fourth letter in the English alphabet is d:

```
>>> 'd'.letterNumber('english')
4
```

`letterNumber` works the same with uppercase letters:

```
>>> 'D'.letterNumber('english')
4
```

The third letter in the Greek alphabet is γ:

```
>>> 'γ'.letterNumber('greek')
3
```

Get positions of each of the letters in a string:

```
>>> 'turtle'
>>> .characters
>>> .letterNumber('english')
[20 21 18 20 12 5]
```

* * *

See also: alphabet, indexOf, String
