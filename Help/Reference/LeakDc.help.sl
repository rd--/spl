# LeakDc

- _LeakDc(in, coef=0.995)_

Remove Dc offset from a signal.

- in: input signal
- coef: leak coefficient

Add Dc to a pulse wave and then remove it:

```
let a = LfPulse(800, 0, 0.5) * 0.5 + 0.5;
[a * 0.1, LeakDc(a, 0.995) * 0.1]
```

* * *

See also: Dc

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/LeakDC.html)

Categories: Ugen
