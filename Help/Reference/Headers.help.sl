# Headers

A `Type` holding  Http request and response headers.

Implements a subset of the `Dictionary` protocol:
`asRecord`, `at`, `atIfAbsent`, `atPut`, `includesKey`, `removeKey`.

Key queries are case insensitive.

* * *

See also: asHeaders, Response

Categories: Network
