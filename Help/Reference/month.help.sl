# month

- _month(aDate)_

Answer the month of _aDate_.

```
>>> 0.asDate.month
1

>>> '2024-03-04'.parseDate.month
3
```

* * *

See also: asDate, Date, dayOfMonth, minute, parseDate, year

References:
_Smalltalk_
5.8.1.19

Categories: Time, Type
