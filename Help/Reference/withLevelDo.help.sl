# withLevelDo

- _withLevelDo(anIterable, aBlock:/2)_

Apply _aBlock_ to each item in _anIterable_ with its level as the second argument.

* * *

See also: level, levelBy, withLevelCollect
