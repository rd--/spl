# absMax

- _absMax(aCollection)_

The maximum absolute value in _aCollection_.
The collection must be non-empty and contain compatible Magnitudes.

```
>>> let c = -9:5;
>>> (c.max, c.absMax)
(5, 9)
```

* * *

See also: abs, max

Categories: Testing
