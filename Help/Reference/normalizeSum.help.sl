# normalizeSum

- _normalizeSum(aCollection)_

Answer the collection divided by its `sum`,
so that the answer will `sum` to `one`.

```
>>> [1 2 3].normalizeSum
[1/6 1/3 1/2]
```

* * *

See also: normalize, normalizeRange, normalizeSignal, rescale

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Array.html#-normalizeSum)

Categories: Collection
