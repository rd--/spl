# SmallKansas

`SmallKansas` is a `Type` implementing a simple windowing system.
It is modelled on the Smalltalk-80 system and runs inside a _Web Browser_.

* * *

See also: SmallKansan

Categories: Kansas
