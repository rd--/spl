# MidiPortBrowser

A `ColumnBrowser` where:

- the first column select input or output ports
- the second column selects the _manufacturer_
- the third column selects the port name

The selected port is described in the text pane.

* * *

See also: MidiMonitorMenu, SmallKansas

Categories: Kansas
