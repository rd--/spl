# stringReverse

- _stringReverse(aString)_

Answer a `String` that reverses the order of the characters in _aString_.

Reverse the characters in a string:

```
>>> 'abcdef'.stringReverse
'fedcba'
```

Threads over lists:

```
>>> ['cat' 'dog' 'fish' 'coelenterate'].stringReverse
['tac' 'god' 'hsif' 'etaretneleoc']
```

* * *

See also: reversed, String

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/StringReverse.html)
