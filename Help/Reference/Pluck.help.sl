# Pluck

- _Pluck(in, trig, maxDelayTime, delayTime, decayTime, coef)_

Karplus-strong algorithm.

- in: an excitation signal
- trig: fill delay line on trigger
- maxDelayTime: the max delay time in seconds (initializes the internal delay buffer)
- delayTime: delay time in seconds
- decayTime: time for the echoes to decay by 60 decibels, negative times emphasize odd partials
- coef: the coef of the internal `OnePole` filter, values should be between -1 and +1

On _trig_, _n_ samples of the excitation signal are fed into the delay line, where _n = delaytime * SampleRate() / 2_.
The delay line is filled using a rectangular envelope, that is there is no fading.

Excitation signal is `WhiteNoise`, triggered twice a second with varying `OnePole` coef:

```
Pluck(
	WhiteNoise() * 0.1,
	Impulse(2, 0),
	1 / 440,
	1 / 440,
	10,
	MouseX(-0.999, 0.999, 0, 0.2)
)
```

Randomised duplicates:

```
let k = 12;
let freq = SinOsc(
	{ Rand(0.01, 0.15) } ! k,
	{ Rand(0, 1) } ! k
).LinLin(-1, 1, 777, 3333);
Splay(
	LeakDc(
		Pluck(
			WhiteNoise() / 4 # k,
			Impulse({ Rand(3, 11) } ! k, 0),
			1 / 100,
			1 / freq,
			2,
			Rand(0.01, 0.2)
		),
		0.995
	)
)
```

* * *

See also: OnePole, WhiteNoise

References:
_Csound_
[1](https://csound.com/docs/manual/pluck.html),
_SuperCollider_
[1](https://doc.sccode.org/Classes/Pluck.html)

Further Reading: Karplus 1983

Categories: Ugen
