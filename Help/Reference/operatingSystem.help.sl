# operatingSystem

- _operatingSystem(aSystem)_

Answer a `String` indicating the operating system.

```
>>> ['android' 'darwin' 'linux' 'windows']
>>> .includes(
>>> 	system.operatingSystem
>>> )
true
```

* * *

See also: instructionSetArchitecture

Guides: System Functions

Categories: System
