# allButFirstDo

- _allButFirstDo(aSequence, aBlock:/1)_

Apply _aBlock_ at all but the first element of _aSequence_.

* * *

See also: allButLastDo, do

Categories: Accessing
