# replicateInteger

- _replicateInteger(anObject, anInteger)_

Make a `List` having _anInteger_ copies of _anObject_:

```
>>> 'x'.replicateInteger(3)
['x' 'x' 'x']

>>> [1 2 3].replicateInteger(2)
[1 2 3; 1 2 3]
```

* * *

See also: #, !, fill, replicateEach, replicateShape, reshape, shape

Categories: Copying
