# Storage

Storage implements a persistent store for associations where both keys and values are of type `String`.

Storage implements the `Dictionary` protocol.

`System` provides a persistent local store at `localStorage`.

There may also be a per-session persistent local store at `sessionStorage`.

To delete all items from the storage use `removeAll`.

* * *

See also: Dictionary, Indexable, localStorage, sessionStorage, System

Categories: Collection, Type
