# evaluate

- _evaluate(aSystem, aString)_

Compile and execute _aString_ in the context of _aSystem_.

```
>>> system.evaluate('3 + 4')
7

>>> system.evaluate('1.pi ^ 1.e')
22.45915
```

* * *

See also: evaluateNotifying, System

References:
_Tc39_
[1](https://tc39.es/ecma262/multipage/global-object.html#sec-eval-x)

Categories: Evaluating
