# MidiRatio

- _MidiRatio(aNumber)_

Convert an interval in midi notes into a frequency ratio.

Inverse of `RatioMidi`.

```
>>> 12.MidiRatio
2

>>> 7.02.MidiRatio
1.5
```

* * *

See also: MidiCps, RatioMidi

Categories: Arithmetic
