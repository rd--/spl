# MethodSignatureBrowser

A `ColumnBrowser` where the singular column lists the _Method Signatures_ the system knows of.

When a method is selected the text pane shows the definition.

The _Status Text_ shows if the selected value is a `Trait` or a `Type`.

* * *

See also: CategoryBrowser, MethodBrowser, TraitBrowser, TypeBrowser, SystemBrowser

Categories: Kansas
