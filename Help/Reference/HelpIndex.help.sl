# HelpIndex

- _HelpIndex(aList)_

A `Type` holding an index of the Spl help files.
Each entry in the index is a pair _[Kind, Topic]_.

The `helpIndex` method `System` answers a `HelpIndex`:

```
>>> system.helpIndex.typeOf
'HelpIndex'
```

* * *

See also: helpIndex, HelpFile
