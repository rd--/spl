# asCodePoint

- _asCodePoint(aCharacter | anInteger | aString)_

Answer an `Integer` of the Unicode code point given a `Character`, a `String` or an `Integer`.

```
>>> 'c'.asCodePoint
99

>>> 'c'.asCharacter.asCodePoint
99

>>> 99.asCodePoint
99
```

* * *

See also: asCharacter, asString

Categories: Converting
