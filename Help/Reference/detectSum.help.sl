# detectSum

- _detectSum(aCollection, aBlock:/1)_

Evaluate _aBlock_ with each of element of _aCollection_ as the argument and answer their sum.

* * *

See also: detect, sum

Categories: Enumerating
