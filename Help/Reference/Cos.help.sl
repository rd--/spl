# Cos

- _Cos(aNumber)_

Answers the cosine of _aNumber_.

```
>>> 1/6.pi.Cos
(3.sqrt / 2)
```

* * *

See also: cos, Sin, Tan

Categories: Math, Ugen
