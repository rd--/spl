# CombL

- _CombL(in, maxdelaytime=0.2, delaytime=0.2, decaytime=1)_

Comb filter.

* * *

See also: AllpassL, CombC, CombN

Categories: Ugen, Filter
