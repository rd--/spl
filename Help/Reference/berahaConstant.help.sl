# berahaConstant

- _berahaConstant(anInteger)_

The first ten Beraha constants:

```
>>> 1:10.collect(berahaConstant:/1)
[4 0 1 2 2.618 3 3.247 3.414 3.532 3.618]
```

Notable values:

```
>>> 5.berahaConstant
1.goldenRatio + 1

>>> 7.berahaConstant
1.silverConstant

>>> 10.berahaConstant
1.goldenRatio + 2
```

* * *

See also: goldenRatio, silverConstant

References:
_Mathematica_
[1](https://mathworld.wolfram.com/BerahaConstants.html),
_W_
[1](https://en.wikipedia.org/wiki/Beraha_constants)

Categories: Math, Constant
