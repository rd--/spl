# cancel

- _cancel(anInteger)_

Cancel the scheduled activity associated with _anInteger_.
Answers `nil`.
If no scheduled activity is associated with _anInteger_ do nothing.

* * *

See also: valueAfter, valueAfterWith, valueEvery

Categories: Scheduling
