# Divide

- _Divide(aNumber, anotherNumber)_

Answer the division of _aNumber_ and _anotherNumber_.
`Divide` is an alias for `/`.

At `SmallFloat`:

```
>>> Divide(3, 4)
0.75
```

Quiet and quieter pink noise:

```
Divide(
	PinkNoise(),
	[10 25]
)
```

* * *

See also: /, +, *, +, Minus, Plus, Times

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/Divide.html)

Categories: Ugen
