# asNumericArray

- _asNumericArray(aNumericArray)_

Answer a value of type `NumericArray` representing the matrix at _aNumericArray_.

A 2×2 matrix from a nested `List`:

```
>>> [1 2; 3 4].asNumericArray.shape
[2 2]
```

A 3×2×3 array from a nested `List`:

```
>>> [3 2 3].iota.asNumericArray.shape
[3 2 3]
```

* * *

See also: List, NumericArray

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/NumericArray.html)

Categories: Converting
