# metres

- _metres(aNumber)_

Answer a `Length` representing _aNumber_ metres.

```
>>> 3.metres.isLength
true

>>> 3.metres.centimetres
300
```

* * *

See also: centimetres, Length
