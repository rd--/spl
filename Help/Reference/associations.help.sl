# associations

- _associations(aDictionary)_

Answer a `List` of the `Association` values held by _aDictionary_.

```
>>> (x: 1, y: 2, z: 3).associations
['x' -> 1, 'y' -> 2, 'z' -> 3]

>>> ().associations
[]
```

* * *

See also: ->, associationsDo, Association, Dictionary, indexValueAssociations, keys, values

Categories: Accessing
