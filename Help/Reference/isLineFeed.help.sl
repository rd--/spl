# isLineFeed

- _isLineFeed(aCharacter)_

Answer `true` if _aCharacter_ is a line feed (also called a new line).

```
>>> '\n'.isLineFeed
true

>>> 10.asCharacter.isLineFeed
true
```

* * *

See also: Character, isCarriageReturn, isFormFeed, isSeparator, isSpace, isTab

Categories: Testing
