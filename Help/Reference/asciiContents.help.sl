# asciiContents

- _asciiContents(aStream)_

Read the contents of _aStream_,
which should be of a Ascii encoded `ByteArray`,
and answer a `String` value.

* * *

See also: contents, AsciiStream, asciiString
