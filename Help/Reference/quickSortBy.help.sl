# quickSortBy

- _quickSortBy(aSequence, aBlock:/2)_

Sort _aSequence_ in place using the comparator _aBlock_.
Answers _aSequence_.

At `List`, sort into descending order:

```
>>> let a = [1 7 3 9 5];
>>> a.quickSortBy(>=);
>>> a
[9, 7 .. 1]
```

At `List`, sort into ascending order:

```
>>> [1 7 3 9 5].quickSortBy(<=)
[1, 3 .. 9]
```

* * *

See also: mergeSortBy, quickSort, sortBy

Categories: Sorting
