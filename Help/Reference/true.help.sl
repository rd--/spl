# true

The constant value inidicating the `Boolean` value `true`.

```
>>> 3 + 4 = 7
true
```

The print string of `true` is _true_:

```
>>> true.printString
'true'
```

Where supported `true` is displayed as ⊤.

`true` is a reserved word, like `nil` and `false`.

* * *

See also: Boolean, false, not

Unicode: U+22A4 ⊤ Down tack

Categories: Boolean, Constant
