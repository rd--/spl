# speedOfLight

- _speedOfLight(aNumber)_

Answer _aNumber_ times the speed of light,
given in metres per second (_m/s_).

The speed of light,
denoted by _c_,
is a fundamental physical constant.

```
>>> 1.speedOfLight
299792458
```

* * *

See also: e, goldenRation, pi, planckConstant

References:
_W_
[1](https://en.wikipedia.org/wiki/Speed_of_light)
