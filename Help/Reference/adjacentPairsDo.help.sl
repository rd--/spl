# adjacentPairsDo

- _adjacentPairsDo(aSequence, aBlock:/2)_

Apply _aBlock_ to the items of _aSequence_ taken two at a time, moving forward one place each turn.
The iterative form of `adjacentPairsCollect`.

* * *

See also: adjacentPairsCollect, do, pairsDo

Categories: Enumerating
