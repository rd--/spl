# addAllLast

- _addAllLast(aSequence, anotherSequence)_

Add all the elements of _anotherSequence_ to the end of _aSequence_.
Answer _anotherSequence_.

```
>>> let l = [1 2 3];
>>> (l.addAllLast([4 5]), l)
([4 5], [1 2 3 4 5])
```

* * *

See also: add, addAll, addAllFirst

References:
_Smalltalk_
5.7.18.11

Categories: Adding
