# NaN

`NaN` is a numeric constant.

`NaN` is, perhaps confusingly, a number:

```
>>> NaN.isNumber
true
```

In particular, NaN` is a `SmallFloat`:

```
>>> NaN.isSmallFloat
true
```

`NaN` is not finite:

```
>>> NaN.isFinite
false
```

* * *

See also: e, Infinity, pi
