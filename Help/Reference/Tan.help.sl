# Tan

- _Tan(aNumber)_

Answers the tangent of _aNumber_.

```
>>> 1/6.pi.Tan
(1 / 3.sqrt)
```

* * *

See also: ArcTan, Cos, Sin, Tanh

Categories: Ugen
