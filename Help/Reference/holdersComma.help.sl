# holdersComma

- _holdersComma(aNumber)_

Answer Holders, also called Holdrians, comma raised to _aNumber_.
It is equal to one step of the 53-EDO tuning.

```
>>> 1.holdersComma
2.nthRoot(53)

>>> 1.holdersComma
1.0132

>>> 1.holdersComma.ratioToCents
22.6415
```

* * *

See also: holdersComma, mercatorsComma, pythagoreanComma, septimalComma, syntonicComma

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/Holdrian_comma)

Categories: Math, Contant, Tuning
