# parseSvg

- _parseSvg(aString)_

Answer an Svg element parsed from _aString_.

* * *

See also: Navigator
