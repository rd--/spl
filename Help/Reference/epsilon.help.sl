# epsilon

- _epsilon(aNumber)_

Mathematical constant.
Answer _aNumber_ times epsilon.

```
>>> 1.epsilon
0.0000000000000010

>>> 2.epsilon
0.0000000000000020
```

At `Fraction`:

```
>>> 1/2.epsilon
0.0000000000000005
```

Where supported `epsilon` is displayed as ε.

* * *

See also: ~, isCloseTo, pi, smallFloatEpsilon, isVeryCloseTo

Guides: Mathematical Constants

References:
_Smalltalk_
5.6.8.5

Unicode: U+03B5 ε Greek Small Letter Epsilon

Categories: Math, Constant
