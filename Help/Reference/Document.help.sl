# Document

A system `Trait`.
Not all systems will implement `Document`.
Where implemented it is accessed using the method `document` at `Window` and `Navigator`.

```
>>> system.window.isWindow
true

>>> system.window.navigator.isNavigator
true
```

* * *

See also: Navigator, Window

Categories: System, UserInterface
