# asRegExp

- _asRegExp(aRegExp | aString)_

Answer a regular expression, compiling _aString_ if required.

```
>>> 'caddr'.matchesRegExp(
>>> 	'c(a|d)+r'.asRegExp
>>> )
true
```

* * *

See also: matches, matchesRegExp, RegExp, String

Categories: Converting
