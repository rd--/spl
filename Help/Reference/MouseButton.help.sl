# MouseButton

- _MouseButton(minval=0, maxval=1, lag=0.2)_

Control input.
Read mouse button state,
_minval_ is the output value when the button is not pressed, and
_maxval_ is the output value when the button is pressed.

* * *

See also: KeyState, MouseX, MouseY

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/MouseButton.html)

Categories: Ugen
