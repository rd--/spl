# isComplex

- _isComplex(anObject)_

Answers `true` if _anObject_ is a `Complex`, else `false`.

```
>>> 3J4.isComplex
true

>>> 3.141.isComplex
false
```

* * *

See also: isNumber, Complex

Categories: Testing, Math
