# Delay2

- _Delay2(in)_

Two sample delay.

- in: sample to be delayed.

Original, with delayed subtracted from original:

```
let z = Dust(1000);
[z, z - Delay2(z)]
```

* * *

See also: Delay1, DelayC, DelayL, DelayN

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Delay2.html)

Categories: Ugen, Delay
