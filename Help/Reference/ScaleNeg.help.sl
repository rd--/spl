# ScaleNeg

- _ScaleNeg(aNumber, anotherNumber)_

Waveshaping.
Scale negative part of input.

```
SinOsc(500, 0).ScaleNeg(Line(1, -1, 7)) / 7
```

* * *

See also: times

Categories: Ugen
