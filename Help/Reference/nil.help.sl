# nil

`nil` represents a prior value for variables that have not been initialized,
or for results which are meaningless.

```
>>> nil.isNil
true
```

`nil` can be copied, and answers `nil`:

```
>>> nil.copy
nil
```

`nil` is a reserved word, like `true` and `false`.

* * *

See also: false, Nil, true, Void

Categories: UndefinedObject, Type, Reserved Word
