# permutationReplace

- _permutationReplace(aSequence, aPermutation)_

Replaces each item in _aSequence_ by its `image` under _aPermutation_.

`permutationReplace` on arrays of integers returns the list of respective images:

```
>>> [1 .. 6].permutationReplace(
>>> 	[1 5 3; 2 7]
>>> )
[5 7 1 4 3 6]
```

* * *

See also: image, Permutation

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/PermutationReplace.html)

Categories: Permutations
