# addFrame

- _addFrame(aSmallKansas, subject, event)_

Add a `Frame` to `SmallKansas`.

* * *

See also: addFrameWithAnimator, SmallKansas

Categories: Kansas
