# Log2

- _Log2(aNumber)_

Base two logarithm.

```
>>> Log2(Power(2, 10))
10
```

The `Log2` of the `Sqrt` of two is one half:

```
>>> 2.Sqrt.Log2
1/2
```

* * *

See also: Exp, Log, Log10, Power, Sqrt

Categories: Ugen
