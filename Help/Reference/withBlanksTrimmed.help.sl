# withBlanksTrimmed

- _withBlanksTrimmed(aString)_

Remove whitespace from both ends of _aString_.

```
>>> '   aaa bbb ccc   '.withBlanksTrimmed
'aaa bbb ccc'
```

* * *

See also: String, trim, withoutLeadingBlanks, withoutTrailingBlanks

Categories: Converting
