# atPut

- _atPut(aCollection, anInteger, anObject)_
- _atPut(c, i, j, v)_ ⇒ _at(c, i).atPut(j, v)_
- _atPut(c, i, j, k, v)_ ⇒ _at(c, i).at(j).atPut(k, v)_

Store _anObject_ at _anInteger_ in _aCollection_.
If the collection does not have indexed variables,
or if the argument is less than one or greater than the number of indexed variables,
then report an error.
Answer _anObject_.

The trait definitions of the extended arity forms are as above.

```
>>> let l = [1 2 3];
>>> (l.atPut(2, 'two'), l)
('two', [1 'two' 3])
```

- _atPut(aCollection, aKey, anObject)_

Associate _anObject_ with _aKey_ in _aCollection_.
If the collection is not a kind of dictionary then report an error.
If the dictionary does not have an entry for _aKey_ create one.
Answer _anObject_.

```
>>> let r = Record();
>>> (r.atPut('x', 1), r.at('x'))
(1, 1)
```

There is `AtPut Syntax` for mutating indexed values:

```
>>> let a = ['x' 'y'];
>>> a[1] := 'z';
>>> a
['z' 'y']

>>> let d = (x: 1, y: 2);
>>> d['x'] := 3;
>>> d
(x: 3, y: 2)
```

* * *

See also: at, atPathPut, size

Guides: AtPut Syntax, Quoted AtPut Syntax

References:
_Smalltalk_
5.7.2.5

Categories: Accessing
