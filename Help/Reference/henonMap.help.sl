# henonMap

- _henonMap(a, b)_

A two-dimensional dissipative quadratic map by Michel Hénon.

~~~spl svg=A
henonMap(1.4, 0.3)
.nestList([0 0], 99)
.scatterPlot
~~~

![](sw/spl/Help/Image/henonMap-A.svg)

* * *

See also: deJongMap, henonAreaPreservingMap, martinMap

References:
_Mathematica_
[1](https://mathworld.wolfram.com/HenonMap.html),
_W_
[1](https://en.wikipedia.org/wiki/H%C3%A9non_map)

Further Reading: Hénon 1976
