# benedettiHeight

- _benedettiHeight(aFraction)_

The `product` of the `numerator` and `denominator`.

```
>>> 81/64.benedettiHeight
5184
```

Threads over lists:

```
>>> [1/1 2/1 3/2 6/5 9/7 13/11].benedettiHeight
[1 2 6 30 63 143]
```

* * *

See also: tenneyHeight

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/Benedetti_height)

Categories: Tuning
