# deepMin

- _deepMin(aCollection)_

Answer `deepReduce` of `min`.

At a 3×3 matrix:

```
>>> [8 1 6; 3 5 7; 4 9 2].deepMin
1
```

* * *

See also: deepMax, deepReduce, min, max, floor, minBy

Categories: Testing
