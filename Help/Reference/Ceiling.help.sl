# Ceiling

- _Ceiling(aNumber)_

Next higher integer.

```
>>> 1.5.Ceiling
2
```

Stepped line:

```
let m = XLine(48, 57, 7);
SinOsc([m, m.Ceiling].MidiCps, 0) * 0.1
```

* * *

See also: ceiling, Floor, RoundTo

References:
_Csound_
[1](https://csound.com/docs/manual/ceil.html)

Categories: Math, Ugen
