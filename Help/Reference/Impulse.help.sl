# Impulse

- _Impulse(freq=440, phase=0)_

Impulse oscillator.

Outputs non band limited single sample impulses.

- freq: frequency in Hertz
- phase: phase offset in cycles (0,1)

Constant frequency:

```
Impulse(800, 0) * 0.2
```

Modulate frequency:

```
Impulse(XLine(800, 100, 5), 0) * 0.2
```

Modulate phase:

```
Impulse(4, [0, MouseX(0, 1, 0, 0.2)]) * 0.2
```

* * *

See also: Blip

References:
_Csound_
[1](https://csound.com/docs/manual/mpulse.html),
_SuperCollider_
[1](https://doc.sccode.org/Classes/Impulse.html)

Categories: Ugen
