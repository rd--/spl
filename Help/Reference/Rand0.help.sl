# Rand0

- _Rand0(α)_ ⟹ _Rand(0, α)_

Random number generator.

* * *

See also: Rand, Rand2

Categories: Ugen
