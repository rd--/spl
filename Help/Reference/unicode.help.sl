# unicode

- _unicode(aFraction)_

If _aFraction_ has a unicode code-point answer it.

```
>>> 3/4.unicode
'¾'

>>> 2/3.unicode
'⅔'

>>> { 11/13.unicode }.ifError { true }
true
```

* * *

See also: Fraction

Categories: Printing
