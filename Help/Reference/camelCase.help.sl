# camelCase

- _camelCase(aList)_

Capitalse all but the first word in a `List`.

```
>>> ['analogue' 'clock'].camelCase
['analogue', 'Clock']
```

* * *

See also: camelCaseToWords
