# isAsciiCodePoint

- _isAsciiCodePoint(anInteger)_

Answers `true` if _anInteger_ is between 0 and 127, else `false`.

```
>>> 'x'.codePoint.isAsciiCodePoint
true
```

* * *

See also: codePoint, isAscii

Categories: Testing, Text
