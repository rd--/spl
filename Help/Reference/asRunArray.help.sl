# asRunArray

- _asRunArray(aListSequence)_

Convert a sequence of _run -> value_ associations into a `RunArray`:

```
>>> 'abbbccccc'.asList.asRunArray.runs
[1 3 5]
```

* * *

See also: RunArray

Categories: Converting
