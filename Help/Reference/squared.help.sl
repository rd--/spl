# squared

- _squared(aNumber)_

Answer _aNumber_ multiplied by itself.

```
>>> (3 * 3, 3 ^ 2, 3.squared)
(9, 9, 9)
```

At `List`:

```
>>> [1 2 3 4].squared
[1 4 9 16]
```

The inverse is `sqrt`:

```
>>> 3.squared.sqrt
3
```

Threads over lists:

```
>>> 1:9.sum.squared
2025
```

Where supported `squared` is displayed as ².

* * *

See also: ^, *, cubed, sqrt

References:
_Smalltalk_
5.6.2.35

Unicode: U+00B2 ² Superscript Two
