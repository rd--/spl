# unitVector

- _unitVector(n, k)_

Answers the _n_-dimensional unit vector in the _k_-th direction.

The unit vector in the _x_ direction in two dimensions:

```
>>> 2.unitVector(1)
[1 0]
```

The unit vector in the _y_ direction in three dimensions:

```
>>> 3.unitVector(2)
[0 1 0]
```

* * *

See also: identityMatrix, kroneckerDelta, List, norm, normalize, rotationMatrix

References:
_Mathematica_
[1](https://mathworld.wolfram.com/UnitVector.html)
[2](https://reference.wolfram.com/language/ref/UnitVector.html)

Categories: Geometry
