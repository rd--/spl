# increment

- _increment(aNumber)_

Answer _aNumber_ plus `one`.

At `SmallFloat`:

```
>>> -5.increment
-4

>>> 0.increment
1
```

At `Complex`:

```
>>> 4J2.increment
5J2
```

Threads over lists:

```
>>> [-5 1 0 5].increment
[-4 2 1 6]
```

* * *

See also: +, Number

References:
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/gtco)
