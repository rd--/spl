# Minus

- _Minus(aNumber, anotherNumber)_

`Minus` is an alias for the binary operator `-`,
which is named `hyphenMinus`.

At `SmallFloat`:

```
>>> Minus(3, 4)
-1

>>> 3 - 4
-1

>>> hyphenMinus(3, 4)
-1
```

It answers the sum of _aNumber_ and the negation of _anotherNumber_:

```
>>> Plus(3, Negated(4))
-1

>>> 3 + -4
-1
```

* * *

See also: -, +, *, /, Divide, Negated, Plus, Times

Categories: Math, Ugen
