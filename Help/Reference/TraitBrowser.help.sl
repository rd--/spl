# TraitBrowser

A `ColumnBrowser` where:

- column one lists the names of each `Trait` the system knows of
- column two shows the qualified names for the methods implemented at the selected trait

When a method is selected the text pane shows the definition of the method.

* * *

See also: TypeBrowser, SystemBrowser

Categories: Kansas
