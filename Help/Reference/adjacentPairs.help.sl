# adjacentPairs

- _adjacentPairs(aSequence)_

Answer the adjacent pairs of _aSequence_.

At `Range`:

```
>>> 1:7.adjacentPairs
[1 2; 2 3; 3 4; 4 5; 5 6; 6 7]
```

At `List`:

```
>>> [1 .. 7].adjacentPairs
[1 2; 2 3; 3 4; 4 5; 5 6; 6 7]
```

* * *

See also: adjacentPairsCollect, adjacentPairsDo, partition, partitionDo
