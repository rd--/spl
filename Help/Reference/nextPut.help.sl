# nextPut

- _nextPut(aStream, anObject)_

Write _anObject_ into _aStream_.

* * *

See also: nextPutAll, putOn, Stream, WriteStream

References:
_Smalltalk_
5.9.4.3

Categories: Accessing
