# isBigEndian

- _isBigEndian(aSystem)_

Answer `true` if _aSystem_ is Big-endian.

```
>>> system.isBigEndian
false
```

* * *

See also: byteOrdering, isLittleEndian

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/ByteOrdering.html),
_W_
[1](https://en.wikipedia.org/wiki/Endianness)
