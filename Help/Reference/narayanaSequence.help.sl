# narayanaSequence

- _narayanaSequence(anInteger)_

Answer the first _anInteger_ elements of the Narayana sequence.

The first few elements, c.f. OEIS A000930:

```
>>> 23.narayanaSequence
[
	1 1 1 2 3 4 6 9 13 19
	28 41 60 88 129 189 277 406 595 872
	1278 1873 2745
]
```

The limit ratio between consecutive terms is the supergolden ratio:

```
>>> 2745/1873
1.supergoldenRatio
```

Plot first few terms:

~~~spl svg=A
23.narayanaSequence.linePlot
~~~

![](sw/spl/Help/Image/narayanaSequence-A.svg)

* * *

See also: supergoldenRatio

Guides: Mathematical Sequences

Categories: Math, Sequence
