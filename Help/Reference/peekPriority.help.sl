# peekPriority

- _peekPriority(aPriorityQueue)_

Answer the priority of the front item in _aPriorityQueue_,
or `nil` if the queue is empty.

* * *

See also: PriorityQueue
