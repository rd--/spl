# isCachedPrime

- _isCachedPrime(aCache, anInteger)_

Answer `true` if _anInteger_ is an element of the `cachedPrimesList` at _aCache_.

```
>>> system.isCachedPrime(23)
true
```

* * *

See also: cachedPrimesList, primesList

Categories: Testing
