# SuperColliderProgramOracle

The program oracle selects a program at random and opens it in a `SuperColliderProgramBrowser`.

* * *

See also: SmallKansas, SuperColliderProgramBrowser

Categories: Kansas
