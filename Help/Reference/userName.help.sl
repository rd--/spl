# userName

- _userName(aUrl)_

Answer a `String` having the user name of _aUrl_.
The user name is part of the authority component.

```
>>> 'x://u@x.x/x?x=x#x'.asUrl.userName
'u'
```

* * *

See also: fragment, host, hostName, href, pathName, port, protocol, query, Url

Categories: Network
