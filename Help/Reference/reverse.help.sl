# reverse

- _reverse(aSequence)_

Reverse _aSequence_ in place.
Answer _aSequence_.

At `List`:

```
>>> let a = [1 3 5 7];
>>> (a.reverse == a, a)
(true, [7 5 3 1])
```

* * *

See also: reversed, reverseDo, reverseWithDo

Categories: Rearranging
