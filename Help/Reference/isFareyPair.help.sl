# isFareyPair

- _isFareyPair(aFraction, anotherFraction)_

Answer true if two fractions are neighbours in a `fareySequence`.

```
>>> 1/3.isFareyPair(2/5)
true

>>> 1/3.isFareyPair(3/8)
true

>>> 2/5.isFareyPair(1/3)
false

>>> 3/8.isFareyPair(1/3)
false
```

* * *

See also: fareySequence
