# ArcSin

- _ArcSin(aNumber)_

Arc sine.

At `SmallFloat`:

```
>>> 0.4.ArcSin
0.41152
```

* * *

See also: arcSin, ArcCos

Categories: Math, Ugen, Trigonometry
