# asRectangle

- _asRectangle(aMatrix)_

Answer a `Rectangle` given _lower left_ and _upper right_ coordinates.

At `List`:

```
>>> [0 0; 1 1].asRectangle
Rectangle([0, 0], [1, 1])
```

* * *

See also: Rectangle

Categories: Geometry
