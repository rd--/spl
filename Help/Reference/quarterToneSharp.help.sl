# quarterToneSharp

- _quarterToneSharp(aNumber)_

Answer _aNumber + 0.05_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 4.quarterToneSharp
4.05
```

Where supported `quarterToneSharp` is displayed as 𝄲.

* * *

See also: degreeToKey, flat, quarterToneFlat, Scale, sharp

Unicode: U+1D132 𝄲 Musical Symbol Quarter Tone Sharp
