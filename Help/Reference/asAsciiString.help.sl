# asAsciiString

- _asAsciiString(aString)_

Answer an `AsciiString` value holding a byte-string encoding of _aString_.

```
>>> 'text'.asAsciiString.asByteArray
[116 101 120 116].asByteArray
```

* * *

See also: AsciiString, asciiByteArray, asHexString, isAscii, removeDiacritics

Categories: Converting
