# :@ (colonCommercialAt)

`:@` is a syntax token, it is not an operator.

The `:@` syntax directly reads and writes the value of a slot at a type.

Where supported `:@` is displayed as ⋄

The name of this token is `colonCommercialAt`.

* * *

See also: @, at

Guides: Slot Access Syntax, Syntax Guides

Unicode: U+22C4 ⋄ Diamond Operator

Categories: Syntax
