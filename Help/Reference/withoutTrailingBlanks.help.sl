# withoutTrailingBlanks

- _withoutTrailingBlanks(aString)_

Answer a copy of _aString_ from which trailing blanks have been trimmed.

```
>>> ' blanks '.withoutTrailingBlanks
' blanks'
```

* * *

See also: withBlanksTrimmed, withoutLeadingBlanks

Categories: Converting
