# terseReferenceSummary

- _terseReferenceSummary(aDirectoryName, options)_

Read all files at _aDirectoryName_,
extract all `Documentation Tests`,
evaluate each and summarise the results.

* * *

See also: HelpFile, terseGuideSummary, terseReferenceEntry

Guides: Documentation Tests

Categories: Help
