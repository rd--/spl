# LocalControl

A `Type` representing a local control input in a unit generator graph.

* * *

See also: localControls, NamedControl, ScUgen, Ugen, UgenGraph

Categories: Ugen
