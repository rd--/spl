# diameter

- _diameter(aGeometry)_

Answer the diameter of _aGeometry_,
which is twice the `radius`.

At `Circle`:

```
>>> Circle([0 0], 1).diameter
2
```

At `Sphere`:

```
>>> Sphere([0 0 0], 1).diameter
2
```

* * *

See also: Circle, circumference, radius, Sphere

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Diameter.html)

Categories: Geometry
