# second

- _second(aSequence | aDate)_

Answer the second element of _aSequence_:

```
>>> [1 2 3].second
2
```

or the second of _aDate_:

```
>>> '2024-03-04T21:41:07'.parseDate.second
7
```

* * *

See also: Date, dayOfMonth, first, hour, minute, month, parseDate, Sequence, year

References:
_Smalltalk_
5.8.1.25

Categories: Accessing, Time, Type
