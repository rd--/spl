# degreesToRadians

- _degreesToRadians(aNumber)_

Convert from degrees to radians.

There are 180° in `pi` radians:

```
>>> 360.degreesToRadians
2.pi
```

Equivalent to `degree`:

```
>>> 1.degree
1/180.pi
```

* * *

See also: Angle, degree, degrees, radians, radiansToDegrees

References:
_Smalltalk_
5.6.7.6

Categories: Converting, Math, Geometry
