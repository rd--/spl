# degrees

- _degrees(aNumber)_

Answer an `Angle` representing _aNumber_ degrees.

```
>>> 90.degrees
0.5.pi.radians

>>> 30.degrees
(1 / 6).pi.radians
```

See also `degree`, which answers the angle in radians directly:

```
>>> 90.degree
0.5.pi

>>> 30.degree
(1 / 6).pi
```

* * *

See also: Angle, degree, radians

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Degree.html)
