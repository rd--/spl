# hasEqualSlots

- _hasEqualSlots(anObject, anotherObject)_

Answer `true` if _aObject_ and _anotherObject_ have the same `type`,
and if each of the slots of _aObject_ equal the corresponding slot of _anotherObject_.

```
>>> 2/3.hasEqualSlots(2/3)
true
```

* * *

See also: =, ==, equalByAtNamedSlots, hasEqualElements, hasEqualSlots, slotList

Categories: Comparing, Reflection
