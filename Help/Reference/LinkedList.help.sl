# LinkedList

A collection of links, which are containers for other objects.

Using the methods `addFirst` and `removeLast` causes the list to behave as a stack.

Using `addLast` and `removeFirst` causes the list to behave as a queue.

Unlike `List` certain methods, such as `atIndex`, are linear and not constant time.

* * *

Categories: Collection, Type
