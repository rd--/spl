# SuperColliderProgramIndex

- _SuperColliderProgramIndex(aList)_

A `Type` holding an index of SuperCollider programs.

Each entry is a triple _[Category, Author, ProgramName]_.

* * *

See also: superColliderProgramIndex
