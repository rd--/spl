# Extensible

To implement `Extensible` a type must define `add`.

Methods for adding: `add`, `addAll`

* * *

See also: add, Collection, Removeable

Categories: Collection, Trait
