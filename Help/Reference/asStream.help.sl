# asStream

- _asStream(aCollection)_

Answer a `CollectionStream` on _aCollection_.

```
>>> let i = 1:9.asStream;
>>> (i.next, i.next, i.next)
(1, 2, 3)
```

* * *

See also: asIterator, asWriteStream, CollectionStream, Iterator, next, Stream

Categories: Converting
