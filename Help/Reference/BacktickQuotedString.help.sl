# BacktickQuotedString

- _BacktickQuotedString(aString)_

A `Type` representing a string written using backtick (or _grave accent_) quotes.
The `contents` method answers the quoted string.

```
>>> BacktickQuotedString('x').contents
'x'
```

* * *

See also: DoubleQuotedString, graveAccent, String, Symbol

Guides: String Syntax

Unicode: U+00060 Grave Accent

Categories: Text, Type
