# leastResidueSystem

- _leastResidueSystem(anInteger)_

Answer a complete `ResidueSet` with modulus _anInteger_.

```
>>> 4.leastResidueSystem
[0 1 2 3].asResidueSet(4)
```

* * *

See also: asResidueSet, ResidueSet

Categories: Enumerating
