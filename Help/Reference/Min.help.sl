# Min

- _Min(aNumber, anotherNumber)_

Maximum value.

```
>>> 9.Min(2)
2
```

Modulates and envelopes:

```
let z = SinOsc(500, 0);
z.Min(SinOsc(0.1, 0)) * 0.1
```

* * *

See also: Max

Categories: Ugen
