# respondsTo

- _respondsTo(anObject, aMethod)_

Answer whether the method dictionary at `Type` of _anObject_ contains an entry for _aMethod_.

```
>>> 9.respondsTo(sqrt:/1)
true

[1 2 3].respondsTo(select:/2)
true
```

* * *

See also: perform, Type, typeOf

References:
_Smalltalk_
5.3.1.19

Categories: Reflection
