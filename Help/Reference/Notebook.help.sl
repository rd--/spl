# Notebook

A simple model of a _computational notebook_.

A notebook is a numbered sequence of `SmallProgram` views.

When a program is _evaluated_, the `SmallProgram` is made read only, and a new program cell is created.

* * *

See also: SmallKansas, SmallProgram, Workspace

Categories: Kansas
