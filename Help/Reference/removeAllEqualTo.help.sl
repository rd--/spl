# removeAllEqualTo

- _removeAllEqualTo(aCollection, anObject)_

Remove every element of _aCollection_ that compares equal to _anObject_.

```
>>> let c = [1 2 2 3 3 3];
>>> c.without(3);
>>> c
[1 2 2]
```

* * *

See also: removeAllSuchThat, without

Categories: Removing
