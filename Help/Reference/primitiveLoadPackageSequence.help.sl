# primitiveLoadPackageSequence

- _primitiveLoadPackageSequence(aList)_

Loads the sequence of packages in _aList_,
answers a `Promise` that will resolve when the operation is complete.

* * *

See also: Package, Promise

Categories: Kernel
