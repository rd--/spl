# basicAtPut

- _basicAtPut(aList | aRecord, index, value)_

Unchecked mutation, answers nil for invalid indices.

* * *

See also: atPut, basicAt

Guides: Slot Access Syntax

Categories: Accessing
