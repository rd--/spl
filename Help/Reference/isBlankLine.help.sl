# isBlankLine

- _isBlankLine(aString)_

Answer `true` if _aString_ contains no characters (`isEmpty`),
or contains only spaces `isSpace` or tabs `isTab`.

```
>>> ' \t'.isBlankLine
true

>>> 'x'.isBlankLine
false
```

* * *

See also: isAllWhiteSpace, isEmpty, isSpace, isTab

Categories: Testing
