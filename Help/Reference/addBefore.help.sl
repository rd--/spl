# addBefore

- _addBefore(aSequence, newObject, oldObject)_

Add _newObject_ as an element of _aSequence_.
Put it in the sequence just preceding _oldObject_.
Answer _newObject_.

```
>>> let l = [1 2 4];
>>> let r = l.addBefore(3, 4);
>>> (r, l)
(3, [1 2 3 4])
```

* * *

See also: add, addAfter, addBeforeIndex

References:
_Smalltalk_
5.7.18.4

Categories: Adding
