# instructionSetArchitecture

- _instructionSetArchitecture(aSystem)_

Answer a `String` indicating the instruction set architecture.
_aarch64_ indicates Arm-64, _x86_64_ indicates Intel-64.

```
>>> ['aarch64', 'x86_64'].includes(
>>> 	system.instructionSetArchitecture
>>> )
true
```

* * *

See also: operatingSystem

Guides: System Functions

Categories: System
