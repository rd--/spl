# xFadeTextureProgram

- _xFadeTextureProgram(aBlock, sustainTime, transitionTime)_

Answer a `TextureProgram`.
Like `overlapTextureProgram` but with only two cross fading sounds.

* * *

See also: collectTextureProgram, overlapTextureProgram, spawnTextureProgram, TextureProgram, XFadeTexture

Categories: Scheduling
