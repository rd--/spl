# plainChangesDo

- _plainChanges(aSequence, aBlock:/1)_
- _steinhausJohnsonTrotterDo(alpha, beta)_ ⟹ _plainChangesDo(alpha, beta)_

Apply _aBlock_ at each of the permutations of _aSequence_
in the order given by the Steinhaus–Johnson–Trotter algorithm.

* * *

See also: plainChanges
