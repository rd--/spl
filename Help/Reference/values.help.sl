# values

- _values(aDictionary)_

Answer a `List` of the values of a dictionary or dictionary-like object.

At `Record`:

```
>>> (x: 1, y: 2, z: 3).values
[1 2 3]
```

At `TimeSeries`:

```
>>> [1 1; 2 3; 3 5].TimeSeries.values
[1 3 5]
```

* * *

See also: associations, Dictionary, indices, keys

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/Values.html),
_Smalltalk_
5.7.2.19

Categories: Accessing
