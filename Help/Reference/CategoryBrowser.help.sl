# CategoryBrowser

A `ColumnBrowser` where:

- column one lists the _categories_ the system knows of
- column two shows the _types_ belonging to the selected category
- column three shows the _qualified method names_ for the selected type

The methods shown include `Trait` methods.

For clarity the methods of the `Object` trait are omitted from the list.

When a method is selected the text pane shows the definition of the method.

The _Status Text_ shows the _Traits_ the selected type implements,
and the `origin` of the selected method.

* * *

See also: ColumnBrowser, HelpBrowser, MethodBrowser, MethodSignatureBrowser, TraitBrowser, TypeBrowser, SystemBrowser, WorldMenu

Categories: Kansas
