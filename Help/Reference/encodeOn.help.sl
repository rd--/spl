# encodeOn

- _encodeOn(aString, aBinaryStream)_

Encode _aString_ using `utf8ByteArray` onto _aBinaryStream_.

* * *

See also: isBinary, nextPutAll, utf8ByteArray, Utf8Stream
