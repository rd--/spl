# Menu

A `Menu` is a list of buttons that invoke blocks.

A menu is either transient or not transient.

A transient menu is deleted when an entry is selected.
A not transient menu persists and allows multiple interactions.

There is a context menu attached to the title bar that allows making the menu either transient or not.

* * *

See also: Frame, MenuItem, SmallKansas

Categories: Kansas
