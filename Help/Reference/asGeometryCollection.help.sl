# asGeometryCollection

- _asGeometryCollection(aGraph)_

Answer a `GeometryCollection` holding a representation of _aGraph_.

* * *

See also: GeometryCollection, Graph
