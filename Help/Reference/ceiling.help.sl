# ceiling

- _ceiling(aNumber)_

Answer the integer nearest the receiver toward infinity.

```
>>> 1.ceiling
1

>>> 1.1.ceiling
2

>>> -2.ceiling
-2

>>> -2.1.ceiling
-2

>>> (3 / 2).ceiling
2

>>> (-3 / 2).ceiling
-1

>>> 3/2.ceiling
2

>>> -3/2.ceiling
-1
```

Where supported `ceiling` is displayed as ⌈.

* * *

See also: floor, max, min, rounded, roundUpTo

References:
_Apl_
[1](https://aplwiki.com/wiki/Ceiling),
_Haskell_
[1](https://hackage.haskell.org/package/base/docs/Prelude.html#v:ceiling),
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/gtdot),
_Mathematica_
[1](https://mathworld.wolfram.com/CeilingFunction.html)
[2](https://reference.wolfram.com/language/ref/Ceiling.html),
_Smalltalk_
5.6.2.18

Unicode: U+02308 ⌈ Left Ceiling

Categories: Truncating and rounding
