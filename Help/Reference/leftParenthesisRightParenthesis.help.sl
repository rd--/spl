# ()

_()_ is part of the `Dictionary Syntax`, where constructs an empty `Record`:

```
>>> ()
Record()
```

It is also part of the `Apply Syntax` and `Value Apply Syntax` where it represents an empty parameter list:

```
>>> List()
[]

>>> List:/0 . ()
[]
```

* * *

See also: Record
