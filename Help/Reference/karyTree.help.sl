# karyTree

- _karyTree(n, k)_

Answer a _k_-ary tree with _n_ vertices.

Generate a binary tree with ten vertices:

~~~spl svg=A
karyTree(10, 2).treePlot
~~~

![](sw/spl/Help/Image/karyTree-A.svg)

Generate a ternary tree with fifteen vertices:

~~~spl svg=B
karyTree(15, 3).treePlot
~~~

![](sw/spl/Help/Image/karyTree-B.svg)

Generate a quaternary tree with eight vertices:

~~~spl svg=C
karyTree(8, 4).treePlot
~~~

![](sw/spl/Help/Image/karyTree-C.svg)

* * *

See also: completeKaryTree, starGraph, Tree

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/KaryTree.html)
