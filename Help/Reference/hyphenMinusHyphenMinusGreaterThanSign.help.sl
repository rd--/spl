# ---> (hyphenMinusHyphenMinusG...)

- _α --> β_ ⟹ _DirectedEdge(α, β)_

Answer a `DirectedEdge` between vertices α and β.

```
>>> 1 --> 2
DirectedEdge(1, 2)
```

* * *

See also: ---, DirectedEdge, Graph, UndirectedEdge

Unicode: U+027F6 ⟶ Long Rightwards Arrow
