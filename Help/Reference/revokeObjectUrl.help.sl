# revokeObjectUrl

- _revokeObjectUrl(aUrl)_

Release an existing object Url, previously created by `createObjectUrl`.

* * *

See also: createObjectUrl

References:
_W3c_
[1](https://w3c.github.io/FileAPI/#dfn-revokeObjectURL)
