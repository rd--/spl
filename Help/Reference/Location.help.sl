# Location

Location is a `Type` representing the location (Url) of an object.

`Location` implements the `Url` trait.

```
>>> system.window.location.isLocation
true
```

* * *

See also: Navigator, System, Url

References:
_Mdn_
[1](https://developer.mozilla.org/en-US/docs/Web/API/Location),
_Whatwg_
[1](https://html.spec.whatwg.org/multipage/nav-history-apis.html#the-location-interface)

Categories: Network
