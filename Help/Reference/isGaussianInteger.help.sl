# isGaussianInteger

- _isGaussianInteger(aComplex)_

A Gaussian integer is a `Complex` number _a+bi_ where _a_ and _b_ are integers.

```
>>> 2J3.isGaussianInteger
true
```

* * *

See also: Complex, isInteger

References:
_Mathematica_
[1](https://mathworld.wolfram.com/GaussianInteger.html)

Categories: Testing
