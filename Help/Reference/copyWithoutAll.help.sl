# copyWithoutAll

- _copyWithout(aCollection, anotherCollection)_

Answer a copy of _aCollection_ that does not contain any elements of _anotherCollection_.

* * *

See also: copyWithout

Categories: Copying
