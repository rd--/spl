# environmentVariables

- _environmentVariable(aSystem)_

Answer a `Record` holding the set of environment variables.
The keys are the names of the environment variables,
and each is associated with its value.

```
>>> system
>>> .environmentVariables
>>> .includesKey('HOME')
true
```

* * *

See also: environmentVariable

Guides: System Functions

Categories: System
