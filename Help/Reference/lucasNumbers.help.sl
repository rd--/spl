# lucasNumbers

- _lucasNumber(anInteger)_

Answer the first _anInteger_ Lucas numbers.

```
>>> 20.lucasNumbers
[
	1 3 4 7 11
	18 29 47 76 123
	199 322 521 843 1364
	2207 3571 5778 9349 15127
]
```

* * *

See also: fibbonaciSequence, lucasNumber, lucasPellNumbers, pellNumbers

References:
_OEIS_
[1](https://oeis.org/A000032)

Categories: Math, Sequence
