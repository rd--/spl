# asAssociationList

- _asAssociationList(aRunArray)_

Answer an `Association` `List`.

```
>>> RunArray([1, 3, 5], ['a', 'b', 'c'])
>>> .asAssociationList
['a' -> 1, 'b' -> 3, 'c' -> 5]
```

* * *

See also: associations, Association, RunAray

Categories: Converting, Collection
