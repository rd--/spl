# replaceWith

- _replaceWith(aRegExp, aString, anotherString)_

Replace first occurence of a string matching _aRegExp_ in _aString_ with _anotherString_:

```
>>> RegExp('x|z').replaceWith(
>>> 	'x y z',
>>> 	'-'
>>> )
'- y z'
```

* * *

See also: replace, replaceString, replaceStringAll, replaceAllWith

Categories: Replacing
