# lowBit

- _lowBit(anInteger)_

Answer the index of the low order one bit of _anInteger_.

```
>>> 40.lowBit
4

>>> 2r00101000.lowBit
4
```

* * *

See also: Binary, hightBit
