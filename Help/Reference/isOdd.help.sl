# isOdd

- _isOdd(aNumber)_

Answer `true` if _aNumber_ is an odd integer, else `false`.

Test whether integers are odd:

```
>>> 5.isOdd
true

>>> 8.isOdd
false

>>> 1:5.collect(isOdd:/1)
[true false true false true]
```

Non-integers are never odd:

```
>>> 1.pi.isOdd
false
```

* * *

See also: divisible, isEven, isInteger

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/OddQ.html),
_Smalltalk_
5.6.5.16

Categories: Testing, Math
