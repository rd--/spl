# isCollection

- _isCollection(anObject)_

Answer `true` if _anObject_ is a sort of `Collection`,
understanding the collection messages such as `size` and `do`.
The opposite predicate is `isAtom`.

* * *

See also: isAtom, Collection, collect, do, size, species

Categories: Testing, Collection
