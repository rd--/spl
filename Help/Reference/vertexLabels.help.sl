# vertexLabels

- _vertexLabels(aGraph)_
- _vertexLabels(aGraph)_ := aList

Answer, or assign, the vertex labels of _aGraph_.

Labels are arbitrary values associated with each vertex.

* * *

See also: edgeLabels, Graph, labeledVertexList, vertexCount, vertexList

Guides: Graph Functions

Categories: Graph
