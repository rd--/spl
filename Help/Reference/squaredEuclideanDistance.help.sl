# squaredEuclideanDistance

- _squaredEuclideanDistance(u, v)_

Answer the squared Euclidean distance between vectors _u_ and _v_.

Equivalent to _(u - v).norm ^ 2_.

Squared Euclidean distance between numeric vectors:

```
>>> [1 2 3]
>>> .squaredEuclideanDistance(
>>> 	[2 4 6]
>>> )
14

>>> [1 5 2 3 10]
>>> .squaredEuclideanDistance(
>>> 	[4 15 20 5 5]
>>> )
462
```

* * *

See also: chessboardDistance, editDistance, euclideanDistance, manhattanDistance, norm

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/SquaredEuclideanDistance.html),

Categories: Distance
