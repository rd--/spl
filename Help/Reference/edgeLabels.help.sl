# edgeLabels

- _edgeLabels(aGraph)_
- _edgeLabels(aGraph)_ := aList

Answer, or assign, the edge labels of _aGraph_.

Labels are arbitrary values associated with each edge.

* * *

See also: edgeCount, edgeList, Graph, vertexLabels

Guides: Graph Functions

Categories: Graph
