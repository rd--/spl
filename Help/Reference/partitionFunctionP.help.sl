# partitionFunctionP

- _partitionFunctionP(n)_
- _partitionFunctionP(n, k)_

The unary form answers the number of unrestricted partitions of the `Integer` _n_.

The binary form denotes the number of ways of writing _n_ as a `sum` of exactly _k_ terms
or, equivalently, the number of partitions into parts of which the largest is exactly _k_.

```
>>> 0:15.collect(partitionFunctionP:/1)
[
	1 1 2 3 5 7 11 15 22 30
	42 56 77 101 135 176
]

>>> 121.partitionFunctionP
2056148051

>>> 666.partitionFunctionP
11956824258286445517629485L
```

`partitionFunctionP` gives the length of `integerPartitions`:

```
>>> 5.integerPartitions
[5; 4 1; 3 2; 3 1 1; 2 2 1; 2 1 1 1; 1 1 1 1 1]

>>> 5.partitionFunctionP
7
```

Initial triangle of _P(n, k)_:

```
>>> 1:6.collect { :n |
>>> 	1:n.collect { :k |
>>> 		n.partitionFunctionP(k)
>>> 	}
>>> }
[
	1;
	1 1;
	1 1 1;
	1 2 1 1;
	1 2 2 1 1;
	1 3 3 2 1 1
]
```

Plot the number of unrestricted partitions:

~~~spl svg=A
0:50.functionPlot { :x |
	x.partitionFunctionP.asFloat.log10
}
~~~

![](sw/spl/Help/Image/partitionFunctionP-A.svg)

* * *

See also: integerPartitions, partitionFunctionQ, partitionsP

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PartitionFunctionP.html)
[2](https://reference.wolfram.com/language/ref/PartitionsP.html)

Categories: Math, Combinatorics
