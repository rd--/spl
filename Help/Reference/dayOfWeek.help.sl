# dayOfWeek

- _dayOfWeek(aDate)_

Answer the day of week of _aDate_.
Sunday is day one, Saturday is day seven.

```
>>> 0.asDate.dayOfWeek
5

>>> '2024-03-04'.parseDate.dayOfWeek
2
```

* * *

See also: asDate, Date, minute, month, parseDate, year

References:
_Smalltalk_
5.8.1.9

Categories: Time, Type
