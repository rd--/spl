# <- (lessThanSignHyphenMinus)

- _value <- key_ ⇒ _Association(key, value)_

The _<-_ operator creates as `Association` object associating _key_ with _value_.

```
>>> 1 <- 'x'
Association('x', 1)
```

The name of this operator is `lessThanSignHyphenMinus`.

Where supported `<-` is displayed as ←.

* * *

See also: ->, Association, key, value

Unicode: U+2190 ← Leftwards Arrow
