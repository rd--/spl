# labeledVertexList

- _labeledVertexList(aGraph)_

Answer a answers a `List` of `Associations` between vertices and labels,
providing a default empty label if `vertexLabels` is `nil`.

Labels are arbitrary values associated with each vertex,
set using `vertexLabels`.

* * *

See also: Graph, vertexCount, vertexLabels, vertexList

Guides: Graph Functions

Categories: Graph
