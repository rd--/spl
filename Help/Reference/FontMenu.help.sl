# FontMenu

A simple font selection menu.

When opened from the `WorldMenu` it edits the font of `SmallKansas`.

* * *

See also: FontSizeMenu, WorldMenu

Categories: Kansas
