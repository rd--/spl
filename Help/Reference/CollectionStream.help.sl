# CollectionStream

A `CollectionStream` is a `Type` that implements a `Stream` which is backed by a `Sequence` type.

`CollectionStream` implements the `Iterator` and `Stream` traits.

The basic protocols on a `Stream` are `next` and `reset`.

* * *

See also: asStream, asWriteStream, next, peek, reset, PositionableStream, Stream, WriteStream

Categories: Collection, Type
