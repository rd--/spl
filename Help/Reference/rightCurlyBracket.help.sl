# } (rightCurlyBracket)

`}` is a syntax token, it is not an operator.

`}` is a part of `Block Syntax`,
where it ends a block that was begun by `{`.

The name of this syntax token is `rightCurlyBracket`.

* * *

See also: {

Guides: Block Syntax, Syntax Tokens

Categories: Syntax
