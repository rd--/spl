# perrinSequence

- _perrinSequence(anInteger)_

Answer the first _anInteger_ elements of the Perrin sequence:

```
>>> 27.perrinSequence
[
	   3    0    2    3    2    5    5    7   10   12
	  17   22   29   39   51   68   90  119  158  209
	 277  367  486  644  853 1130 1497
]
```

C.f. `perrinFunction`:

```
>>> 0:26.collect(perrinFunction:/1)
[
	   3    0    2    3    2    5    5    7   10   12
	  17   22   29   39   51   68   90  119  158  209
	 277  367  486  644  853 1130 1497
]
```

* * *

See also: padovanSequence, perrinFunction, plasticRatio

Guides: Mathematical Sequences

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PerrinSequence.html),
_OEIS_
[1](https://oeis.org/A001608)

Categories: Math, Sequence
