# pathIsAbsolute

- _pathIsAbsolute(aString)_

Answer a `Boolean` telling if the path is absolute or relative.

```
>>> '/p/q/r.s'.pathIsAbsolute
true

>>> 'p/q/r.s'.pathIsAbsolute
false
```

* * *

See also: pathBasename, pathDirectory, pathExtension, pathJoin, pathNormalize

Guides: File Functions

Categories: Testing, System
