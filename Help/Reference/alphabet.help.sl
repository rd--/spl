# alphabet

- _alphabet(aString)_

Answer a `List` of the letters in the named alphabet.

```
>>> 'english'.alphabet.stringCatenate
'abcdefghijklmnopqrstuvwxyz'

>>> 'greek'.alphabet.stringCatenate
'αβγδεζηθικλμνξοπρστυφχψω'
```

The answer has only lower case letters:

```
>>> 'english'
>>> .alphabet
>>> .stringCatenate
>>> .asUpperCase
'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

>>> 'greek'
>>> .alphabet
>>> .stringCatenate
>>> .asUpperCase
'ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ'
```

* * *

See also: characterRange, Character, String

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/Alphabet.html)

Categories: Enumerating, Text
