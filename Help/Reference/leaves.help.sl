# leaves

- _leaves(aTree)_

Answer a List of the _value_ of each of the leaf subtrees:

```
>>> [1, [2, [3], 4], 5]
>>> .expressionTree(nil)
>>> .leaves
[1 2 3 4 5]
```

Count the even numbers in a Tree:

```
>>> [1, [2, [3], 4], 5]
>>> .expressionTree(nil)
>>> .count { :each |
>>> 	each.value.isNumber & {
>>> 		each.value.isEven
>>> 	}
>>> }
2
```

* * *

See also: leafCount, Tree

Guides: Tree Functions

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/TreeLeaves.html)

Categories: Accessing
