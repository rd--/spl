# Max

- _Max(aNumber, anotherNumber)_

Maximum value.

```
>>> 9.Max(2)
9
```

Modulates and envelopes:

```
let z = SinOsc(500, 0);
z.Max(SinOsc(0.1, 0)) * 0.1
```

* * *

See also: Min

Categories: Ugen
