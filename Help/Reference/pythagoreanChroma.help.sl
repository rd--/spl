# pythagoreanChroma

- _pythagoreanChroma(aNumber)_

The Pythagorean chroma,
or apotome,
or Pythagorean chromatic semitone,
is the chromatic semitone in the Pythagorean tuning.

```
>>> 1.pythagoreanChroma
2187/2048

>>> 1.pythagoreanChroma.ratioToCents
113.685
```

It is the 3-limit interval between seven perfect just fifths (3/2) and four octaves (2/1):

```
>>> (3/2 ^ 7) / (2 ^ 4)
2187/2048
```

The Pythagorean minor second is narrower:

```
>>> 1.pythagoreanLimma.ratioToCents
90.225
```

* * *

See also: mercatorsComma, octaveReduced, pythagoreanComma, pythagoreanLimma, septimalComma, syntonicComma

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/2187/2048)

Categories: Math, Contant, Tuning
