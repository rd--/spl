# Reciprocal

- _Reciprocal(aNumber)_

Answer the reciprocal of _aNumber_,
which is `one` divided by _aNumber_.

```
>>> 3.Reciprocal
1/3

>>> 1.Divide(3)
1/3

>>> 1 / 3
1/3
```

* * *

See also: /, Divide, reciprocal

Categories: Ugen
