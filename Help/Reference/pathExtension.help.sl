# pathExtension

- _pathExtension(aPath)_

Answer the extension component of the `String` _aPath_.

```
>>> '/p/q/r.s'.pathExtension
'.s'
```

If there is no extension answers the empty string:

```
>>> '/p/q/r'.pathExtension
''
```

* * *

See also: pathBasename, pathDirectory, pathIsAbsolute, pathJoin, pathNormalize

Guides: File Functions

Categories: System
