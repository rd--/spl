# Package

A `Package` is a named collections of types, traits and methods.

A package has a _name_ and a _category_.
The qualified name of a package is _category-name_.

The following `Trait` packages are considered to be part of the _Kernel_ and do not need to be required:

- `Binary`
- `Cache`
- `Integer`
- `Magnitude`
- `Number`
- `Object`
- `Random`

The following `Type` packages are considered to be part of the _Kernel_ and do not need to be required:

- `Association`
- `Block`
- `Boolean`
- `Error`
- `List`
- `Map`
- `Nil`
- `Promise`
- `Range`
- `Record`
- `Reponse`
- `SmallFloat`
- `String`
- `System`
- `Void`

* * *

See also: packageDictionary, packageMethods, packageTraits, packageTypes, System, Trait, Type

Categories: System, Library, Organisation
