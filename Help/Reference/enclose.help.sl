# enclose

- _enclose(anObject)_

Enclose _anObject_ in a `List`.
Answers a `List` of one place.

```
>>> 1.enclose
[1]

>>> [1].enclose
[[1]]

>>> [1 2 3] + [4 5 6].enclose
[5 6 7; 6 7 8; 7 8 9]

>>> 1.enclose.first
1
```

_enclose(x)_ can be written _[x]_.

* * *

See also: first, nest

References:
_Apl_
[1](https://aplwiki.com/wiki/Enclose)
