# selection

- _selection(aDocument | aWindow)_

Answer the `Selection` associated with _aDocument_,
or the `Document` at _aWindow_.
May answer `nil` if the document is not displayed.

* * *

See also: Document, DocumentRange, Selection, Window

References:
_W3c_
[1](https://w3c.github.io/selection-api/#dom-document-getselection)
