# deepMax

- _deepMax(aCollection)_

Answer `deepReduce` of `max`.

At 3×3 matrix:

```
>>> [8 1 6; 3 5 7; 4 9 2].deepMax
9
```

* * *

See also: ceiling, deepMin, deepReduce, maxBy, max, min

Categories: Testing
