# findFirstElement

- _findFirstElement(aList, aBlock:/1)_

A type specialised form of `detect`.

Detect the first prime number in a `List`:

```
>>> [99 .. 111].findFirstElement(isPrime:/1)
101
```

* * *

See also: detect
