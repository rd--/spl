# Delay1

- _Delay1(in)_

Single sample delay.

- in: sample to be delayed.

Original, with delayed subtracted from original:

```
let z = Dust(1000);
[z, z - Delay1(z)]
```

* * *

See also: DelayC, DelayL, DelayN, Delay2, DelayTap, DelayWrite

References:
_Csound_
[1](https://csound.com/docs/manual/delay1.html),
_SuperCollider_
[1](https://doc.sccode.org/Classes/Delay1.html)

Categories: Ugen, Delay
