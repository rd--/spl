# hertz

- _hertz(aNumber)_

Answer a `Frequency` representing _aNumber_ in hertz.

```
>>> let f = 440.hertz;
>>> (f.isFrequency, f.asHertz)
(true, 440)
```

* * *

See also: asHertz, Frequency
