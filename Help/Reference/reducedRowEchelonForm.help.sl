# reducedRowEchelonForm

- _reducedRowEchelonForm(aMatrix)_

An in place variant of `rowReduce`.

```
>>> let m = [1 2 3; 4 5 6; 7 8 9];
>>> m.reducedRowEchelonForm;
>>> m
[1 0 -1; 0 1 2; 0 0 0]
```

* * *

See also: rowReduce

Categories: Math, Matrix
