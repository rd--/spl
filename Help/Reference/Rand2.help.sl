# Rand2

- _Rand2(α)_ ⟹ _Rand(-α, α)_

Random number generator.

* * *

See also: Rand, Rand0

Categories: Ugen
