# LsTrace

- _LsTrace(input)_

Answer a stream that is equal to _input_ but prints each value as it is acquired.

```
>>> LsTrace(LsSeries(1, 1, 9)).upToEnd
[1 .. 9]
```

* * *

See also: LsCollect

Guides: Patterns and Streams

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Ptrace.html)

Categories: Stream
