# catalanNumber

- _catalanNumber(anInteger)_

Answer the _anInteger_-th Catalan number.
The Catalan numbers are a sequence of natural numbers that occur in various counting problems,
often involving recursively defined objects.

The first 10 Catalan numbers:

```
>>> 1:10.collect(catalanNumber:/1)
[1 2 5 14 42 132 429 1430 4862 16796]
```

* * *

See also: bellNumber, binomial, catalanTriangle, dyckWords, lassalleNumber, superCatalanNumber

References:
_Mathematica_
[1](https://mathworld.wolfram.com/CatalanNumber.html)
[2](https://reference.wolfram.com/language/ref/CatalanNumber.html),
_OEIS_
[1](https://oeis.org/A000108),
_W_
[1](https://en.wikipedia.org/wiki/Catalan_number)

Categories: Math, Combinatorics
