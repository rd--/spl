# isCarriageReturn

- _isCarriageReturn(aCharacter)_

Answer `true` if _aCharacter_ is a carriage return.

```
>>> '\r'.isCarriageReturn
true

>>> 13.asCharacter.isCarriageReturn
true
```

* * *

See also: Character, isFormFeed, isLineFeed, isSeparator, isSpace, isTab

Categories: Testing
