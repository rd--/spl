# indicesDo

- _indicesDo(aCollection, aBlock:/1)_

Evaluate _aBlock_ at the valid indices of _aCollection_.
For sequenceable collections indices are enumerated in sequence.

* * *

See also: includesIndex, Indexable, indices, withIndexDo

Categories: Enumerating
