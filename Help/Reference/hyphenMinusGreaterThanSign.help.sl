# -> (hyphenMinusGreaterThanSign)

- _key -> value_ ⇒ _Association(key, value)_

The _->_ operator creates as `Association` object associating _key_ with _value_.

```
>>> 'x' -> 1
Association('x', 1)
```

The name of this operator us `hyphenMinusGreaterThanSign`.

Where supported `->` is displayed as →.

* * *

See also: <-, Association, key, value

Unicode: U+2190 → Rightwards Arrow

Categories: Collection
