# CrystalStructureBrowser

The `CrystalStructureBrowser` is a one column `ColumnBrowser`.

The single column shows the names of the crytsal lattice structures from the Leitner collection.

When selected, the text view shows the description of the lattice and a number of perspective drawings.

* * *

See also: ColumnBrowser, CrystalStructure, leitnerCatalogue, WorldMenu

Categories: Kansas
