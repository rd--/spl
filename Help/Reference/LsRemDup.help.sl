# LsRemDup

- _LsRemDup(input)_

Remove successive duplicates from _input_.

```
>>> LsRemDup(
>>> 	LsDupEach(
>>> 		LsSeries(1, 3, 5),
>>> 		2
>>> 	)
>>> ).upToEnd
[1 4 7 10 13]
```

* * *

See also: LsDupEach

Guides: Patterns and Streams

Categories: Stream
