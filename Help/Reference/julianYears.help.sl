# julianYears

- _julianYears(aNumber | aDuration)_

A Julian year is a unit of measurement of time defined as exactly 365.25 days of 86400 seconds each.

```
>>> 1.julianYears
365.25.days
```

* * *

See also: Duration, siderealMonths, synodicMonths

References:
_W_
[1](https://en.wikipedia.org/wiki/Julian_year_(astronomy))

Categories: Accessing, Date
