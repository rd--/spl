# planckConstant

- _planckConstant(aNumber)_

Answer _aNumber_ times the Planck constant,
given in joules per hertz (_J/Hz_).

The Planck constant,
denoted by _h_,
is a fundamental physical constant.

```
>>> 1.planckConstant
6.62607015 * (10 ^ -34)
```

* * *

See also: e, pi

References:
_W_
[1](https://en.wikipedia.org/wiki/Planck_constant)
