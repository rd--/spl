# Integer

`Integer` is a numeric `Trait`.

Methods for arithmetic: `isPowerOfTwo`, `factorial`, `gcd`, `lcm`, `take`

* * *

See also: Binary, Fraction, Number, SmallFloat, LargeInteger

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Integer.html)

Categories: Numeric, Trait
