# SystemBrowser

A `ColumnBrowser` where:

- column one lists the `Type` names the system knows of
- column two shows the qualified method names for the methods of the selected type

The methods include those defined at any `Trait` the type implements.

For clarity the methods of the `Object` trait are omitted from the list.

When a method is selected the text pane shows the definition of the method.

The _Status Text_ shows the traits the selected type implements.

* * *

See also: TraitBrowser, TypeBrowser

Categories: Kansas
