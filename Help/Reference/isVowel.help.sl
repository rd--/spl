# isVowel

- _isVowel(aCharacter)_

Answer `true` if _aCharacter_ is an english letter vowel, ie. one of _a_, _e_, _i_, _o_ or _u_, of either case.

```
>>> 'e'.isVowel
true

>>> 105.asCharacter.isVowel
true
```

At non letters answers `false`:

```
>>> '?'.isVowel
false
```

* * *

See also: isLetter, isSpace, isTab

Categories: Testing
