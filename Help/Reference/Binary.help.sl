# Binary

`Binary` is a numeric `Trait` holding behaviours related to the binary (bit-level) represenation of numbers.

* * *

See also: bitAnd, bitAt, bitAtPut, bitCount, bitNot, bitOr, bitShiftLeft, bitShiftRight, bitXor, isBinary

Categories: Numeric, Bits, Trait
