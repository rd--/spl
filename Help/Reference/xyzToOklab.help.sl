# xyzToOklab

- _xyzToOklab(xyz)_

Convert from Cie _Xyz_ tristimulus values to _Oklab_ colourspace.

```
>>> [0.2065400 0.1219723 0.0513695]
>>> .xyzToOklab
[0.516340 0.154695 0.062896]
```

Inverse is `oklabToXyz`:

```
>>> [0.516340 0.154695 0.062896]
>>> .oklabToXyz
[0.2065400 0.1219723 0.0513695]
```

* * *

See also: Colour, oklabToXyz

References:
_Python_
[1](https://colour.readthedocs.io/en/develop/generated/colour.XYZ_to_Oklab.html)
