# */ (asteriskSolidus)

`*/` is the close comment syntax token for PL/I, used also for the C family of languages.

Where supported `*/` is displayed as ».

The name of this token is `asteriskSolidus`.

* * *

See also: *, /

Guides: Comment Syntax

Unicode: U+000BB » Right-Pointing Double Angle Quotation Mark

Categories: Comments, Syntax
