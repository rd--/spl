# AllpassN

- _AllpassN(in=0, maxdelaytime=0.2, delaytime=0.2, decaytime=1)_

All pass delay line, no interpolation.

* * *

See also: AllpassC, AllpassL

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/AllpassN.html)

Categories: Ugen, Filter, Delay
