# SmallKansan

`SmallKansan` is a `Trait`.
The required method is `openIn`.
A small Kansan is a `Type` that can be opened in `SmallKansas`.

* * *

See also: SmallKansas

Categories: Kansas
