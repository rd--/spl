# HelpViewer

A `TextEditor` opened on a help file.

If no viewer is open a help request opens a new viewer, else the requested help is displayed in the existing viewer.

* * *

See also: HelpBrowser, WorldMenu

Categories: Kansas
