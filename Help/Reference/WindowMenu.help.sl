# WindowMenu

A `Menu` listing all of the `Frame`s open in `SmallKansas`.

Selecting a `Frame` brings it to the front of the viewing plane.

* * *

See also: Frame, Menu, SmallKansas

Categories: Kansas
