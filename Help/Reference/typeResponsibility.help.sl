# typeResponsibility

- _typeResponsibility(anObject, aString)_

Signal an error indicating that the requested method has been inherited from a `Trait` when it is a `Type` responsibility.

* * *

See also: Error, error, signal

Categories: Error Handling
