# addAllFirst

- _addAllFirst(aSequence, anotherSequence)_

Add all the elements of _anotherSequence_ to the start of _aSequence_.
Answer _anotherSequence_.

```
>>> let l = [4 5];
>>> (l.addAllFirst([1 2 3]), l)
([1 2 3], [1 2 3 4 5])
```

* * *

See also: addAll, addAllLast

References:
_Smalltalk_
5.7.18.10

Categories: Adding
