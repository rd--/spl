# -}

`-}` is the close comment token in Haskell.

The name of this syntax token is `hyphenMinusRightCurlyBracket`.

* * *

See also: {-

Guides: Comment Syntax
