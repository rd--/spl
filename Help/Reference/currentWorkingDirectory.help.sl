# currentWorkingDirectory

- _currentWorkingDirectory(aSystem)_

Answer a `String` giving the current working directory of the process.

The answer is a `String`:

```
>>> system
>>> .currentWorkingDirectory
>>> .isString
true
```

* * *

See also: environmentVariable, systemCommand

Guides: System Functions

Categories: System
