# Cubed

Cubed value.

```
>>> 3.Cubed
27
```

* * *

See also: cubed, squared

Categories: Math, Ugen
