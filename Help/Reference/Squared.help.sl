# Squared

- _Squared(aNumber)_

Answer _aNumber_ multiplied by itself.

```
>>> 3.Squared
9
```

* * *

See also: squared, Sqrt

Categories: Ugen
