# circumference

- _circumference(aCircle)_

Answer the arc length of _aCircle_.

```
>>> Circle([0 0], 1).circumference
2.pi
```

* * *

See also: arcLength, Circle, perimeter

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Circumference.html)

Categories: Geometry
