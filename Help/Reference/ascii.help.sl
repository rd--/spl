# ascii

- _ascii(aString)_

`ascii` is `asList` of `asciiByteArray`,
and answers a `List` of the Ascii encoding of _aString_,
which must be an Ascii string.

The Ascii encoding of 'ascii':

```
>>> 'ascii'.ascii
[97 115 99 105 105]
```

* * *

See also: asciiByteArray, asciiString, asList, String

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/String.html#-ascii)

Categories: String, Encoding
