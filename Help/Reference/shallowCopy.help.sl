# shallowCopy

- _shallowCopy(anObject)_

Answer a copy of _anObject_ which shares instance variables.

* * *

See also: copy, deepCopy, postCopy

Categories: Copying
