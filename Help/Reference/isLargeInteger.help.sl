# isLargeInteger

- _isLargeInteger(anObject)_

Answers `true` if _anObject_ is a `LargeInteger`, else `false`.

```
>>> 23L.isLargeInteger
true

>>> 23.isLargeInteger
false

>>> 3.141.isLargeInteger
false
```

* * *

See also: isInteger, isNumber, isSmallFloat, LargeInteger

Categories: Testing, Math
