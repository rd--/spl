# Matrix

`Matrix` is a `Trait` collecting behviours applicable to matrices.

* * *

See also: Array, List, NumericArray

Guides: Matrix Operations

References:
_Apl_
[1](https://aplwiki.com/wiki/Matrix),
_Mathematica_
[1](https://mathworld.wolfram.com/Matrix.html),
_W_
[1](https://en.wikipedia.org/wiki/Matrix_(mathematics))

Categories: Collection, Type
