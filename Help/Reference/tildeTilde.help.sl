# ~~ (tildeTilde)

- _anObject ~~ anotherObject_

Answers `true` if _anObject_ is not identical to _anotherObject_, else `false`.

This is the non-identity operator.
It decides if two values are not the same.

Where supported `~~` is displayed as ≢.

The name of this operator is `tildeTilde`.

* * *

See also: =, ~=, ==

Unicode: U+2262 ≢ Not Identical To

References:
_Smalltalk_
5.3.1.4

Categories: Comparing
