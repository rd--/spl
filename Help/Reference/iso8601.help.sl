# iso8601

- _iso8601(aDate | aTimeStamp)_

Format a `Date` or `TimeStamp` in ISO-8601 format.
Answers a `String`.

```
>>> 0.asDate.iso8601
'1970-01-01T00:00:00.000Z'

>>> 0.asTimeStamp.iso8601
'1970-01-01T00:00:00.000Z'
```

* * *

Categories: Time
