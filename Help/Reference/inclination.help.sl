# inclination

- _inclination(aPoint)_

Another name for `phi`:

```
>>> SphericalCoordinates(1, 0, 1.pi).inclination
1.pi
```

* * *

See also: phi

Guides: Geometry Types
