# dayOfMonth

- _dayOfMonth(aDate)_

Answer the day of month of _aDate_.

```
>>> 0.asDate.dayOfMonth
1

>>> '2024-03-04'.parseDate.dayOfMonth
4
```

* * *

See also: asDate, Date, minute, month, parseDate, year

References:
_Smalltalk_
5.8.1.8

Categories: Accessing, Date, Type
