# addWithOccurrences

- _addWithOccurrences(aCollection, anObject, anInteger)_

Add _anObject_ to _aCollection_ multiple times.
The operation is equivalent to adding _anObject_ to _aCollection_ _anInteger_ times using `add`.

```
>>> let l = [];
>>> l.addWithOccurrences(3, 5);
>>> l
[3 3 3 3 3]
```

* * *

See also: add

References:
_Smalltalk_
5.7.6.2

Categories: Adding
