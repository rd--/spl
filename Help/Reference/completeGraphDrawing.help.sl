# completeGraphDrawing

- _completeGraphDrawing(k, radius)_

It is conventional to draw a complete graph by placing the vertices equidistantly on a circle.

Complete graph on five vertices:

~~~spl svg=A
5.completeGraphDrawing(1)
~~~

![](sw/spl/Help/Image/completeGraphDrawing-A.svg)

Complete graph on seven vertices:

~~~spl svg=B
23.completeGraphDrawing(1)
~~~

![](sw/spl/Help/Image/completeGraphDrawing-B.svg)

* * *

See also: circlePoints, completeGraph, Line, LineDrawing, PointCloud

Guides: Graph Functions
