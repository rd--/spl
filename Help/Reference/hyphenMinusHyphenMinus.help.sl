# -- (hyphenMinusHyphenMinus)

- _min -- max_ ⇒ _Interval(min, max)_

Operator form of `Interval`, a type representing a closed interval.

```
>>> 1 -- 9
Interval(1, 9)
```

The name of this operator is `hyphenMinusHyphenMinus`.

Where supported `--` is displayed as —.

* * *

See also: ->, Interval, Range

Guides: Range Syntax

Unicode: U+2014 — Em Dash

Categories: Constructor, Number, Operator
