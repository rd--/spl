# takeLast

- _takeLast(aSequence, anInteger, anObject)_

Answer a copy of _aSequence_ with only the last _anInteger_ places.
Fill any empty slots with _anObject_.

```
>>> [5 4 3 2 1].takeFirst(8, 0)
[5 4 3 2 1 0 0 0]
```

* * *

See also: take, takeFirst
