# arcMinute

- _arcMinute(aNumber)_

Answer _aNumber_ times _arcMinute_, the constant representing one sixtieth of a `degree`.

```
>>> 1.arcMinute
(1.degree / 60)

>>> 1.arcMinute
60.arcSecond
```

* * *

See also: arcSecond, degree, pi

Guides: Mathematical Constants

References:
_Mathematica_
[1](https://mathworld.wolfram.com/ArcMinute.html)

Categories: Math, Constant
