# includesAnyOf

- _includesAnyOf(aCollection, anotherCollection)_

Answer whether any elements of _anotherCollection_ are elements of _aCollection_.

```
>>> 3:5.includesAnyOf(5:7)
true

>>> 3:5.includesAnyOf(9:11)
false
```

* * *

See also: includes, includesAllOf

Categories: Testing
