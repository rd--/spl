# asWords

- _asWords(anInteger)_

Answer _anInteger_ spelled out in English words.

```
>>> 123456E3.asWords
'one hundred twenty-three million, ' ++
'four hundred fifty-six thousand'

>>> 13579.asWords
'thirteen thousand, ' ++
'five hundred seventy-nine'
```

* * *

See also: asString, printString, storeString

Categories: Printing
