# allTrue

- _allTrue(aCollection)_

Answer `true` if all items in _aCollection_ are `true`, else `false`.

```
>>> ([1 3 5 7 9] < [3 5 7 9 11]).allTrue
true
```

The empty list always answers `true`:

```
>>> [].allTrue
true
```

* * *

See also: allSatisfy, anySatisfy, anyTrue, noneSatisfy

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/AllTrue.html),
_Python_
[1](https://docs.python.org/3/library/functions.html#all)

Categories: Testing
