# isOne

- _isOne(aNumber)_

Answer `true` if _aNumber_ is `one`.

```
>>> 1.isOne
true

>>> 0.isOne
false
```

* * *

See also: isEven, isNegative, isOdd, isOne, isPositive, one

Categories: Testing, Math
