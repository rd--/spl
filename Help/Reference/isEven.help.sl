# isEven

- _isEven(aNumber)_

Answer `true` if _aNumber_ is an even integer, else `false`.

Test whether integers are even:

```
>>> 8.isEven
true

>>> 5.isEven
false

>>> 1:5.collect(isEven:/1)
[false true false true false]
```

Non-integers are never even:

```
>>> 1.pi.isEven
false
```

* * *

See also: divisible, isInteger, isOdd

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/EvenQ.html),
_Smalltalk_
5.6.5.10

Categories: Testing, Math
