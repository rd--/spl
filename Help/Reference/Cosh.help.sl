# Cosh

Hyperbolic cosine.

* * *

See also: Sinh, Tanh

References:
_Csound_
[1](https://csound.com/docs/manual/cosh.html)

Categories: Ugen
