# Log

- _Log(aNumber)_

Natural logarithm.

The `Log` of `e` is `one`:

```
>>> 1.e.Log
1
```

* * *

See also: Exp, Log2, Log10, Pow

Categories: Ugen
