# isLeaf

- _isLeaf(aTree)_

A `Tree` is a _leaf_ if its `size` is `zero`:

```
>>> Tree(1, []).isLeaf
true

>>> Tree(1, [Tree(2, [])]).isLeaf
false
```

* * *

See also: isTree, size, Tree, zero

Guides: Tree Functions

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/TreeLeafQ.html)

Categories: Testing
