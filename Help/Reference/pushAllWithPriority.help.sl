# pushAllWithPriority

- _pushAllWithPriority(aPriorityQueue, anAssociationList)_

Add each item at _anAssociationList_ to _aPriorityQueue_.
The associations hold the item as the `key` and the priority as the `value`.

* * *

See also: addAll, PriorityQueue

Categories: Adding
