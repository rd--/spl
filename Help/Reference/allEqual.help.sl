# allEqual

- _allEqual(aCollection)_

Answer `true` is all items in _aCollection_ are equal according to `=`.

```
>>> [1 1 1].allEqual
true

>>> [1 2 3].allEqual
false
```

* * *

See also: =, allSatisfy, kroneckerDelta

Categories: Testing
