# numberOfColumns

- _numberOfColumns(aSequence | aMatrix)_

Answer the number of columns in an array.
This is the second element of `shape`.

A 2×3 matrix:

```
>>> [
>>> 	1 2 3;
>>> 	4 5 6
>>> ].numberOfColumns
3
```

A 3×2 matrix:

```
>>> [
>>> 	1 2;
>>> 	3 4;
>>> 	5 6
>>> ].numberOfColumns
2
```

At `NumericArray`:

```
>>> [3 4].iota.asNumericArray
>>> .numberOfColumns
4
```

* * *

See also: isArray, isMatrix, isVolume, List, Matrix, numberOfRows, shape
