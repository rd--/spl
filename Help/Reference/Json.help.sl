# Json

A `Trait` for types that have an encoding in Json (JavaScript Object Notation).
Json is a simple plain text data-interchange format.

The types implementing the trait are: `Nil`, `Boolean`, `SmallFloat`, `String`, `List` & `Record`.

* * *

See also: asJson, Boolean, isJson, json, List, parseJson, Nil, Record, SmallFloat, String

References:
_Json_
[1](https://www.json.org/json-en.html)

Further Reading: Crockford 2006

Categories: Protocol, Trait
