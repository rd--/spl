# lowBitPerByteTable

A ByteArray that contains the index of the lowest set bit of the integers between 1 and 255.

* * *

Categories: System
