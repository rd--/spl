# ' (apostrophe)

`'` is a syntax token, it is not an operator.

It is part of `String Syntax`.

```
>>> 'quincunx'.isString
true
```

The name of this syntax token is `apostrophe`.

* * *

See also: graveAccent, quotationMark, String

Guides: Literals Syntax, String Syntax

Unicode: U+0027 Apostrophe

Categories: Syntax
