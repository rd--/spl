# Html

- _Html(aString)_

A `Type` holding a Hypertext Markup Language document,
or document fragment.

```
>>> Html(
>>> 	'<p>A paragraph.</p>'
>>> ).isFragment
true
```

Methods are:

- `asHtmlElement`: document node
- `contents`: Html string
- `isDocument`: is document
- `isFragment`: is fragment

* * *

See also: asHtmlElement, contents, isDocument, isFragment, parseHtml

Categories: Text
