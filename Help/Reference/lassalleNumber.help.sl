# lassalleNumber

- _lassalleNumber(m)_

Lassalle’s sequence connected with Catalan numbers and Narayana polynomials.

```
>>> 1:13.collect(lassalleNumber:/1)
[
	1 1 5 56 1092 32670 1387815 79389310
	5882844968 548129834616 62720089624920
	8646340208462880 1413380381699497200
]
```

* * *

See also: catalanNumber, narayanaNumber

References:
_OEIS_
[1](https://oeis.org/A180874)

Further Reading: Defant 2020, Lassalle 2012

Categories: Math, Sequence
