# asPowerOfTwo

- _asPowerOfTwo(anInteger)_

Answers the next power of two less than or equal to _anInteger_.

```
>>> 300.asPowerOfTwo
256

>>> 256.asPowerOfTwo
256
```

* * *

See also: asLargerPowerOfTwo, isPowerOfTwo

Categories: Truncation, Rounding
