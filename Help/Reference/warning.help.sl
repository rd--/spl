# warning

- _warning(anObject, aString)_

User warning.
Answer _anObject_, and also prints a warning.
The warning gives the type of _anObject_, its value, and the message _aString_.

```
>>> 1.pi.warning('pi')
1.pi
```

* * *

See also: error, identity, notify, postLine

Categories: Error
