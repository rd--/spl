# system

`system` is the name of the singular instance of the `System` type.

`system` holds the `methodDictionary`:

```
>>> system
>>> .methodDictionary
>>> .size
4262

>>> system
>>> .methodDictionary
>>> .keys
>>> .includes('sum')
true
```

`system` holds the `traitDictionary`:

```
>>> system
>>> .traitDictionary
>>> .size
44

>>> system
>>> .traitDictionary
>>> .keys
>>> .includes('Collection')
true
```

`system` holds the `typeDictionary`:

```
>>> system
>>> .typeDictionary
>>> .size
178

>>> system
>>> .typeDictionary
>>> .keys
>>> .includes('List')
true
```

* * *

See also: Method, Trait, Type, System

Categories: Reflection
