# Osc1

- _Osc1(table, dur)_

One-shot oscillator.
An oscillator that reads through a table only once.

- table: an instance of Signal; its size must be a power of 2.
- dur: how long to read through the table

Pitch class table, linear interpolation, first slowly, then quickly, then slowly again:

```
let tbl = [0 2 10 12].asLocalBuf;
SinOsc((Osc1(tbl, 5) + 48).MidiCps, 0) * 0.1
```

* * *

See also: Osc

Categories: Oscillator
