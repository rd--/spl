# wordAtCaret

- _wordAtCaret(aDocument | aWindow)_

Answer a `String` holding the word at the caret.
A word is delimited by white space.
If there is no such word answer the empty string.

* * *

See also: insertStringAtCaret, selectedText, selectedTextOrWordAtCaret, Window
