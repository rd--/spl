# sharp

- _sharp(aNumber)_

Answer _aNumber + 0.1_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 4.sharp
4.1

>>> 4.sharp.sharp
4.2
```

Where supported `sharp` is displayed as ♯.

* * *

See also: degreeToKey, flat, quarterToneSharp, Scale

Unicode: U+266F ♯ Music Sharp Sign
