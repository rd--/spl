# isPositive

- _isPositive(aNumber)_

Answer `true` if _aNumber_ is greater than `zero`.

Evaluate at different types of numbers:

```
>>> [1.6 3/4 1.pi 0 -5].collect(isPositive:/1)
[true true true false false]
```

* * *

See also: isNegative, isNonNegative

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/Positive.html),
_Smalltalk_
5.6.2.36

Categories: Math, Testing
