# withoutLeadingBlanks

- _withoutLeadingBlanks(aString)_

Answer a copy of _aString_ from which leading blanks have been trimmed.

```
>>> ' blanks '.withoutLeadingBlanks
'blanks '
```

* * *

See also: withBlanksTrimmed, withoutTrailingBlanks

Categories: Converting
