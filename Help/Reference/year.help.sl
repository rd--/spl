# year

- _year(aDate)_

Answer the year of _aDate_.

```
>>> 0.asDate.year
1970

>>> '2024-03-04'.parseDate.year
2024
```

* * *

See also: asDate, Date, day, minute, month, parseDate

References:
_Smalltalk_
5.8.1.28

Categories: Accessing, Date
