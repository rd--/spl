# range

- _range(aCollection)_

Answer the difference between the `min` and `max` values of _aCollection_.

```
>>> 1:9.range
8

>>> 9.primesList.range
21
```

* * *

See also: -, max, min

Categories: Math
