# stringCatenate

- _stringCatenate(aSequence)_

Join together the `String` items of _aSequence_.

At `List`:

```
>>> ['p' 'q' 'r' 's'].stringCatenate
'pqrs'
```

At the empty list:

```
>>> [].stringCatenate
''
```

* * *

See also: catenate, join, interleave, intersperse, splitBy, stringIntercalate, stringJoin

Categories: Rearranging, String
