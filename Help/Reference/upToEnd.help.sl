# upToEnd

- _upToEnd(aStream)_

Answer the remainder of the items accessible by _aStream_.

```
>>> 1:9.asStream.reject(isEven:/1).upToEnd
[1 3 5 7 9]
```

* * *

See also: contents, next, nextOrUpToEnd, reset, Stream, upTo

Categories: Accessing
