# floorLog

- _floorLog(aNumber, radix)_

Answer the `floor` of the `log` base _radix_ of _aNumber_.

```
>>> 100.floorLog(10)
2

>>> 100.log(10).floor
2

>>> (1 / 100).floorLog(10)
-2

>>> (1 / 100).log(10).floor
-2
```

* * *

See also: floor, log

References:
_Smalltalk_
5.6.7.8
