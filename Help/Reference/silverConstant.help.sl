# silverConstant

- _silverConstant(anInteger)_

The silver constant is the seventh Beraha constant:

```
>>> 1.silverConstant
7.berahaConstant

>>> 1.silverConstant
3.247
```

* * *

See also: berahaConstant

References:
_Mathematica_
[1](https://mathworld.wolfram.com/SilverConstant.html),
_OEIS_
[1](https://oeis.org/A116425),

Categories: Math, Constant
