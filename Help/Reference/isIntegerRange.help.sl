# isIntegerRange

- _isIntegerRange(aRange)_

Answers `true` if _aRange_ has integral `start`, `end` and `step` values.

```
>>> 1:9.isIntegerRange
true
```

* * *

See also: isInteger, Range

Categories: Testing
