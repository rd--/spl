# siderealMonths

- _siderealMonths(aNumber | aDuration)_

The period of the Moon’s orbit as defined with respect to the celestial sphere of apparently fixed stars.
It is the time it takes the Moon to return to a similar position among the stars.

```
>>> 1.siderealMonths
27.days + 7.hours + 43.minutes + 11.6.seconds
```

* * *

See also: Duration, synodicMonths

References:
_W_
[1](https://en.wikipedia.org/wiki/Lunar_month#Sidereal_month)
