# limit

- _limit(aRatioTuning)_

Answer the `primeLimit` of _aRatioTuning_.

```
>>> [1 8/7 4/3 14/9 16/9]
>>> .asRatioTuning
>>> .limit
7
```

* * *

See also: primeLimit, RatioTuning, Tuning
