# isAlphaNumeric

- _isAlphaNumeric(aCharacter | aString)_

Answer `true` if _aCharacter_ is either a letter or digit, else `false`.

```
>>> '3'.isAlphaNumeric
true

>>> 'x'.isAlphaNumeric
true

>>> ','.isAlphaNumeric
false
```

* * *

See also: isDigit, isLetter

References:
_Smalltalk_
5.3.4.6

Categories: Testing
