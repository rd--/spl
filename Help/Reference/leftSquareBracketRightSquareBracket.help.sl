# []

This is the `Empty List Syntax`.

It constructs an empty List.

```
>>> []
List()
```

* * *

See also: (), List, Record
