# isIdentity

- _isIdentity(aPermutation)_

Answer `true` if _aPermutation_ is the identity permutation.

A permutation with only fixed points (unary cycles):

```
>>> [1; 2; 3].cycles.isIdentity
true
```

An empty cycles list:

```
>>> [].cycles.isIdentity
true
```

* * *

See also: Permutation

Categories: Permutations
