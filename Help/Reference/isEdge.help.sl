# isEdge

- _isEdge(anObject)_

Answer `true` if _anObject_ is allowed as an _edge_ in a `Graph`.

Directed edges are edges:

```
>>> (1 --> 2).isEdge
true
```

Undirected edges are edges:

```
>>> (1 --- 2).isEdge
true
```

* * *

See also: Graph

Guides: Graph Functions

References:
_Mathematica_
[1](https://mathworld.wolfram.com/GraphEdge.html)

Categories: Collection
