# @> (commercialAtGreaterThanSign)

- _aCollection @> aSequence_

The operator (infix) form of `atPath`.

```
>>> (w: (x: (y: (z: 1)))) @> ['w' 'x' 'y' 'z']
1
```

The name of this operator is `commercialAtGreaterThanSign`.

* * *

See also: @*, atAll, atPath

Categories: Accesing
