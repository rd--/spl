# pathNormalize

- _pathNormalize(aPath)_

Answer the normalized form of the `String` _aPath_.

```
>>> '/p/./q/../q/r.s'.pathNormalize
'/p/q/r.s'
```

* * *

See also: pathBasename, pathDirectory, pathExtension, pathIsAbsolute, pathJoin

Guides: File Functions

Categories: System
