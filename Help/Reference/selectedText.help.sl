# selectedText

- _selectedText(aDocument | aWindow)_

Answer a `String` holding the currently selected text.
If there is no selection the answer will be the empty string.

* * *

See also: wordAtCaret
