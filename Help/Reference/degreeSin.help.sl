# degreeSin

- _degreeSin(aNumber)_

Answer the sine of _aNumber_ taken as an angle in degrees.

```
>>> 90.degreeSin
1

>>> 90.degrees.sin
1
```

* * *

See also: cos, degreeCos, sin

Categories: Trigonometry
