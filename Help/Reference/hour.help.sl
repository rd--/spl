# hour

- _hour(aDate)_

Answer the hour of _aDate_.

```
>>> '2024-03-04T21:41'.parseDate.hour
21
```

* * *

See also: asDate, Date, minute, month, parseDate, year

References:
_Smalltalk_
5.8.1.13

Categories: Accessing, Time
