# Float64Array

- _Float64Array(anInteger)_

Answer a `Float64Array` of _anInteger_ places, each initialized to `zero`.

A `Float64Array` is an array whose elements are IEEE 64-bit floating point values.
Unlike a `List`, a `Float64Array` is of fixed size.

```
>>> Float64Array(5)
[0 0 0 0 0].asFloat64Array
```

* * *

See also: asFloat64Array, ByteArray, List, Float32Array

References:
_Tc39_
[1](https://tc39.es/ecma262/multipage/indexed-collections.html#table-49)

Categories: Collection, Type
