# \ (reverseSolidus)

- _aCollection \ anotherCollection_

The operator form of `difference`.

The name of this operator is `reverseSolidus`.

_Note_:
This is also the usual mathematical notation for `quotient`.

* * *

See also: -, difference

Categories: Math
