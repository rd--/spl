# Sphere

- _Sphere(aPoint, aNumber)_

Answer a `Sphere` with center at _aPoint_ and radius _aNumber_.

```
>>> let s = Sphere([0 0 0], 1);
>>> (
>>> 	s.center,
>>> 	s.radius,
>>> 	s.diameter,
>>> 	s.surfaceArea,
>>> 	s.volume
>>> )
([0 0 0], 1, 2, 4.pi, 4/3.pi)
```

* * *

See also: center, diameter, Circle, Ellipse, radius, randomSurfacePoint, spherePoints, surfaceArea, volume

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Sphere.html)
[2](https://reference.wolfram.com/language/ref/Sphere.html)

Categories: Geometry, Type
