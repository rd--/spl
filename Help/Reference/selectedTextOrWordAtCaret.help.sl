# selectedTextOrWordAtCaret

- _selectedTextOrWordAtCaret(aDocument | aWindow)_

Answer `selectedText` if there is a non-empty selection,
else answer `wordAtCaret`.

* * *

See also: selectedText, wordAtCaret, Window
