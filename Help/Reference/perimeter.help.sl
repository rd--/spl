# perimeter

- _perimeter(anObject)_

Answer the perimeter of the two-dimensional _anObject_.

At `Circle`:

```
>>> Circle([0 0], 1).perimeter
2.pi
```

At `Rectangle`:

```
>>> Rectangle([-1 -1], [1 1]).perimeter
8
```

* * *

See also: arcLength, circumference, Circle, Rectangle

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Perimeter.html)
[2](https://reference.wolfram.com/language/ref/Perimeter.html)

Categories: Geometry
