# barlowDisharmonicity

- _barlowDisharmonicity(aFraction)_

Compute the disharmonicity of the interval _aFraction_.
The disharmonicity is the inverse of the harmonicity.

```
>>> [9/10 8/9 1/1].collect(
>>> 	barlowDisharmonicity:/1
>>> )
[-12 - 11/15, -8 - 1/3, 0]
```

* * *

See also: barlowHarmonicity, barlowIndigestibility
