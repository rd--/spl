# prefixes

- _prefixes(aSequence)_

Answer a List of all of the prefixes of _aSequence_.

```
>>> [1 2 3].prefixes
[1; 1 2 ; 1 2 3]

>>> 1:9.prefixes.collect(sum:/1)
[1 3 6 10 15 21 28 36 45]
```

* * *

See also: prefixSum, suffixes

References:
_Haskell_
[1](https://hackage.haskell.org/package/base/docs/Data-List.html#v:inits),
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/bslash)
