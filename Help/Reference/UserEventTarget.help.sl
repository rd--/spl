# UserEventTarget

`UserEventTarget` is a `Trait`.

A `UserEventTarget` must implement `eventListeners` to answer a `Dictionary`.

`UserEventTarget` implements two methods, `addEventListener` and `dispatchEvent`.

* * *

See also: addEventListener, dispatchEvent
