# isRegular

- _isRegular(aGraph)_

Answer `true` if _aGraph_ is regular, else `false`.
A graph is said to be regular of degree _r_ if all local degrees are the same number _r_.

```
>>> 4.completeGraph.isRegular
true
```

`vertexDegree` is uniform for regular graphs:

```
>>> 4.completeGraph.vertexDegree
[3 3 3 3]
```

* * *

See also: Graph, vertexDegree

References:
_Mathematica_
[1](https://mathworld.wolfram.com/RegularGraph.html)

Categories: Testing, Graph
