# radians

- _radians(aNumber)_

Answer an `Angle` representing _aNumber_ radians.

```
>>> 0.5.pi.radians
90.degrees
```

* * *

See also: Angle, degrees, radiansToDegrees

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Degree.html)
