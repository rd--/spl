# xyzToAdobeRgb

- _xyzToAdobeRgb(xyz)_

Convert from Cie _Xyz_ tristimulus values in _(0,1)_ to an Adobe _Rgb_ colourspace array in _(0,1)_.

```
>>> [0.25 0.40 0.10]
>>> .xyzToAdobeRgb
>>> .adobeRgbEncode
[0.5323 0.7377 0.2730]
```

* * *

See also: adobeRgbEncode, Colour, xyzToRgb

Categories: Colour
