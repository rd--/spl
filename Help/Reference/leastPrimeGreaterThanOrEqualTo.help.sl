# leastPrimeGreaterThanOrEqualTo

- _leastPrimeGreaterThanOrEqualTo(anInteger)_

Answer the next prime number after _anInteger_, or _anInteger_ if it is prime:

```
>>> 19.leastPrimeGreaterThanOrEqualTo
19

>>> (17393 + 1).leastPrimeGreaterThanOrEqualTo
17401
```

* * *

See also: nextPrime, previousPrime

References:
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/pco#dyadic)

Categories: Arithmetic
