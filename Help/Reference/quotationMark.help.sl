# " (quotationMark)

" is a syntax token, it is not an operator.

It is part of `String Syntax`.

```
>>> "Double Quoted String"
DoubleQuotedString(
	'Double Quoted String'
)

>>> "Double Quoted String"
>>> .isDoubleQuotedString
true
```

The name of this token is `quotationMark`.

* * *

See also: apostrophe, DoubleQuotedString, graveAccent, String

Guides: String Syntax

Unicode: U+0022 Quotation Mark

Categories: Syntax
