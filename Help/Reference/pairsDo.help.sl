# pairsDo

- _pairsDo(aSequence, aBlock:/2)_

Apply _aBlock_ to the items of _aSequence_ taken two at a time.
The iterative form of `pairsCollect`.

* * *

See also: pairsCollect

Categories: Enumerating
