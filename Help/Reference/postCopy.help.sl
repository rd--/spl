# postCopy

- _postCopy(anObject)_

When an `Object` is copied, a shallow copy is made, and then `postCopy` is called on the shallow copy.

Types implement any neccesary further copying required.

* * *

See also: copy, shallowCopy

Categories: Copying
