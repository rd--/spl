# degree

- _degree(aNumber)_

Answer _aNumber_ times _degree_, the constant representing the number of radians in one degree,
approximately equal to _0.01745_.

```
>>> 1.degree
(2.pi / 360)

>>> 360.degree
2.pi
```

One degree is divided into sixty _arc minutes_,
which are in turn divided into sixty _arc seconds_:

```
>>> 1.degree
60.arcMinute

>>> 1.degree
(60 * 60).arcSecond
```

Where supported `degree` is displayed as °.

* * *

See also: arcMinute, arcSecond, degrees, pi

Guides: Mathematical Constants

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Degree.html)
[2](https://reference.wolfram.com/language/ref/Degree.html)

Unicode: U+00b0 ° Degree Sign

Categories: Math, Constant
