# queryParameters

- _queryParameters(aUrl)_

Answer a `UrlQueryParameters` value holding the `query` component of _aUrl_.

```
>>> 'x://?p=i&q=j'.asUrl.queryParameters.associations
['p' -> 'i', 'q' -> 'j']
```

* * *

See also: query, Url, UrlQueryParameters

Categories: Network, Address
