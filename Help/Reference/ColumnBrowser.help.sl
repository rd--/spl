# ColumnBrowser

A `ColumnBrowser` is an interface element modelled on the Smalltalk _System Browser_.

A sequence of lists views allow traversing a tree structure from left to right.
Selecting an element in the leftmost list populates the list to the right, and so on.
Selecting an element in the rightmost list fills the text pane with a view of the selected entity.
The lists may include a filter panel to narrow the selection.

The status text may show additional information about the most recently selected entity.

* * *

See also: CategoryBrowser, MethodBrowser, MethodSignatureBrowser, TraitBrowser, TypeBrowser, SystemBrowser

Categories: Kansas
