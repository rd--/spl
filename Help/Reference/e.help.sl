# e

- _e(aNumber)_
- _e(aNumber, anotherNumber)_

In the unary case,
the number `e`,
also known as Euler’s number or Napier’s constant,
is a mathematical constant approximately equal to 2.71828.
It is the base of natural logarithms.

Answer _aNumber_ times e.

```
>>> 1.e
2.71828

>>> 2.e
5.43656

>>> 3.e
8.15485
```

`e` threads over lists:

```
>>> [0.5, 1.5].e
[1.35914, 4.07742]
```

_e_ raised to _iπ_ plus `one` is `zero`:

```
>>> 1.e ^ (1.pi * 0J1) + 1
0

>>> 1.pi.cos + (1.i * 1.pi.sin) + 1
0
```

In the binary case,
answer the scientific notation:

```
>>> 1.e(6)
1E6

>>> 1.e(-6)
1E-6
```

The binary form likewise threads over lists:

```
>>> [0 0.1 0.2 0.3 0.4 0.5 0.75 1.0].e(2)
[0 10 20 30 40 50 75 100]
```

_e_ is also a part of the `Scientific Notation` for `Number Literals`,
however in that context an upper case _E_ is ordinary.

* * *

See also: exp, log, pi

Guides: Mathematical Constants, Number Literals, Scientific Notation

References:
_Mathematica_
[1](https://mathworld.wolfram.com/e.html),
_Smalltalk_
5.6.8.2

Categories: Math, Constant
