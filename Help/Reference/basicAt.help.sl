# basicAt

- _basicAt(aList | aRecord, index)_

Unchecked lookup, answers nil for invalid indices.

* * *

See also: at, basicAtPut

Guides: Slot Access Syntax

Categories: Accessing
