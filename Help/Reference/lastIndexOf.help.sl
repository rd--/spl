# lastIndexOf

- _lastIndexOf(aCollection, anElement)_

Answer the index of the last occurence of _anObject_ within _aCollection_.
If _aCollection_ does not contain _anObject_, answer 0.

```
>>> [1 2 3 2 3].lastIndexOf(3)
5

>>> [1 2 3 2 3].lastIndexOf(4)
0
```

* * *

See also: indexOf

Categories: Accessing
