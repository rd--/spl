# SuperColliderProgramBrowser

A `ColumnBrowser` where:

- column one lists the kinds of SuperCollider programs the system knows of
- column two shows the initials of the authors of the programs of the selected kind
- column three shows the names of the programs by the selected author

When a program is selected the text pane shows the definition.
The context menu of the `TextEditor` can be used to play the program, and to reset the synthesiser when done.

* * *

See also: CategoryBrowser, MethodSignatureBrowser, TraitBrowser, TypeBrowser, SuperColliderProgramOracle, SystemBrowser

Categories: Kansas
