# isList

- _isList(anObject)_

Answer `true` if _anObject_ is a `List`.

```
>>> [1 2 3].isList
true
```

A `Tuple` is not a `List`:

```
>>> (1, 2, 3).isList
false
```

* * *

See also: List

Categories: Testing, Collection
