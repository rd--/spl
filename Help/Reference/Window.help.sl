# Window

A `Type` representing a window containing an Html document.
In a browser context, there is a separate Window object for each tab.

```
>>> system.window.isWindow
true
```

Methods are:

- `alert`: show alert
- `confirm`: request confirmation
- `document`: Html document
- `localStorage`: local storage
- `location`: location
- `name`: name
- `navigator`: navigator
- `prompt`: show prompt
- `sessionStorage`: session storage

* * *

See also: Navigator, System
