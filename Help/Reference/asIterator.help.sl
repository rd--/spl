# asIterator

- _asIterator(aCollection)_

Answer a `CollectionStream` on _aCollection_.

```
>>> let i = 1:9.asIterator;
>>> (i.next, i.next, i.next)
(1, 2, 3)
```

* * *

See also: CollectionStream, do, Iterator, Iterable, next, Stream

Categories: Converting
