# cache

- _cache(hasCache)_

Answer the cache `Dictionary` at _hasCache_.

The `system` cache:

```
>>> system
>>> .cache
>>> .isDictionary
true
```

The `system` cache holds the `preferences`:

```
>>> system
>>> .cache
>>> .includesKey('preferences')
true
```

* * *

See also: Cache, cached, caches, whenCached
