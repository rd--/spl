# Removeable

A `Collection` `Trait`.
To implement `Removeable` a `Type` must define `removeIfAbsent`.

* * *

See also: Collection, Extensible, remove, removeAll, removeAllFoundIn, removeAllSuchThat, removeIfAbsent

References:
_Smalltalk_
5.7.16

Categories: Collection trait
