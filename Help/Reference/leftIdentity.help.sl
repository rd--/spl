# leftIdentity

- _leftIdentity(anObject, anotherObject)_

Answer _anObject_.

```
>>> 'left'.leftIdentity('right')
'left'
```

Where supported `leftIdentity` is displayed as ⊣.

* * *

See also: identity, rightIdentity

References:
_Apl_
[1](https://aplwiki.com/wiki/Identity)

Unicode: U+22A3 ⊣ Left Tack
