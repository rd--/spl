# ) (rightParenthesis)

`)` is a syntax token, it is not an operator.

`)` is a part of `Tuple Syntax`,
where it closes a tuple that has been opened by `(`.

It is also a part of `Apply Syntax` and `Method Syntax`,
where it closes a tuple holding the arguments to a procedure.

The name of this syntax token is `rightParenthesis`.

* * *

See also: (

Guides: Apply Syntax, Method Syntax, Tuple Syntax
