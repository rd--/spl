# LsDupEach

- _LsDupEach(input, repeats)_

Duplicate each _input_ item _repeats_ number of times.

```
>>> LsDupEach(
>>> 	LsSeries(1, 3, 5),
>>> 	LsSeq([2 3], Infinity)
>>> ).upToEnd
[1 1 4 4 4 7 7 10 10 10 13 13]
```

* * *

See also: LsRemDup

Guides: Patterns and Streams

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Pdup.html)

Categories: Stream
