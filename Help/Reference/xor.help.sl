# xor

- _xor(aBoolean, anotherBoolean)_

Boolean exclusive or.
Answer the Boolean exclusive or of the receiver and operand.

Truth table:

```
>>> xor:/2.table([true false], [true false])
[false true; true false]
```

* * *

See also: &, &&, |, ||

References:
_Smalltalk_
5.3.3.12
