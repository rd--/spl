# HelpBrowser

A `ColumnBrowser` where:

- the first column indicates the _kind_ of help to look for
- the second column indicates the names of available help entries

When selected, the help text is shown in the text editor pane.

* * *

See also: HelpViewer, WorldMenu

Categories: Kansas
