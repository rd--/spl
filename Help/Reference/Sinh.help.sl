# Sinh

- _Sinh(aNumber)_

Answer the hyperbolic sine of _aNumber_.

```
>>> 1.4.Sinh
1.90430
```

* * *

See also: Cosh, sinh, Tanh

Categories: Ugen
