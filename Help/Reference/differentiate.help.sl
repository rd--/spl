# differentiate

- _differentiate(aCollection)_

```
>>> [1 3 6 10 15].differentiate
[1 2 3 4 5]
```

See also `differences`:

```
>>> [1 3 6 10 15].differences
[2 3 4 5]
```

* * *

See also: differences, integrate
