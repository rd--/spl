# valuesAndCounts

- _valuesAndCounts(aBag)_

Answer a `Dictionary` of `Association`s between values and counts, not sorted.

```
>>> [3 2 1 2 3 1 2 1 0].asIdentityBag.valuesAndCounts
[3 -> 2, 2 -> 3, 1 -> 3, 0 -> 1].asMap
```

* * *

See also: sortedElements

Categories: Accessing
