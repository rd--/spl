# fragment

- _fragment(aUrl)_

Answer a `String` having the fragment, also called the _hash_, component of the `Url`.

```
>>> 'A://B/C#D'.asUrl.fragment
'#D'
```

* * *

See also: host, hostName, href, pathName, port, protocol, query, Url

Categories: Network
