# pop

- _pop(aPriorityQueue)_

Remove and answer the item from the front of _aPriorityQueue_,
or `nil` if the queue is empty.

* * *

See also: PriorityQueue, removeFirst, removeLast

Categories: Accessing
