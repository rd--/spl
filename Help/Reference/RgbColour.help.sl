# RgbColour

- _RgbColour([r, g, b], alpha)_

A `Type` representing a color in the _Rgb_ color space,
using red, blue and green components,
with an `alpha` channel.

* * *

See also: asColour, Colour, HsvColour, srgbEncode

Guides: Colour Functions

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/RGBColor.html),
_W_
[1](https://en.wikipedia.org/wiki/RGB_color_model)
