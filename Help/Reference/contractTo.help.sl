# contractTo

- _contractTo(aString, anInteger)_

Answer _aString_ or a copy shortened by ellipsis to _anInteger_ places.

```
>>> 'A clear but rather long-winded summary'
>>> .contractTo(19)
'A clear ... summary'

>>> 'antidisestablishmentarianism'
>>> .contractTo(10)
'anti...ism'
```

* * *

See also: concisePrintString, printStringLimitedTo, String, truncateTo

Categories: Converting
