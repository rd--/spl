# unrankPermutation

- _unrankPermutation(r, n)_

Answer the _r_-th permutation in the lexicographic ordering of the symmetric group _n_.

```
>>> 17.unrankPermutation(4)
[3 4 2 1].asPermutation

>>> [1 3 5 2 4].permutationRank
10

>>> 10.unrankPermutation(5)
[1 3 5 2 4].asPermutation
```

* * *

See also: mixedRadixEncode, permutationRank

References:
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/acapdot#dyadic)
