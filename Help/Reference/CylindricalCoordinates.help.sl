# CylindricalCoordinates

- _CylindricalCoordinates(ρ, φ, z)_

The three components of `CylindricalCoordinates` are:

- the radial distance ρ (or r) is the Euclidean distance from the z-axis to _p_
- the azimuth φ (or theta) is the angle of the line from the origin to the projection of _p_ on the plane
- the axial coordinate z is the signed distance from the plane to the point _p_

* * *

See also: CartesianCoordinates, fromCylindricalCoordinates, SphericalCoordinates, toCylindricalCoordinates

References:
_Mathematica_
[1](https://mathworld.wolfram.com/CylindricalCoordinates.html),
_W_
[1](https://en.wikipedia.org/wiki/Cylindrical_coordinate_system)

Categories: Geometry, Type
