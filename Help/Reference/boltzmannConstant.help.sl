# boltzmannConstant

- _boltzmannConstant(aNumber)_

Answer _aNumber_ times the Boltzmann constant.

The Boltzmann constant (_k_) is the proportionality factor that relates the average relative thermal energy of particles in a gas with the thermodynamic temperature of the gas.

```
>>> 1.boltzmannConstant
1.380649E-23
```

* * *

See also: brunsConstant, e, pi, planckConstant, speedOfLight

References:
_W_
[1](https://en.wikipedia.org/wiki/Boltzmann_constant)
