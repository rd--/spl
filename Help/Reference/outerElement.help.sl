# outerElement

`outerElement` is the required method for a type to implement the trait `View`.

* * *

See also: Frame, SmallKansas, View

Categories: Kansas
