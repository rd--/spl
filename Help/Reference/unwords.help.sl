# unwords

- _unwords(aSequence)_

Answer a `String` made by joining together the items of _aSequence_ with separating spaces.

```
>>> ['Lorem' 'ipsum' 'dolor'].unwords
'Lorem ipsum dolor'

>>> ['foo' 'bar' '' 'baz'].unwords
'foo bar  baz'
```

* * *

See also: stringIntercalate, unlines, words

References:
_Haskell_
[1](https://hackage.haskell.org/package/base/docs/Prelude.html#v:unwords)

Categories: Converting
