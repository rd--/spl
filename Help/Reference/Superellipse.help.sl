# Superellipse

- _Superellipse(center, a, b, n)_

```
>>> Superellipse([0 0], 1, 1, 1).area
2

>>> Superellipse([0 0], 1, 1, 4).area
3.70815
```

* * *

See also: Circle, Ellipse, superellipseFunction

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Superellipse.html),
_W_
[1](https://en.wikipedia.org/wiki/Superellipse)
