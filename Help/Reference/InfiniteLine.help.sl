# InfiniteLine

- _InfiniteLine(aPoint, aVector)_

A `Type` to represent the infinite straight line passing through _aPoint_ in the direction _aVector_.

* * *

See also: HalfLine, Line

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/InfiniteLine.html)

Categories: Geometry
