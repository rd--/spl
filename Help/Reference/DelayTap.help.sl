# DelayTap

- _DelayTap(buffer, delayTime)_

`DelayTap` reads from a delay line.
C.f. `DelayWrite`.

- buffer: memory for delay line
- delayTime: delay time in seconds

* * *

See also: DelayWrite

Categories: Ugen
