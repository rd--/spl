# slotValueList

- _slotValueList(anObject)_

Answer a `List` of the slot values for _anObject_.

```
>>> 2/3.slotValueList
[2 3]
```

* * *

See also: perform, slotList, slotRead, slotNameList, slotWrite

Guides: Slot Access Syntax

Categories: Accessing, Reflection

Status: Experimental
