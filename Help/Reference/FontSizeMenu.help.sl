# FontSizeMenu

A simple font size selection menu.

When opened from the `WorldMenu` it edits the font of `SmallKansas`.

* * *

See also: FontMenu

Categories: Kansas
