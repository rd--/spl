# sortBlock

- _sortBlock(aSortedList)_

Answer the sort block of _aSortedList_.

* * *

See also: SortedList

References:
_Smalltalk_
5.7.17.10

Categories: Accessing
