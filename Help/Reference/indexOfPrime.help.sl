# indexOfPrime

- _indexOfPrime(anInteger)_

Answer the one-index of _anInteger_ in the list of prime numbers.
Also called `primePi`.

```
>>> 97.indexOfPrime
25

>>> 25.primesList.last
97
```

Inverse is `nthPrime`:

```
>>> 25.nthPrime
97
```

* * *

See also: nthPrime, primePi, primesList

References:
_W_
[1](https://en.wikipedia.org/wiki/List_of_prime_numbers)
