# WorldMenu

A menu that is accessible as the _context menu_ of the Small Kansas workspace.

~~~spl ui
system.smallKansas.WorldMenu(
	true,
	system.smallKansas.where
)
~~~

* * *

See also: SmallKansas

Categories: Kansas
