# minor

- _minor(aMatrix, i, j)_

The determinant of the submatrix obtained from _aMatrix_ by deleting row _i_ and column _j_.

```
>>> [1 4 7; 3 0 5; -1 9 11].minor(2, 3)
13
```

* * *

See also: minors

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Minor.html)

Categories: Math, Matrix
