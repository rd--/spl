# messageText

- _messageText(anError)_

Answer the message text of the exception.

```
>>> Error('message text').messageText
'message text'
```

* * *

See also: description, error, Error

References:
_Smalltalk_
5.5.1.4

Categories: Errors
