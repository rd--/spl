# Error

The value `Type` of an `Error`.

```
>>> Error('message text').name
'Error'

>>> Error('message text').messageText
'message text'

>>> Error('message text').description
'Error: message text'
```

* * *

See also: description, error, ifError, messageText, name, signal

Categories: Exception, Type
