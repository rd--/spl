# includeAll

- _includeAll(aCollection, anotherCollection)_

Include all the elements of _anotherCollection_ into _aCollection_.
Answer _anotherCollection_.

In general, any object responding to _do_ can be used as the second argument.

At `IdentityBag`:

```
>>> let c = [1 2 2 3 3 3];
>>> let r = IdentityBag();
>>> (r.includeAll(c), r.size)
(c, 6)
```

At `IdentitySet`:

```
>>> let c = [1 2 2 3 3 3];
>>> let r = IdentitySet();
>>> (r.includeAll(c), r.size)
(c, 3)
```

At `Set`:

```
>>> let c = [1 2 2.00001 3 3.00001 3.00002];
>>> let r = Set(~);
>>> (r.includeAll(c), r.size, r.asList)
(c, 3, [1 2 3])
```

At `List`, including `Range`:

```
>>> let c = 1:9;
>>> let r = [];
>>> (r.includeAll(c), r.size)
(c, 9)
```

At `List`, including `String`:

```
>>> let c = 'text';
>>> let r = [];
>>> (r.includeAll(c), r.size)
(c, 4)
```

At `Record`, including `Record`:

```
>>> let c = (y: 2, z: 3);
>>> let r = (x: 1);
>>> (r.includeAll(c), r)
(c, (x: 1, y: 2, z: 3))
```

* * *

See also: add, addAll, include

Categories: Adding
