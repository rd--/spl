# json

- _json(aResponse)_

Answer a `Promise` that will hold the Json value encoded in _aResponse_.

* * *

See also: blob, byteArray, Json, Response, text

Categories: Accessing
