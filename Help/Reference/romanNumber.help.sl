# romanNumber

- _romanNumber(aString)_

Answer the integer described by the roman numeral _aString_.

```
>>> 'MMXXIII'.romanNumber
2023
```

* * *

See also: printStringRoman

Categories: Converting
