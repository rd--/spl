# isDigit

- _isDigit(aCharacter)_

Answer `true` if _aCharacter_ represents a digit, else `false`.
There are ten digits, 0 1 2 3 4 5 6 7 8 and 9.

_3_ is a digit:

```
>>> '3'.isDigit
true
```

_x_ is not a digit:

```
>>> 'x'.asCharacter.isDigit
false
```

Comma is not a digit:

```
>>> ','.isDigit
false
```

* * *

See also: Character, isLetter

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/DigitQ.html),
_Smalltalk_
5.3.4.7

Categories: Testing
