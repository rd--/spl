# withoutFactorsOfTwo

- _withoutFactorsOfTwo(aFraction)_

Answer _aFraction_ with any factors of two deleted.

```
>>> 3/4.withoutFactorsOfTwo
3/1

>>> 7/6.withoutFactorsOfTwo
7/3

>>> 6/5.withoutFactorsOfTwo
3/5
```

* * *

See also: primeFactors

Categories: Math
