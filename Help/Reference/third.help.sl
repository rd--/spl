# third

- _third(aSequence)_

Answer the third element of _aSequence_.

```
>>> 1:6.third
3

>>> 6:-1:1.third
4
```

* * *

See also: first, fourth, last, middle, second

Categories: Accessing
