# asTuple

- _asTuple(anObject)_

Composite types may provide an `asTuple` method to answer a `Tuple` form of the value.

At `List`:

```
>>> [1 3 5].asTuple
(1, 3, 5)
```

* * *

See also: asList

Categories: Converting
