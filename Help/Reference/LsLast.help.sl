# LsLast

- _LsLast(input)_

Answer the last element of _input_.
There is no last element of an infinite Stream.

```
>>> LsLast(LsSeries(1, 1, 9))
9
```

* * *

See also: last

Categories: Stream
