# synodicMonths

- _synodicMonths(aNumber | aDuration)_

The synodic month is the average period of the Moon’s orbit with respect to the line joining the Sun and Earth

```
>>> 1.synodicMonths
29.days + 12.hours + 44.minutes + 2.9.seconds
```

* * *

See also: Duration, siderealMonths

References:
_W_
[1](https://en.wikipedia.org/wiki/Lunar_month#Synodic_month)
