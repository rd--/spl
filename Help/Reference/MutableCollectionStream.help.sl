# MutableCollectionStream

`MutableCollectionStream` is a `Type` that implements the `Stream`, `PositionableStream` and `WriteStream` traits.

* * *

See also: asStream, asWriteStream, PositionableStream, Stream, WriteStream

Categories: Collection, Type
