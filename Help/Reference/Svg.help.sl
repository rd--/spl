# Svg

- _Svg(aString)_

A `Type` holding a Scalable Vector Graphics image.

* * *

See also: asGreyscaleSvg, contents, parseSvg, SvgViewer

Categories: Graphics
