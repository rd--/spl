# reducedPermutation

- _reducedPermutation(aSequence)_

Answer the permutation pattern of _aSequence_,
which must not have duplicate elements.

```
>>> [7 3 5].reducedPermutation
[3 1 2]

>>> [2 6 9 1 3].reducedPermutation
[2 4 5 1 3]
```

* * *

See also: permutationHasPattern, permutationPatternPositions
