# Times

- _Times(aNumber, anotherNumber)_

Answer the product of _aNumber_ and _anotherNumber_.
`Times` is an alias for `*`.

At `SmallFloat`:

```
>>> Times(3, 4)
12
```

Quiet and quieter pink noise:

```
Times(
	PinkNoise(),
	[10 25].Reciprocal
)
```

* * *

See also: +, *, Divide, Minus, MulAdd, Plus, Power

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/Times.html)

Categories: Ugen
