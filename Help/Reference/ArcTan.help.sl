# ArcTan

- _ArcSin(aNumber)_

Arc tangent.

At `SmallFloat`:

```
>>> 1.ArcTan
0.25.pi
```

* * *

See also: arcTan, ArcCos

Categories: Math, Ugen, Trigonometry
