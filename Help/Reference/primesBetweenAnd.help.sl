# primesBetweenAnd

- _primesBetweenAnd(min, max)_

Answer the `List` of prime numbers that lie between _iMin_ and _iMax_.

```
>>> 1.primesBetweenAnd(100)
[
	2 3 5 7 11 13 17 19 23 29
	31 37 41 43 47 53 59 61 67 71
	73 79 83 89 97
]
```

If there are no primes answer the empty list:

```
>>> 14.primesBetweenAnd(16)
[]
```

* * *

See also: primesList, primesUpTo
