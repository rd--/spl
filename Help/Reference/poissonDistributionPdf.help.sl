# poissonDistributionPdf

- _poissonDistributionPdf(x, lambda)_

Answer the probability density function for the Poisson distribution.

Plot over the positive integers:

~~~spl svg=A
let lambda = 35;
0:60.functionPlot { :x |
	x.poissonDistributionPdf(lambda)
}
~~~

![](sw/spl/Help/Image/poissonDistributionPdf-A.svg)

* * *

See also: normalDistributionPdf

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PoissonDistribution.html),
_NIST_
[1](https://www.itl.nist.gov/div898/handbook/eda/section3/eda366j.htm)
_W_
[1](https://en.wikipedia.org/wiki/Poisson_distribution)
