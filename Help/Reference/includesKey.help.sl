# includesKey

- _includesKey(aDictionary, key)_

Answer whether _aDictionary_ has a key equal to _key_.

```
>>> (Italie: 'Rome', France: 'Paris').includesKey('France')
true
```

_Rationale:_
The indices of dictionaries are called keys.
`includesKey` is another name for `includesIndex`,
and `keys` is another name for `indices`.

* * *

See also: at, Dictionary, includesIndex, Indexable, keys

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/KeyExistsQ.html)
_Smalltalk_
5.7.2.7

Categories: Testing
