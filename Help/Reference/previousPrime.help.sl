# previousPrime

- _previousPrime(anInteger)_

Answer the greatest prime number less than _anInteger_.

Compute the last prime before 50:

```
>>> 50.previousPrime
47
```

If _anInteger_ is prime, answer the prime with index one less:

```
>>> 1000.previousPrime
997

>>> 997.previousPrime
991

>>> (991.primePi, 997.primePi)
(167, 168)
```

* * *

See also: isPrime, nextPrime
