# localTimeZoneOffset

- _localTimeZoneOffset(aSystem)_

Answer a `Duration` giving the local time offset from _Greenwich Mean Time_.

```
>>> system
>>> .localTimeZoneOffset
>>> .hours
>>> .betweenAnd(-24, 24)
true
```

* * *

See also: Duration, System
