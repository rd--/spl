# paragraphAtCaret

- _paragraphAtCaret(aDocument | aWindow)_

Answers the paragraph that the caret is placed within.

A paragraph is delimited by blank lines.

* * *

See also: paragraphAtIndex, wordAtCaret
