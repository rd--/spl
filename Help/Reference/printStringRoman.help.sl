# printStringRoman

- _printStringRoman(anInteger)_

Answer the roman numeral notation for _anInteger_.

```
>>> 2023.printStringRoman
'MMXXIII'
```

* * *

See also: romanNumber

Categories: Converting
