# fnv1aHash

- _fnv1aHash(aByteArray)_

Answer the 32-Bit Fowler-Noll-Vo hash function of _aByteArray_.

```
>>> 'FNV-1a'.asciiByteArray.fnv1aHash
3973616866

>>> 'Fowler-Noll-Vo'.asciiByteArray.fnv1aHash
354390154
```

* * *

See also: *, bitXor, isBinary

References:
_W_
[1](https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function)
