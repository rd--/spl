# rightIdentity

- _rightIdentity(anObject, anotherObject)_

Answer _anotherObject_.

```
>>> 'left'.rightIdentity('right')
'right'
```

Where supported `rightIdentity` is displayed as ⊢.

* * *

See also: leftIdentity, identity

References:
_Apl_
[1](https://aplwiki.com/wiki/Identity)

Unicode: U+22A2 ⊢ Right Tack

Categories: Functions
