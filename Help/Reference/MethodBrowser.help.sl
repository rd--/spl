# Method Browser

A `ColumnBrowser` where:

- column one lists the qualified `Method` names the system knows of
- column two shows the `Trait`s and `Type`s that implement the selected method

When a `Trait` or `Type` is selected the text pane shows the definition of the method.

The _Status Text_ shows if the selected value is a `Trait` or a `Type`.

* * *

See also: CategoryBrowser, MethodSignatureBrowser, TraitBrowser, TypeBrowser, SystemBrowser

Categories: Kansas
