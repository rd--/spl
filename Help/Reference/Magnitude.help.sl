# Magnitude

A `Trait` defining comparison operators.
Objects that are magnitudes should implement `=`.

The method that must be defined to implement this trait is `<`.

`Magnitude` implements: `<`, `>`, `<=`, `>=`, `betweenAnd`, `min`, `max`, `minMax`.

* * *

See also: <=, >, >=, <=>, betweenAnd, boole, clamp, min, max

References:
_Smalltalk_
5.6.1,
_Swift_
[1](https://developer.apple.com/documentation/swift/comparable)

Categories: Comparison, Trait
