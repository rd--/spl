# isPermutationOf

- _isPermutationOf(aSequence, anotherSequence)_

Answer `true` if _anotherSequence_ is a permutation of _aSequence_, else `false`.

```
>>> [1 2 3 4].isPermutationOf([4 3 2 1])
true

>>> [1 2 3 5].isPermutationOf([1 2 3 4])
false
```

* * *

See also: Permutation

Categories: Permutations, Testing
