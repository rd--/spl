# lightnessCie

- _lightnessCie(y, yn)_

Return the Lightness _L*_ of given luminance _y_ using given reference white luminance _yn_ as per Cie 1976 recommendation.

```
>>> 12.1972.lightnessCie(100)
41.5278
```

* * *

See also: Colour

Guides: Colour Functions

References:
_Python_
[1](https://colour.readthedocs.io/en/latest/generated/colour.colorimetry.lightness_CIE1976.html)
