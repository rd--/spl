# permutationRuns

- _permutationRuns(aPermutation)_

Answer the ascending runs of _aPermutation_.

```
>>> [2 4 1 3].permutationRuns
[2 4; 1 3]

>>> [6 1 7 3 4 5 2].permutationRuns
[6; 1 7; 3 4 5; 2]

>>> [1 2 3 4].permutationRuns
[[1 2 3 4]]

>>> [4 3 2 1].permutationRuns
[4; 3; 2; 1]

>>> [1].permutationRuns
[[1]]
```

* * *

See also: orderedSubstrings, Permutation

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PermutationRun.html),
_Sage_
[1](https://doc.sagemath.org/html/en/reference/combinat/sage/combinat/permutation.html#sage.combinat.permutation.Permutation.runs)

Categories: Permutation
