# circularPartitionsDrawing

- _circularPartitionsDrawing(aList)_

Answer a `LineDrawing` showing a circular drawing of the partitions at _aList_,
which should each sum to the same value.

`zero`,
and equivalently the period,
points upwards,
and divisions increase clockwise:

~~~spl svg=A
6.integerPartitions
.circularPartitionsDrawing
~~~

![](sw/spl/Help/Image/circularPartitionsDrawing-A.svg)

* * *

See also: integerPartitions, integerPartitionsTable, rectangularPartitionsDrawing
