# copyUpTo

- _copyUpTo(aSequence, anElement)_

Answer all elements up to but not including anElement.
If there is no such element, answer a copy of _aSequence_.

* * *

See also: copy

Categories: Copying
