# CategoryDictionary

A two-level dictionary for categorizing names.

The first level is called the _domain_ and the second level the _category_.

* * *

See also: Dictionary, Record

Categories: Collection, Type
