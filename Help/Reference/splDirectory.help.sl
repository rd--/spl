# splDirectory

- _splDirectory(aSystem)_

Answer the absolute file path of the Spl directory.
This reads the 'SPL_DIR' environment variable.

```
>>> system.splDirectory
'/home/rohan/sw/spl'
```

`splFileName` locates files relative to this directory.

* * *

See also: environmentVariable, splFileName
