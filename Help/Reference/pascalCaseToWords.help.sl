# pascalCaseToWords

- _pascalCaseToWords(aString)_

Pascal case names capitalize each word, including the first word.

```
>>> 'PascalCaseToWords'.pascalCaseToWords
'Pascal Case To Words'
```

* * *

See also: camelCaseToWords, RegExp
