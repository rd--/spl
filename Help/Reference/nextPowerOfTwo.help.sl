# nextPowerOfTwo

- _nextPowerOfTwo(anInteger)_

Answers the next power of two greater than or equal to _anInteger_.

```
>>> 300.nextPowerOfTwo
512

>>> 512.nextPowerOfTwo
512
```

* * *

See also: isPowerOfTwo, nextPowerOf

Categories: Truncation, Rounding
