# days

- _days(aDuration | aNumber)_

Answer the number of complete days in _aDuration_,
or construct a `Duration` value holding _aNumber_ days.

```
>>> 5.days.minutes
7200

>>> 3.days.hours
(3 * 24)

>>> 7.days.weeks
1
```

* * *

See also: asSeconds, Duration, hours, seconds, minutes, weeks

Categories: Time, Type
