# wythoffLower

- _wythoffLower(anInteger)_

Answer the _anInteger_-th element of the _lower Wythoff sequence_,
c.f. OEIS A000201:

```
>>> 1:19.collect(wythoffLower:/1)
[1 3 4 6 8 9 11 12 14 16 17 19 21 22 24 25 27 29 30]
```

* * *

See also: wythoffArray, wythoffPair, wythoffUpper

References:
_OEIS_
[1](https://oeis.org/A000201)

Categories: Math
