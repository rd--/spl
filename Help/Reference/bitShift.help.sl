# bitShift

- _bitShift(aNumber, anInteger)_

Left shift, towards most significant:

```
>>> 2.bitShift(3)
16
```

Negative values shift right, towards least significant:

```
>>> 16.bitShift(-3)
2
```

* * *

See also: <<, >>, bitShiftLeft, bitShiftRight

References:
_Smalltalk_
5.6.5.8

Categories: Bits, Binary
