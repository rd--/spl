# Distort

- _Distort(aNumber)_

Nonlinear distortion.
Distortion increases with amplitude:

```
(SinOsc(500, 0) * XLine(0.1, 10, 10)).Distort * 0.1
```

* * *

Categories: Ugen
