# HalfPlane

- _HalfPlane(aLine, aVector)_

A `Type` that represents the half-plane bounded by _aLine_ and extended in the direction _aVector_.

* * *

See also: InfinitePlane

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Half-Plane.html)
[2](https://reference.wolfram.com/language/ref/HalfPlane.html)
_W_
[1](https://en.wikipedia.org/wiki/Half-space_(geometry))

Categories: Geometry
