# << (lessThanSignLessThanSign)

- _alpha << beta_ ⟹ _bitShiftLeft(alpha, beta)_

The operator form or `bitShiftLeft`.

```
>>> 3 << 4
48

>>> 2r000011 << 4
2r110000

>>> 3.bitShiftLeft(4)
48
```

Where supported `<<` is displayed as ≪.

The name of this operator is `lessThanSignLessThanSign`.

* * *

See also: >>, bitShift, bitShiftLeft, bitShiftRight

Unicode: U+226A ≪ Much Less-Than

Categories: Bits, Binary
