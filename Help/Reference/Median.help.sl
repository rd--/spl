# Median

- _Median(length, in)_

Median filter.
Returns the median of the last length input points. This non linear filter is good at reducing impulse noise from a signal.

- length: number of input points in which to find the median. Must be an odd number from 1 to 31. If length is 1 then Median has no effect.
- in: input signal to be processed

A signal with impulse noise.

```
Saw(500) * 0.1 + (Dust2(100) * 0.9)
```

After applying median filter:

```
let z = Saw(500) * 0.1 + (Dust2(100) * 0.9);
Median(3, z)
```

The median length can be increased for longer duration noise.

A signal with longer impulse noise:

```
Saw(500) * 0.1 + (Lpz1(Dust2(100) * 0.9))
```

Length three does not help here because the impulses are two samples long.

```
let z = Saw(500) * 0.1 + (Lpz1(Dust2(100) * 0.9));
Median(3, z)
```

Length five does better:

```
let z = Saw(500) * 0.1 + (Lpz1(Dust2(100) * 0.9));
Median(5, z)
```

Long Median filters begin chopping off the peaks of the waveform:

```
let x = SinOsc(1000, 0) * 0.1;
XFade2(x, Median(31, x), MouseX(-1, 1, 0, 0.2), 1)
```

Another noise reduction application:

```
WhiteNoise() + SinOsc(800, 0) * 0.1
```

Use Median filter for high frequency noise:

```
let z = WhiteNoise() + SinOsc(800, 0) * 0.1;
Median(31, z)
```

Use LeakDc for low frequency noise:

```
let z = WhiteNoise() + SinOsc(800, 0) * 0.1;
LeakDc(Median(31, z), 0.9)
```

* * *

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Median.html)

Categories: Ugen
