# then

- _then(aPromise, aBlock:/1)_

Schedules _aBlock_ to be called when _aPromise_ resolves, with the value it has resolved to.
If the `Promise` rejects an `error` is raised.
The answer is an equivalent `Promise` object to _aPromise_.

* * *

See also: finally, onRejection, Promise, thenElse

References
_Tc39_
[1](https://tc39.es/ecma262/multipage/control-abstraction-objects.html#sec-promise.prototype.then)

Categories: Scheduling
