# doWhileFalse

- _doWhileFalse(aBlock:/0, conditionBlock:/0)_

Conditional evaluation.
Evaluate _aBlock_ once, then again as long the value of _conditionBlock_ is false.

* * *

See also: doWhileTrue, if, ifFalse, ifNil, ifTrue, whileFalse, whileTrue

Categories: Evaluating
