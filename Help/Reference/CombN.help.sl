# CombN

- _CombN(in, maxdelaytime=0.2, delaytime=0.2, decaytime=1)_

Comb filter.

* * *

See also: CombC

Categories: Ugen, Filter
