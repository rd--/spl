# isSingleton

- _isSingleton(aGraph)_

Answer `true` if _aGraph_ has one vertex and no edges.

The singleton graph is the graph consisting of a single isolated node with no edges.
It is therefore the empty graph on one node.
It is commonly denoted _K1_ (i.e., the complete graph on one node).

```
>>> Graph([1], []).isSingleton
true

>>> 1.completeGraph.isSingleton
true
```

* * *

See also: completeGraph, Graph, isEmpty

References:
_Mathematica_
[1](https://mathworld.wolfram.com/SingletonGraph.html)

Categories: Testing, Graph
