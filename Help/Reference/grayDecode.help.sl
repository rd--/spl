# grayDecode

- _grayDecode(anInteger)_

The inverse of `grayEncode`.

Decode encoded first few integers:

```
>>> [
>>> 	0   1  3  2
>>> 	6   7  5  4
>>> 	12 13 15 14
>>> 	10 11  9  8
>>> ].collect(grayDecode:/1)
[0 .. 15]
```

* * *

See also: Binary, bitShiftRight, bitXor, grayEncode
