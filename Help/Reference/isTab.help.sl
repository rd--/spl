# isTab

- _isTab(aCharacter)_

Answer `true` if _aCharacter_ is a tab.

```
>>> '\t'.isTab
true

>>> 9.asCharacter.isTab
true
```

* * *

See also: Character, isCarriageReturn, isFormFeed, isLineFeed, isSeparator, isSpace

Categories: Testing
