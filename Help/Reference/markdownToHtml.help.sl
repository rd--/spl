# markdownToHtml

- _markdownToHtml(aString)_

Convert a string holding _CommonMark_ to a string holding _Html_.

```
>>> '# A Heading'.markdownToHtml.lines[1]
'<h1>A Heading</h1>'

>>> '# H1\n\nP 1\n\nP 2'.markdownToHtml
'<h1>H1</h1>\n<p>P 1</p>\n<p>P 2</p>\n'
```

* * *

See also: Markdown, String

References:
_CommonMark_
[1](https://spec.commonmark.org/)
