# rho

- _rho(aPoint)_

Another name for `r`:

```
>>> PolarCoordinates(1, 0).rho
1
```

Where supported `rho` is displayed as ρ.

* * *

See also: r, radius

Guides: Geometry Types

Unicode: U+03C1 ρ Greek Small Letter Rho

Categories: Accessing, Geometry
