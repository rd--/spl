# dividesImmediately

- _dividesImmediately(aNumber, anotherNumber)_

Answer `true` if _aNumber_ is _anotherNumber_ times a prime number.

Twelve divided by four is three, and three is a prime number:

```
>>> 12.dividesImmediately(4)
true
```

Twelve divided by three is four, and four is not a prime number:

```
>>> 12.dividesImmediately(3)
false
```

At `Fraction`:

```
>>> 15/7.dividesImmediately(3/7)
true
```

* * *

See also: /, Fraction, isPrime
