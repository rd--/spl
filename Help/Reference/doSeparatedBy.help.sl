# doSeparatedBy

- _doSeparatedBy(aCollection, elementBlock:/1, separatorBlock:/0)_

Evaluate the _elementBlock_ for all elements in _aCollection_, and evaluate the _separatorBlock_ between.

* * *

See also: do

References:
_Smalltalk_
5.7.1.14

Categories: Enumerating
