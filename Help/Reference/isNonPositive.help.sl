# isNonPositive

- _isNonPositive(aNumber)_

Answers `true` if _aNumber_ is negative or `zero`, else `false`.

```
>>> [-1 0 1].collect(isNonPositive:/1)
[true true false]
```

* * *

See also: negated, isNegative, isNonNegative, isPositive

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Nonpositive.html)

Categories: Math
