# Frame

A `Frame` is the `Type` of the values that appear as `Window`s in `SmallKansas`.

* * *

See also: SmallKansas, Window

Categories: Kansas
