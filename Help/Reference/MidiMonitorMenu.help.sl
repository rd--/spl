# MidiMonitorMenu

A simple menu to select a midi port to open a midi monitor window on.

* * *

See also: MidiPortBrowser, SmallKansas

Categories: Kansas
