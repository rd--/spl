# simplify

- _simplify(anObject)_

Simplify an _anObject_ in place.
Answer _anObject_.

At `Fraction`:

```
>>> let r = ReducedFraction(2, 4);
>>> r.simplify;
>>> r
1/2
```

* * *

See also: Fraction, ReducedFraction, simplify
