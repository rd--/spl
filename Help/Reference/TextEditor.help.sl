# TextEditor

A `Type` of `View` that holds a simple text editor.

* * *

See also: SmallKansas, View

Categories: Kansas
