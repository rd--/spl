# completeKaryTree

- _completeKaryTree(n, k)_

Answer a complete _k_-ary tree with _n_ levels.

A complete binary tree with four levels:

~~~spl svg=A
completeKaryTree(4, 2).treePlot
~~~

![](sw/spl/Help/Image/completeKaryTree-A.svg)

A complete ternary tree with three levels:

~~~spl svg=B
completeKaryTree(3, 3).treePlot
~~~

![](sw/spl/Help/Image/completeKaryTree-B.svg)

* * *

See also: karyTree, Tree

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/CompleteKaryTree.html)
