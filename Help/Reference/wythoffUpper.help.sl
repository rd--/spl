# wythoffUpper

- _wythoffUpper(anInteger)_

Answer the _anInteger_-th element of the _upper Wythoff sequence_,
c.f. OEIS A001950.

```
>>> 1:19.collect(wythoffUpper:/1)
[
	 2  5  7 10 13 15 18 20 23 26
	28 31 34 36 39 41 44 47 49
]
```

* * *

See also: wythoffArray, wythoffLower, wythoffPair

References:
_OEIS_
[1](https://oeis.org/A001950)
