# circumcenter

- _circumcenter(aMatrix | aTriangle)_

The `circumcenter` is the `center` of the `circumcircle`.

```
>>> let t = [0 0; 0 1; 1 0].asTriangle;
>>> let c = t.circumcircle;
>>> (c.center, t.circumcenter)
([0.5 0.5], [0.5 0.5])
```

* * *

See also: center, circumcircle, circumradius

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Circumcenter.html)

Categories: Geometry
