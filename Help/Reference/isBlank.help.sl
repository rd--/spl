# isBlank

- _isBlank(aCharacter)_

Answer `true` if _aCharacter_ is a space (`isSpace`) or a tab (`isTab`).

```
>>> ' '.isBlank
true

>>> 9.asCharacter.isBlank
true
```

* * *

See also: isBlankLine, isSpace, isTab

Categories: Testing
