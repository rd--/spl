# asGraymap

- _asGraymap(aMatrix)_

Answer `Graymap` of `rescale` of _aMatrix_.

A gradient, rescaled from _(1,10000)_ to _(0,1)_:

~~~spl png=A
[100 100].iota.asGraymap
~~~

![](sw/spl/Help/Image/asGraymap-A.png)

* * *

See also: Bitmap, Graymap, Image, matrixPlot, Svg

Categories: Graphics
