# permutationPeaks

- _permutationPeaks(aPermutation)_

Answer the indices of the peaks of a permutation.
A peak has lower values at each side.

```
>>> [1 3 2 4 5].permutationPeaks
[2]
```

The first and last elements cannot be peaks:

```
>>> [4 1 3 2 6 5].permutationPeaks
[3 5]
```

The empty permutation has no peaks:

```
>>> [].permutationPeaks
[]
```

* * *

See also: Permutation

References:
_Sage_
[1](https://doc.sagemath.org/html/en/reference/combinat/sage/combinat/permutation.html#sage.combinat.permutation.Permutation.peaks)
