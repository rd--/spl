# binaryDistance

- _binaryDistance(u, v)_

Answer the angular binary distance between vectors _u_ and _v_.

`binaryDistance` is equivalent to _(u = v).boole_.

Binary distance between numeric vectors:

```
>>> [1 2 3].binaryDistance([3 5 7])
0

>>> let u = [1 2 3];
>>> u.binaryDistance(u)
1
```

* * *

See also: =, boole

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/BinaryDistance.html),

Categories: Distance
