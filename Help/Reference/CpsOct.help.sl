# CpsOct

- _CpsOct(aNumber)_

Convert cycles per second to decimal octaves.
Inverse of `OctCps`.

```
>>> 440.CpsOct
(4 + 9/12)
```

* * *

See also: CpsMidi, CpsRatio, OctCps

Categories: Math, Ugen
