# indefiniteArticle

- _indefiniteArticle(aString)_

Answer the indefinite article of _aString_, which is presumably a noun or noun phrase.

```
>>> 'List'.indefiniteArticle
'a'
```

The article depends on the first letter of the noun:

```
>>> 'Object'.indefiniteArticle
'an'
```

* * *

See also: withIndefiniteArticle

Categories: Printing
