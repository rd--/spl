# TableViewer

- _TableViewer(aSmallKansas, aString, aList)_

Table viewer.
Add a window to _aSmallKansas_ with title _aString_ displaying a table of the matrix at _aList_.

~~~spl smallKansas
system.smallKansas.TableViewer(
	'Luoshu',
	[4 9 2; 3 5 7; 8 1 6]
)
~~~

* * *

See also: PngViewer, SmallKansas, SvgViewer

Categories: Kansas
