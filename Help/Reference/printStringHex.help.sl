# parseStringHex

- _parseStringHex(anInteger)_

Answer the hexadecimal notation of _anInteger_.

```
>>> 58909.printStringHex
'16rE61D'
```

This is equivalent to `printString` with radix set to sixteen:

```
>>> 58909.printString(16)
'16rE61D'
```

* * *

See also: Integer, printString, String
