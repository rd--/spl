# octave

- _octave(aTuning)_

Answer the _ratio_ of the _octave_ value at _aTuning_.

This is ordinarily, but not necessarily, two.

* * *

See also: Tuning

Categories: Accessing, Tuning
