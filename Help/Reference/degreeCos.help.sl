# degreeCos

- _degreeCos(aNumber)_

Answer the cosine of _aNumber_ taken as an angle in degrees.

```
>>> 90.degreeCos
0

>>> 90.degrees.cos
0
```

* * *

See also: cos, degreeSin, sin

Categories: Trigonometry
