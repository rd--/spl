# maxBy

- _maxBy(p, q, aBlock:/1)_

Answer which of _p_ or _q_ answers as `max` according to _aBlock_.

```
>>> -2J2.maxBy(4J1, abs:/1)
4J1

>>> [1 2 3].maxBy([4 5], size:/1)
[1 2 3]
```

* * *

See also: <, max, maximalBy, minBy, min
