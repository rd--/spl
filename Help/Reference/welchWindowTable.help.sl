# welchWindowTable

- _welchWindowTable(anInteger, alpha=1)_

Answer a `List` describing a _Welch window_ of the indicated size.

~~~spl svg=A
128.welchWindowTable(1).linePlot
~~~

![](sw/spl/Help/Image/welchWindowTable-A.svg)

* * *

See also: hammingWindowTable, hannWindowTable, kaiserWindowTable, welchWindow

Guides: Window Functions
