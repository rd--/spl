# with

- _with(anObject, aBlock:/1)_

Evaluate _aBlock_ with _anObject_ as argument and answer the answer of _aBlock_.
`with` is an alias for `in`.

_Rationale:_
The _with_ methods in Smalltalk are all class methods and do not translate to Spl.

* * *

See also: also, in, value

References:
_Kotlin_
[1](https://kotlinlang.org/docs/scope-functions.html#with)
[2](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/with.html)

Categories: Behaviour
