# LsCauchy

- _LsCauchy(mean, spread, length)_

Random values that follow a Cauchy distribution.

~~~spl svg=A
LsCauchy(0, 1 / 9, 99, Sfc32(280142))
.upToEnd
.linePlot
~~~

![](sw/spl/Help/Image/LsCauchy-A.svg)

* * *

See also: LsBeta, LsBrown, LsWhite

Guides: Patterns and Streams

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Pcauchy.html)

Categories: Stream
