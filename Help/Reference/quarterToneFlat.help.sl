# quarterToneFlat

- _quarterToneFlat(aNumber)_

Answer _aNumber - 0.05_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 7.quarterToneFlat
6.95
```

Where supported `quarterToneFlat` is displayed as 𝄳.

* * *

See also: degreeToKey, flat, quarterToneSharp, Scale, sharp

Unicode: U+1D133 𝄳 Musical Symbol Quarter Tone Flat
