# eulerGamma

- _eulerGamma(aNumber)_

_1.eulerGamma_ is Euler’s constant γ, with numerical value ~0.57722.

```
>>> 1.eulerGamma
0.57722
```

* * *

See also: e, pi

Guides: Mathematical Constants

References:
_Mathematica_
[1](http://mathworld.wolfram.com/Euler-MascheroniConstant.html)
[2](https://reference.wolfram.com/language/ref/EulerGamma.html)
