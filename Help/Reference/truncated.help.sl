# truncated

- _truncated(aNumber)_

Answer the integer nearest _aNumber_ toward `zero`.

```
>>> 1.25.truncated
1

>>> -1.25.truncated
-1
```

* * *

See also: integerPart, rounded, truncateTo

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Truncate.html),
_Smalltalk_
5.6.2.41

Categories: Truncating, Rounding
