# isPositiveInteger

- _isPositiveInteger(aNumber)_

Answers `true` if _aNumber_ is a positive integer.

```
>>> 23.isPositiveInteger
true

>>> 0.isPositiveInteger
false

>>> 1.pi.isPositiveInteger
false
```

* * *

See also: isInteger, isOdd, isPositiveOddInteger, isNumber

Categories: Predicate, Testing
