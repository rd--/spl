# Log10

- _Log10(aNumber)_

Base ten logarithm.

```
>>> Log10(Power(10, 6))
6

>>> 5.Log10
0.69897
```

* * *

See also: Exp, Log, Log2, Power

Categories: Ugen
