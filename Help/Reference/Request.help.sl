# Request

- _Request(aString)_

A `Type` representing a resource request.

```
>>> Request('http://cern.ch/').url
'http://cern.ch/'
```

* * *

See also: fetch, Url

Guides: Network Functions

Categories: Network
