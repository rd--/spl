# ulamSpiralMatrix

- _ulamSpiralMatrix(anInteger)_

The Ulam spiral matrix is a counter-clockwise evolute at first rightwards.

The 5×5 Ulam spiral:

```
>>> 5.ulamSpiralMatrix
[
	17 16 15 14 13;
	18  5  4  3 12;
	19  6  1  2 11;
	20  7  8  9 10;
	21 22 23 24 25
]
```

The 7×7 Ulam spiral:

```
>>> 7.ulamSpiralMatrix
[
	37 36 35 34 33 32 31;
	38 17 16 15 14 13 30;
	39 18  5  4  3 12 29;
	40 19  6  1  2 11 28;
	41 20  7  8  9 10 27;
	42 21 22 23 24 25 26;
	43 44 45 46 47 48 49
]
```

* * *

See also: spiralMatrix

References:
_Mathematica_
[1](https://mathworld.wolfram.com/PrimeSpiral.html),
_W_
[1](https://en.wikipedia.org/wiki/Ulam_spiral)
