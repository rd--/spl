# decrement

- _decrement(aNumber)_

Answer _aNumber_ minus `one`.

At `SmallFloat`:

```
>>> -5.decrement
-6

>>> 0.decrement
-1
```

At `Complex`:

```
>>> 4J2.decrement
3J2
```

Threads over lists:

```
>>> [-5 1 0 5].decrement
[-6 0 -1 4]
```

* * *

See also: +, Number

References:
_J_
[1](https://code.jsoftware.com/wiki/Vocabulary/ltco)
