# wilsonHeight

- _wilsonHeight(aFraction)_

The `sum` of the `primeFactors` of the `benedettiHeight`.

```
>>> 81/64.benedettiHeight
5184

>>> 81/64.wilsonHeight
24

>>> 81/64.tenneyHeight
12.34
```

Threads over lists:

```
>>> [81/1 80/1 81/80 6480/1].wilsonHeight
[12 13 25 25]

>>> [80/79 82/81].wilsonHeight
[92 55]
```

* * *

See also: benedettiHeight, tenneyHeight

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/Wilson_height)

Categories: Tuning
