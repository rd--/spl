# allButFirstAndLast

- _allButFirstAndLast(aSequence, anInteger)_
- _allButFirstAndLast(α)_ ⇒ _allButFirstAndLast(α, 1)_

Answer a copy of _aSequence_ containing all but the first and last _anInteger_ elements.
Signal an error if there are not enough elements.

```
>>> 1:9.allButFirstAndLast(3)
4:6
```

The unary form answers all but the first and last element.

```
>>> 1:9.allButFirstAndLast
2:8

>>> 'cat'.contents.allButFirstAndLast
['a']
```

* * *

See also: allButFirst, allButLast

Categories: Accessing
