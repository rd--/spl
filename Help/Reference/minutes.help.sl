# minutes

- _minutes(aDuration)_

Answer the number of complete minutes in the _aDuration_.

```
>>> 3.hours.minutes
180

>>> 2.days.minutes
(2 * 24 * 60)
```

* * *

See also: Duration, hours, milliseconds, seconds

References:
_Smalltalk_
5.8.2.12

Categories: Time, Type
