# highBitOfMagnitude

- _highBitOfMagnitude(anInteger)_

Answer the index of the high order bit of the magnitude of the _anInteger_, or zero if _anInteger_ is zero.

At `LargeInteger`:

```

>>> (127L << 8).highBitOfMagnitude
15

>>> 479363082683256994034920463268L
>>> .highBitOfMagnitude
99
```

* * *

See also: Binary, highBit

Categories: Bitwise
