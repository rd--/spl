# blackmanHarrisWindow

- _blackmanHarrisWindow(aNumber)_

Answer the _Blackman-Harris window_ function at _aNumber_.

```
>>> 0.1.blackmanHarrisWindow
0.793834
```

Plot:

~~~spl svg=A
(-1 -- 1).functionPlot(
	blackmanHarrisWindow:/1
)
~~~

![](sw/spl/Help/Image/blackmanHarrisWindow-A.svg)

Discrete Blackman-Harris window of length 15:

~~~spl svg=B
(-0.5 -- 0.5).discretize(
	15,
	blackmanHarrisWindow:/1
)
.discretePlot
~~~

![](sw/spl/Help/Image/blackmanHarrisWindow-B.svg)

* * *

See also: blackmanWindow

Guides: Window Functions

References:
_Mathematica_
[1](https://mathworld.wolfram.com/BlackmanHarrisFunction.html)
[2](https://reference.wolfram.com/language/ref/BlackmanHarrisWindow.html)

Categories: Windowing
