# --- (hyphenMinusHyphenMinusH...)

- _α --- β_ ⟹ _UndirectedEdge(α, β)_

Answer an `UndirectedEdge` between vertices α and β.

```
>>> 1 --- 2
UndirectedEdge(1, 2)
```

* * *

See also: -->, DirectedEdge, Graph, UndirectedEdge

Unicode: U+02E3A ⸺ Two-Em Dash
