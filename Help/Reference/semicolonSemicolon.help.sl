# ;; (semicolonSemicolon)

Double semicolon is a syntax token, it is not an operator.

_Note:_
This is a Lisp end of line comment form.
(In Lisps a single semicolon indicates the beginning of a comment.
The convention is to write comments that begin a line using two semicolons.)
The Haskell comment to end of line token is `--`.
Where supported the comment to end of line token is displayed as ⍝.

The name of this token is `semicolonSemicolon`.

* * *

See also: ;

Unicode: U+235D ⍝ APL Functional Symbol Up Shoe Jot, U+22EF ⋯ Midline Horizontal Ellipsis

Categories: Comment, Syntax
