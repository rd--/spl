# logarithmicIntegralRamanujan

- _logarithmicIntegralRamanujan(aNumber, anInteger)_

The logarithmic integral function,
calculated using a series expansion due to Ramanujan,
with limit _anInteger_.

The `logarithmicIntegral` calls this with the limit set to nine.

```
>>> 20.logarithmicIntegralRamanujan(9)
9.9032

>>> 20.logarithmicIntegralRamanujan(99)
9.9053
```

* * *

See also: log, logarithmicIntegral

References:
_Mathematica_
[1](https://mathworld.wolfram.com/LogarithmicIntegral.html)
[2](https://reference.wolfram.com/language/ref/LogIntegral.html)

Categories: Math
