# :/ (colonSolidus)

Arity selector syntax.

When a method is declared the name it is bound to indicates the arity of the method.

Likewise when a temporary variable is initialised to a literal block value,
the variable defined indicates the arity of the block.

The application syntax _f(x, y)_ rewrites _f_ to the arity-specific name, here _f:/2(x, y)_.
This is the ordinary notation for applying methods.
Therefore, in the ordinary case the arity dispatch is made statically and not dynamically,
and there is no possibility of arity errors.

The _:/_ syntax allows selecting the arity specific block,
which can be used to send it, rather than the arity-generic block, as an argument to another block.

Note that the application syntax cannot be used to apply a name that was not initialised using one of the ways noted above.

In particular blocks passed as arguments cannot be applied using this syntax,
and must be called using _apply_ or _value_.

```
>>> [sqrt(9), 9.sqrt, sqrt:/1.value(9), sqrt:/1 . (9)]
[3 3 3 3]
```

Where supported `:/` is displayed as ⧸.

The name of this token is `colonSolidus`.

* * *

See also: Block

Guides: Arity Notation

Unicode:
U+00B0 ⧸ Big Solidus,
U+02080 ₀ Subscript Zero,
U+02081 ₁ Subscript One,
U+02082 ₂ Subscript Two,
U+02083 ₃ Subscript Three

Categories: Syntax
