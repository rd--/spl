# weeks

- _weeks(aDuration | aNumber)_

Answer the number of complete weeks in _aDuration_,
or construct a `Duration` value holding _aNumber_ weeks.

```
>>> 5.weeks
50400.minutes

>>> 3.weeks.hours
(3 * 7 * 24)

>>> 7.weeks
49.days
```

* * *

See also: asSeconds, Duration, hours, seconds, minutes, weeks

Categories: Time, Type
