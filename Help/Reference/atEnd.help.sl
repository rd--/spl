# atEnd

- _atEnd(aStream)_

Answers `true` if there are no more elements in _aStream_, else `false`.

```
>>> let i = 1:9.asIterator;
>>> [i.next, i.next, i.atEnd]
[1, 2, false]
```

* * *

See also: asIterator, Stream

References:
_Smalltalk_
5.9.2.1

Categories: Testing
