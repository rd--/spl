# Hilbert

- _Hilbert(input)_

Applies the Hilbert transform to an input signal.

Returns two channels with the original signal,
and a copy of that signal that has been shifted in phase by 90 degrees (0.5 pi radians).
Due to the method used, distortion occurs in the upper octave of the frequency spectrum.

* * *

See also: FreqShift

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Hilbert.html)

Categories: Ugen
