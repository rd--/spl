# asByteArray

- _asByteArray(aCollection)_

Convert _aCollection_ to a `ByteArray`.

```
>>> [1 1 1 3 3 5]
>>> .asByteArray
>>> .base64Encoded
'AQEBAwMF'
```

* * *

See also: asList, ByteArray

References:
_Smalltalk_
5.7.1.5

Categories: Converting
