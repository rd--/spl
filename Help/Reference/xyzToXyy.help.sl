# xyyToXyz

- _xyyToXyz(xyy)_

Convert from Cie _Xyz_ tristimulus values to Cie _Xyy_ colourspace.
Answer an _(x,y,z)_ triple given an _(x,y,y)_ triple.

At specific value:

```
>>> [0.20654 0.12197 0.05137].xyzToXyy
[0.54370 0.32108 0.12197]

>>> [0.54370 0.32108 0.12197].xyyToXyz
[0.20654 0.12197 0.05137]
```

* * *

See also: Colour, xyyToXyz, xyzToRgb

Guides: Colour Functions

References:
_Python_
[1](https://colour.readthedocs.io/en/latest/generated/colour.XYZ_to_xyY.html)
