# SfAcquire

- _SfAcquire(soundFileUrl, numberOfChannels, channelSelector)_

Caching sound-file loader.
Answers the buffer, or list of buffers, holding the channels of the sound file.

~~~spl SfAcquire
SfAcquireMono('Floating')
~~~

* * *

See also: BufRd, BufWr

Categories: SoundFile
