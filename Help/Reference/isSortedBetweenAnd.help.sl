# isSortedBetweenAnd

- _isSortedBetweenAnd(aSequence, startIndex, endIndex)_

Answer `true` if _aSequence_ is sorted by `<=` between the indicated indices.

```
>>> [2 1 2 3 2].isSortedBetweenAnd(2, 4)
true

>>> [2 1 2 3 2].isSorted
false
```

* * *

See also: isSorted, isSortedBy, isSortedByBetweenAnd, sort

Guides: Sort Functions
