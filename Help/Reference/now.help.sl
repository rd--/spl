# now

- _now(aSystem)_

Answer a `TimeStamp` indicating the current time.

```
>>> system.now.asDate > '2024-10-02'.parseDate
true
```

* * *

See also: TimeStamp

References:
_Smalltalk_
5.8.4.6

Categories: Time
