# commaSeparated

- _commaSeparated(aList)_

Answer the `String` items of _aList_ joined togther with a comma and space intercalated.

```
>>> ['a' 'b' 'c'].commaSeparated
'a, b, c'
```

* * *

See also: List, String, stringIntercalate, unlines, unwords

Guides: String Functions
