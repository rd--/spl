# TypeBrowser

A `ColumnBrowser` where:

- column one lists the names of each `Type` the system knows of
- column two shows the qualified names of the methods implemented at the selected type

The list includes only the methods implemented directly at the `Type`, not methods imported from any `Trait` the type implements.

When a method is selected the text pane shows the definition of the method.

The _Status Text_ shows the traits the selected type implements.

* * *

See also: TraitBrowser, SystemBrowser

Categories: Kansas
