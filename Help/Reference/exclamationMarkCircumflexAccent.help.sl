# !^ (exclamationMarkCircumflexAccent)

- _alpha !^ beta_ ⟹ _(alpha ! beta).Splay_

Duplicate and distribute.
Names the idiom of generating an array and distributing it across the main ouput channels.

```
{ :tr |
	{
		let f = Choose(
			tr,
			[48 .. 72]
		).MidiCps;
		let ff = f * (
			SinOsc(
				TExpRand(4, 6, tr),
				0
			) * 0.008 + 1
		);
		LfSaw(
			ff * TRand(0.99, 1.01, tr),
			0
		) * 0.05
	} !^ 10
}.OverlapTexture(2, 3, 4).Mix
```

The name of this operator is `exclamationMarkCircumflexAccent`.

* * *

See also: !, !+, Splay
