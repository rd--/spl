# reciprocal

- _reciprocal(aNumber)_

Answer `one` divided by `aNumber`.

At `Integer`:

```
>>> 3.reciprocal
1/3
```

At `Fraction`:

```
>>> 3/4.reciprocal
4/3
```

At `zero` and `inf`:

```
>>> Infinity.reciprocal
0

>>> 0.reciprocal
Infinity
```

* * *

See also: /, one
