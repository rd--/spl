# cancelFlat

- _cancelFlat(aNumber)_

Answer _aNumber + 0.1_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 3.cancelFlat
3.1
```

Where supported `cancelFlat` is displayed as ♮.

* * *

See also: degreeToKey, flat, Scale, sharp

Unicode: U+266E ♮ Music Natural Sign
