# arcSecond

- _arcSecond(aNumber)_

Answer _aNumber_ times _arcSecond_, the constant representing one sixtieth of an `arcMinute`.

```
>>> 1.arcSecond
(1.arcMinute / 60)
```

* * *

See also: arcMinute, degree, pi

Guides: Mathematical Constants
