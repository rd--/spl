# traitNameList

- _traitNameList(aType)_

Answer a `List` of the `Trait` names implemented by _aType_.

```
>>> 23.typeDefinition.traitNameList
[
	'Object'
	'Json'
	'Magnitude'
	'Number'
	'Integer'
	'Binary'
]
```

* * *

See also: Trait, Type

Categories: Reflection
