# randomTree

- _randomTree(r, n)_

Answer a pseudorandom tree with _n_ nodes.
The values of the tree are the integers _(1, n)_.

Generate a random tree with ten nodes:

~~~spl svg=A
Sfc32(329372).randomTree(10).treePlot
~~~

![](sw/spl/Help/Image/randomTree-A.svg)

Generate a random tree with seven nodes:

~~~spl svg=B
Sfc32(319437).randomTree(7).treePlot
~~~

![](sw/spl/Help/Image/randomTree-B.svg)

Generate a random tree with five nodes:

~~~spl svg=C
Sfc32(112212).randomTree(5).treePlot
~~~

![](sw/spl/Help/Image/randomTree-C.svg)

* * *

See also: RandomNumberGenerator, Sfc32, Tree

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/RandomTree.html)
