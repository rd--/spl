# euclideanAlgorithm

- _euclideanAlgorithm(a, b)_

The Euclidean algorithm is an efficient method for computing the greatest common divisor of two integers.

```
>>> 42823.euclideanAlgorithm(6409)
17

>>> [3 7 40].collect { :each |
>>> 	12.euclideanAlgorithm(each)
>>> }
[3 1 4]
```

* * *

See also: extendedEuclideanAlgorithm, gcd
