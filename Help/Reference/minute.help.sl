# minute

- _minute(aDate)_

Answer the minute of _aDate_.

```
>>> '2024-03-04T21:41'.parseDate.minute
41
```

* * *

See also: asDate, Date, hour, month, parseDate, second, year

References:
_Smalltalk_
5.8.1.18

Categories: Accessing, Time
