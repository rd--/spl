# false

The constant value inidicating the _Boolean_ false value.

Where supported `false` is displayed as ⊥.

`false` is a reserved word, like `true` and `nil`.

* * *

See also: Boolean, not, true

References:
_Mathematica_
[1](https://mathworld.wolfram.com/False.html)
_SuperCollider_
[1](https://doc.sccode.org/Classes/False.html)

Unicode: U+22A5 ⊥ Up tack

Categories: Boolean, Constant
