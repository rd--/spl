# href

- _href(aUrl)_

Answer a `String` having the entire Url.

```
>>> 'https://www.w3.org/'.asUrl.href
'https://www.w3.org/'
```

_href_ is an abbreviation for _Hypertext Reference_,
also called a hyperlink.

* * *

See also: host, hostName, Location, origin, pathName, Url

Categories: Network
