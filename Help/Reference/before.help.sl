# before

- _before(aCollection, anObject)_

Answer the element before _anObject_.
Raise an error if _anObject_ is not in the _aCollection_, or if there are no elements before it.

```
>>> 1:9.before(5)
4
```

* * *

See also: beforeIfAbsent

References:
_Smalltalk_
5.7.8.6
