# \\\\\ (reverseSolidusReverseSolidus)

- _aNumber \\\\ anotherNumber_

The operator name for _remainder_.

```
>>> 9 \\ 4
1

>>> -9 \\ 4
-1

>>> 0.9 \\ 0.5
0.4

>>> 8.625 \\ 0.75
0.375
```

Threads over lists:

```
>>> 7.iota \\ 2
[1 0 1 0 1 0 1]
```

The name of this operator is `reverseSolidusReverseSolidus`.

* * *

See also: /, //, quotient, remainder

References:
_Smalltalk_
5.6.2.9

Categories: Arithmetic operators
