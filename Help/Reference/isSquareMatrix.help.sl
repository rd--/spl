# isSquareMatrix

- _isSquareMatrix(aMatrix)_

A _square matrix_ is a _matrix_ with an equal number of rows and column, ie. a _n×n_ matrix.

At `List`:

```
>>> [
>>> 	1 2 3;
>>> 	4 5 6;
>>> 	7 8 9
>>> ].isSquareMatrix
true
```

* * *

See also: isMatrix, List, Matrix, shape

Categories: Testing, Matrix
