# Workspace

A _text editor_ for working on small program fragments.

There is a _context menu_ that holds a set of useful operations,
with keybindings,
such as _Do It_ and _Print It_ and so on.

* * *

See also: Notebook, SmallKansas, SmallProgram

Categories: Kansas
