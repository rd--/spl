# LsAtFold

- _LsAtFold(list, indices)_

At each step answer _atFold_ of _indices_ into _list_.

```
>>> LsAtFold(
>>> 	[1 3 5 7 9],
>>> 	LsSeries(1, 1, 11)
>>> ).upToEnd
[1 3 5 7 9 7 5 3 1 3 5]
```

* * *

See also: LsAt, LsAtWrap

Categories: Stream
