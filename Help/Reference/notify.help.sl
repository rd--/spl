# notify

- _notify(anObject, aString)_

User notification.
Answer _anObject_, and also prints a notification.
The notification gives the type of _anObject_, its value, and the message _aString_.

```
>>> 1.pi.notify('pi')
1.pi
```

* * *

See also: error, identity, postLine, warning

Categories: Error
