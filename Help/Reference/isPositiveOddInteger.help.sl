# isPositiveOddInteger

- _isPositiveOddInteger(aNumber)_

Answers `true` if _aNumber_ is a positive integer and is odd.

```
>>> 23.isPositiveOddInteger
true

>>> 12.isPositiveOddInteger
false

>>> 2.pi.isPositiveOddInteger
false
```

* * *

See also: isInteger, isOdd, isPositive, isPositiveInteger, isNumber

Categories: Predicate, Testing
