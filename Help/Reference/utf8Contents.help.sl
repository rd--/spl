# utf8Contents

- _utf8Contents(aStream)_

Read the contents of _aStream_,
which should be of a Utf8 encoded `ByteArray`,
and answer a `String` value.

* * *

See also: contents, Utf8Stream, utf8String
