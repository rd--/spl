# typeReponsibility

- _typeReponsibility(anObject, message)_

Signal a _type responsibility_ error.

A `Trait` defines required methods using this method.

* * *

See also: error, Trait, Type, signal

Categories: Error
