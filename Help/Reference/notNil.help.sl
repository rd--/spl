# notNil

- _notNil(anObject)_

Answer `false` if _anObject_ is `nil`, else `true`.

```
>>> nil.notNil
false

>>> ().notNil
true
```

* * *

See also: isNil, Nil

References:
_Smalltalk_
5.3.1.14

Categories: Testing
