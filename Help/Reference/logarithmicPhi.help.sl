# logarithmicPhi

- logarithmicPhi(aNumber)_

Answer _aNumber_ times logarithmic phi.

Logarithmic phi, octave-reduced, is 741.6¢:

```
>>> 1.logarithmicPhi.ratioToCents - 1200
741.6

>>> 1.logarithmicPhi
(2 ^ 1.goldenRatio)
```

* * *

See also: acousticPhi, goldenRatio

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/Logarithmic_phi)

Further Reading: Aiylam 2016
