# isLowerCase

- _isLowerCase(aCharacter | aString)_

Answer `true` if _aString_ is made up of all lower case characters, else `false`.

```
>>> 'word'.isLowerCase
true
```

Lower case _w_ is lower case:

```
>>> 'w'.asCharacter.isLowerCase
true
```

* * *

See also: asLowerCase, isUpperCase

References:
_Smalltalk_
5.3.4.9

Categories: Testing, Text
