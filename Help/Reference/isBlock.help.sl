# isBlock

- _isBlock(anObject)_

Answer `true` if _anObject_ is a `Block`.

```
>>> { }.isBlock
true
```

* * *

See also: Block

Categories: Testing
