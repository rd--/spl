# ] (rightSquareBracket)

`]` is a syntax token, it is not an operator.

`]` is a part of `List Syntax`,
where it ends a `List` that was begun by `[`.
It is also a part of `Vector Syntax`, `Matrix Syntax` and `Volume Syntax`.

The name of this syntax token is `rightSquareBracket`.

* * *

See also: leftSquareBracket

Guides: List Syntax, Matrix Syntax, Vector Syntax, Volume Syntax

Categories: Syntax
