# textDrawing

- _textDrawing(anObject)_

Answer a `String` drawing of _anObject_.

At `Scale`:

```
>>> Scale(1, [2 2 3 2 3], 'Maj. Pentatonic')
>>> .textDrawing
'├─┼─┼──┼─┼──┤'
```

* * *

See also: asLineDrawing, Scale, String
