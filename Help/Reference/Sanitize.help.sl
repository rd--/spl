# Sanitize

- _Sanitize(in, replace=0)_

Replaces infinities, NaNs, and subnormal numbers (denormals) with a given signal.

- in: input signal to sanitize
- replace: the signal that replaces bad values

* * *

See also: CheckBadValues

Categories: Ugen
