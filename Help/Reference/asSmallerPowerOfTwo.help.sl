# asSmallerPowerOfTwo

- _asSmallerPowerOfTwo(anInteger)_

Answers a power of two that is not greater than _anInteger_.

```
>>> 300.asSmallerPowerOfTwo
256

>>> 512.asSmallerPowerOfTwo
512
```

* * *

See also: asPowerOfTwo, asLargerPowerOfTwo, bitShift, highBit, isPowerOfTwo

Categories: Truncation, Rounding
