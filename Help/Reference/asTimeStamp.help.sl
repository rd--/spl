# asTimeStamp

- _asTimeStamp(aNumber)_

Answer a `TimeStamp` holding the time given by taking `aNumber` as the number of milliseconds since the Unix epoch (1970-01-01).

* * *

See also: Date, Duration, now, TimeStamp

Categories: Time, Type
