# intervals

- _intervals(aScale)_

Answer the intervals of _aScale_.

`intervals` is `intervalsBy` of `one`.

```
>>> 'Major'.namedScale.intervals
[2 2 1 2 2 2 1]
```

* * *

See also: intervalClass, intervalClasses, intervalsBy, Scale
