# pushWithPriority

- _pushWithPriority(aPriorityQueue, anObject, aPriority)_

Add _anObject_ to _aPriorityQueue_ with _aPriority_.
Answers `nil`.

* * *

See also: add, addFirst, addLast, PriorityQueue

Categories: Adding
