# addFrameWithAnimator

- _addFrameWithAnimator(aSmallKansas, subject, event, delay, aBlock:/0)_

Add a `Frame`, with an animator, to `SmallKansas`.

* * *

See also: addFrame, SmallKansas

Categories: Kansas
