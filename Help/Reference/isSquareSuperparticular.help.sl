# isSquareSuperparticular

- _isSquareSuperparticular(aFraction)_

Answer `true` if _aFraction_ is a superparticular ratio and the `numerator` is squareful, else `false`.
An integer is squareful if it is not square free.

```
>>> [4/3 9/8 16/15 25/24 36/35]
>>> .allSatisfy(isSquareSuperparticular:/1)
true
```

* * *

See also: Fraction, isSquareFree, isSuperparticular

References:
_Xenharmonic_
[1](https://en.xen.wiki/w/Square_superparticular)

Categories: Testing, Math
