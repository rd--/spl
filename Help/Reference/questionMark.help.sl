# ? (questionMark)

- _alpha ? beta_ ⟹ _ifNil(alpha, beta)_

```
>>> 1 ? { 2 }
1

>>> nil ? { 2 }
2
```

The name of this operator is `questionMark`.

* * *

See also: ifNil

Categories: Conditional, Evaluation
