# Warp1

- _Warp1(numChannels=1, bufnum=0, pointer=0, freqScale=1, windowSize=0.2, envbufnum=-1, overlaps=8, windowRandRatio=0, interp=1)_

Warp a buffer with a time pointer.
Inspired _Warp1_ from SuperCollider2 by Chad Kirby,
which was inspired by _sndwarp_ from CSound by Richard Karpen.
A granular time stretcher and pitchshifter.

- numChannels: number of channels at bufNum
- bufNum: buffer number of soundfile
- pointer: position in buffer (0-1). 0 is the beginning, 1 is the end
- freqScale: amount of frequency shift. 1/2 is one octave down, 2 is one octave up, negative values play backwards
- windowSize: size of each grain window
- envBufNum: buffer number of grain envelope. -1 uses a built-in Hanning envelope
- overlaps: number of overlapping windows
- windowRandRatio: amount of randomness of the windowing function (0-1). 0 is no randomness
- interp: interpolation method. 1 = none, 2 = linear, 4 = cubic

Here the pointer moves from the beginning to the end of the soundfile over fifteen seconds, control pitch with _MouseX_:

```spl SfAcquire
let numChannels = 1;
let soundFile = SfAcquireMono('Floating');
let pointer = LfSaw(1 / 15, 0).LinLin(-1, 1, 0, 1);
let freqScale = MouseX(0.5, 2, 0, 0.2);
let windowSize = 0.1;
Warp1(
	numChannels,
	soundFile,
	pointer,
	freqScale,
	windowSize,
	-1,
	8,
	0.1,
	2
) * 0.25
```

Pointer is `Phasor`, playback slows from unit to a quarter over twenty seconds:

```spl SfAcquire
let sf = SfAcquireMono('Floating');
let pointer = Phasor(
	0,
	SampleDur() / SfDur(sf) * XLine(1, 0.25, 20),
	0,
	1,
	0
);
let sound = Warp1(
	1,
	sf,
	pointer,
	1,
	0.3,
	-1,
	16,
	Line(0, 1, 40),
	4
);
EqPan(sound, pointer * 2 - 1) / 4
```

* * *

See also: GrainFm, TGrains

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/Warp1.html)

Categories: Ugen
