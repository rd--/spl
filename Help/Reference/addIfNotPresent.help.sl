# addIfNotPresent

- _addIfNotPresent(aCollection, anObject)_

Add _anObject_ to _aCollection_ if the collection does not already _include_ the object.
Answers _anObject_.

* * *

See also: addIfNotPresentBy, ifAbsentAdd

Categories: Adding
