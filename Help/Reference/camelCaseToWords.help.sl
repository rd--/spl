# camelCaseToWords

- _camelCaseToWords(aString)_

Camel case names capitalize each word, excluding the first word.

```
>>> 'camelCaseToWords'.camelCaseToWords
'camel Case To Words'
```

* * *

See also: pascalCaseToWords, RegExp
