# CssStyleSheet

- _CssStyleSheet()_
- _CssStyleSheet(options)_

Answer a newly contructed Css style sheet.

* * *

Reference:
_Mdn_
[1](https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet)

Categories: Type
