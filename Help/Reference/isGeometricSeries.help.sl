# isGeometricSeries

- _isGeometricSeries(aSequence)_

Answer `true` if _aSequence_ is a geometic series.

```
>>> [1 3 9 27 81 243 729]
>>> .isGeometricSeries
true
```

The `ratios` of a geometic series are all equal:

```
>>> [1 3 9 27 81 243 729].ratios
[3 3 3 3 3 3]
```

* * *

See also: isArithmeticSeries, geometricSeries, ratios

Categories: Testing
