# atIfPresent

- _atIfPresent(aCollection, aKey, aBlock:/1)_

Lookup the value associated with _aKey_ in _aCollection_.
If the key is present,
answer the value of evaluating the given block with the value associated with the key.
Otherwise, answer nil.

* * *

See also: at, atIfAbsent, atIfPresentIfAbsent

Categories: Accessing
