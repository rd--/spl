# asLowerCase

- _asLowerCase(aCharacter | aString)_

Answer a `String` made up from _aString_ whose characters are all lower case.

```
>>> 'Word'.asLowerCase
'word'

>>> 'X'.asCharacter.asLowerCase
'x'.asCharacter
```

* * *

See also: asUpperCase, isLowerCase

References:
_Smalltalk_
5.3.4.2

Categories: Converting
