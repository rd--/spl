# fourth

- _fourth(aSequence)_

Answer the fourth element of _aSequence_.

```
>>> 1:6.fourth
4

>>> (6 .. 1).fourth
3
```

* * *

See also: first, last, middle, second, third

Categories: Accessing
