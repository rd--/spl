# ScalaTuning

- _ScalaTuning(aRecord)_

A `Type` holding a `Tuning` stored in the form of the scales held in the Scala tuning archive.

The `scalaTuningArchive` stores items as `ScalaTuning` objects.

The `asCentsTuning` and `asRatioTuning` methods convert between tuning types.

The `octave` method answers the octave as a ratio,
though not not necessarily a fraction.

* * *

See also: asCents, asCentsTuning, asRatios, asRatioTuning, CentsTuning, RatioTuning, scalaTuningArchive, Tuning
