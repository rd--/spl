# isFormFeed

- _isFormFeed(aCharacter)_

Answer `true` if _aCharacter_ is a form feed.

```
>>> '\f'.isFormFeed
true

>>> 12.asCharacter.isFormFeed
true
```

* * *

See also: Character, isCarriageReturn, isLineFeed, isSeparator, isSpace, isTab

Categories: Testing
