# flat

- _flat(aNumber)_

Answer _aNumber - 0.1_.
In some contexts, such as `degreeToKey`, a fractional scale degree indicates an _alteration_.

```
>>> 7.flat
6.9

7.flat.flat
6.8
```

Where supported `flat` is displayed as ♭.

* * *

See also: degreeToKey, quarterToneFlat, Scale, sharp

Unicode: U+266D ♭ Music Flat Sign
