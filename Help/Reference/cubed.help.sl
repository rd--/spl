# cubed

- _cubed(aNumber)_

Answer _aNumber_ raised to the power of three.

```
>>> (3 * 3 * 3, 3 ^ 3, 3.cubed)
(27, 27, 27)
```

The inverse is `cubeRoot`:

```
>>> 3.cubed.cubeRoot
3
```

Threads over lists:

```
>>> 1:9.cubed.sum
2025
```

Where supported `cubed` is displayed as ³.

* * *

See also: ^, *, cubeRoot, squared

Unicode: U+00B3 ³ Superscript Three
