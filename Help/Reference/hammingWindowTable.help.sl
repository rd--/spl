# hammingWindowTable

- _hammingWindowTable(anInteger)_

Answer a `List` describing a _Hamming window_ of the indicated size.

~~~spl svg=A
128.hammingWindowTable.linePlot
~~~

![](sw/spl/Help/Image/hammingWindowTable-A.svg)

* * *

See also: hammingWindow, hannWindowTable, kaiserWindowTable, welchWindowTable

Guides: Window Functions

Categories: Windowing
