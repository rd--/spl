# Sqrt

- _Sqrt(aNumber)_

Square root.
The definition of square root is extended for signals so that _sqrt(x)_ when _x < 0_ answers _-sqrt(-x)_.

```
>>> -9.Sqrt
-3
```

Compare with `sqrt`:

```
>>> -9.sqrt
0J3
```

* * *

See also: ^, cubeRoot, nthRoot, sqrt

Categories: Ugen
