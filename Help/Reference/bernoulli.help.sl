# bernoulli

- _bernoulli(anInteger)_

Answer the _anInteger_-th Bernoulli number.

First 10 Bernoulli numbers:

```
>>> 10.bernoulliSequence
[1/1 1/2 1/6 0 -1/30 0 1/42 0 -1/30 0 5/66]
```

The 20th Bernoulli number:

```
>>> let n = 20.bernoulli;
>>> (n, n.asFloat)
(-174611/330, -529.12424)
```

The 60th Bernoulli number:

```
>>> 60.bernoulli
-1215233140483755572040304994079820246041491L
/
56786730L
```

* * *

See also: bernoulliSequence

References:
_Mathematica_
[1](https://mathworld.wolfram.com/BernoulliNumber.html)
[2](https://reference.wolfram.com/language/ref/BernoulliB.html),
_OEIS_
[1](https://oeis.org/A000367)
[2](https://oeis.org/A002445)
