# substringsInCommon

- _substringsInCommon(aSequence, anotherSequence, anInteger)_

Answer the _anInteger_ length substrings of _aSequence_ that are also substrings of _anotherSequence_.

```
>>> let p = [1 2 3 2 3 4 5];
>>> let q = [3 2 1 3 2 3 1];
>>> p.substringsInCommon(q, 2)
[2 3; 3 2; 2 3]
```

* * *

See also: partition, substrings
