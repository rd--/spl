# DigitalClock

A simple digital clock to demonstrate animated frames and frame local font selection.

~~~spl smallKansas
DigitalClock()
.openIn(system.smallKansas, nil)
~~~

* * *

See also: AnalogueClock, SmallKansas

Categories: Kansas
