# \/\* (solidusAsterisk)

`/*` is the open comment syntax token for PL/I, used also for the C family of languages.
It is closed by `*/`.

Sum two numbers and comment upon this:

```
>>> 3 + 4 /* Three plus four is seven. */
7
```

The answers to programs may be commented:

```
>>> 7
3 + 4 /* Seven is three plus four. */
```

Comments within strings are not comments:

```
>>> '/* x */'
'/* x */'
```

In some, but not all, systems comments may be nested.

Where supported `/*` is displayed as «,
and `*/` is displayed as ».

The name of this token is `solidusAsterisk`.

* * *

See also: *, /

Guides: Comment Syntax

Unicode: U+000AB « Left-Pointing Double Angle Quotation Mark

Categories: Comments, Syntax
