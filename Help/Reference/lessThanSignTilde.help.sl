# <~ (lessThanSignTilde)

- _p <~ q_ ⟹ _p < q | { p ~ q }_

Answer `true` if _p_ is less than or similar to _q_, else `false`.

```
>>> 3.1416 <~ 1.pi
true

>>> 3.1416 > 1.pi
true
```

The name of this operator is `lessThanSignTilde`.

Where supported `<~` is displayed as ⪅.

* * *

See also: =, ~, <, >, >~

Unicode: U+2A85 ⪅ Less-Than or Approximate, U+2A9D ⪝ Similar or Less-Than

Categories: Comparing
