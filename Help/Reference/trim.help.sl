# trim

- _trim(aString)_

Remove whitespace from both ends of _aString_.

```
>>> '   aaa bbb ccc   '.trim
'aaa bbb ccc'
```

* * *

See also: String, withBlanksTrimmed, withoutLeadingBlanks, withoutTrailingBlanks

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/StringTrim.html),
_Tc39_
[1](https://tc39.es/ecma262/multipage/text-processing.html#sec-string.prototype.trim)

Categories: Converting
