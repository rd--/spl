# Sin

- _Sin(aNumber)_

Sine function.

```
>>> 1/3.pi.Sin
3.sqrt / 2
```

* * *

See also: Cos, sin, Tan

Categories: Ugen
