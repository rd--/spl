# boxMatrix

- _boxMatrix(radius)_

Answer a matrix whose elements are 1 in a box-shaped region that extends _radius_ index positions to each side.

A _7×7_ box matrix:

```
>>> 3.boxMatrix
[
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1;
	1 1 1 1 1 1 1
]
```

* * *

See also: crossMatrix, diamondMatrix, diskMatrix

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/BoxMatrix.html)

Categories: Math, Matrix
