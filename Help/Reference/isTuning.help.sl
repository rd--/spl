# isTuning

- _isTuning(anObject)_

Answer `true` if _anObject_ implements the `Tuning` trait.

```
>>> 12.equalTemperamentTuning.isTuning
true

>>> [1 6/5 4/3 3/2 8/5]
>>> .asRatioTuning
>>> .isTuning
true
```

* * *

See also: CentsTuning, RatioTuning, ScalaTuning, Tuning

Categories: Testing
