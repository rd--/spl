# query

- _query(aUrl)_

Answer a `String` having the query, also called the _search_, of the `Url`.

```
>>> 'http://google.com/search?q=cern'
>>> .asUrl
>>> .query
'?q=cern'
```

* * *

See also: fragment, host, hostName, href, pathName, protocol, Location, Url

Categories: Network
