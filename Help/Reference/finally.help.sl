# finally

- _finally(aPromise, aBlock:/0)_

Schedule _aBlock_ be called when _aPromise_ is settled, either as fulfilled or rejected.
Answer an equivalent `Promise` object to _aPromise_.

* * *

See also: onRejection, Promise, then, thenElse

References
_Tc39_
[1](https://tc39.es/ecma262/multipage/control-abstraction-objects.html#sec-promise.prototype.finally)
