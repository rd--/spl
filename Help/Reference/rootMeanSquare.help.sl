# rootMeanSquare

- _rootMeanSquare(aCollection)_

For a set of numbers the root-mean-square (sometimes called the quadratic mean) is the square root of mean of the values.

```
>>> [1 5 5 4].rootMeanSquare
(67.sqrt / 2)

>>> [2 10 2 8].rootMeanSquare
43.sqrt

>>> 1:4.rootMeanSquare
(15 / 2).sqrt
```

RootMeanSquare of columns of a matrix:

```
>>> [1 2; 5 10; 5 2; 4 8].rootMeanSquare
[67.sqrt / 2, 43.sqrt]
```

* * *

See also: mean, standardDeviation, variance

References:
_Mathematica_
[1](https://mathworld.wolfram.com/Root-Mean-Square.html)
[2](https://reference.wolfram.com/language/ref/RootMeanSquare.html)
