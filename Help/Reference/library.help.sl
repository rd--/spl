# library

- _library(aSystem)_

Answer a `Record` holding the `LibraryItem` library of _aSystem_.

Count the entries in the library:

```
>>> system.library.size
26
```

Check if the library includes a particular entry:

```
>>> system
>>> .library
>>> .includesKey('ScalaScaleArchive')
true
```

* * *

See also: LibraryItem, Record, system
