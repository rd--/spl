# millisecond

- _millisecond(aDate)_

Answer the millisecond of _aDate_:

```
>>> '2024-03-04T21:41:07.03'.parseDate.millisecond
30
```

* * *

See also: Date, dayOfMonth, first, hour, minute, month, parseDate, second, year

Categories: Accessing, Time
