# addAllIfNotPresent

- _addIfNotPresent(aCollection, anotherCollection)_

Add each item in _anotherCollection_ to _aCollection_ if the collection does not already _include_ the item.
Answers _aCollection_.

* * *

See also: add, addIfNotPresent, ifAbsentAdd

Categories: Adding
