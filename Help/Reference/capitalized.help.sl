# capitalized

- _capitalized(aString)_

Answer a copy of _aString_ with the first character capitalized if it is a letter.

```
>>> 'ascii'.capitalized
'Ascii'

>>> '3.141'.capitalized
'3.141'
```

* * *

See also: asLowerCase, asUpperCase, String

Categories: Converting
