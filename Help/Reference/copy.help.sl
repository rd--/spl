# copy

- _copy(anObject)_

Answer another object just like _anObject_.

```
>>> [1 .. 5].copy
[1 .. 5]

>>> let a = [1 .. 5];
>>> a ~~ a.copy
true

>>> let a = [1 .. 5];
>>> let b = a.copy;
>>> a[1] := 5; b[1] ~= 5
true
```

Copy is defined in terms of `shallowCopy` and `postCopy`.

* * *

See also: copyFromTo, copyReplaceFromToWith, copyWith, copyWithout, deepCopy, postCopy, shallowCopy

References:
_Smalltalk_
5.3.1.6

Categories: Copying
