# takeFirst

- _takeFirst(aSequence, anInteger, anObject)_

Answer a copy of _aSequence_ with only the first _anInteger_ places.
Fill any empty slots with _anObject_.

```
>>> [5 4 3 2 1].takeLast(8, 0)
[0 0 0 5 4 3 2 1]
```

* * *

See also: take, takeLast
