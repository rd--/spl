# '{-'

`{-` is the open comment token in Haskell, it is closed by `-}`.

* * *

See also: -}

Guides: Comment Syntax
