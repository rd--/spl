# LsAtWrap

- _LsAtWrap(list, indices)_

At each step answer _atWrap_ of _indices_ into _list_.

```
>>> LsAtWrap(
>>> 	[1 3 5 7 9],
>>> 	LsSeries(1, 1, 11)
>>> ).upToEnd
[1 3 5 7 9 1 3 5 7 9 1]
```

* * *

See also: LsAt, LsAtWrap

Categories: Stream
