# PeakFollower

- _PeakFollower(in, decay)_

Track peak signal amplitude.
Outputs the peak amplitude of the signal received at the input.
If level is below maximum, the level decreases by the factor given in decay.

- in: input signal.
- decay: decay factor.

Internally, the absolute value of the signal is used, to prevent underreporting the peak value if there is a negative Dc offset.
To obtain the minimum and maximum values of the signal as is, use the `RunningMin` and `RunningMax` Ugens.

No decay:

```
let peak = PeakFollower(
	Dust(20) * Line(0, 1, 4),
	1
);
SinOsc(peak * 1500 + 200, 0) * 0.1
```

A little decay:

```
let peak = PeakFollower(
	Dust(20) * Line(0, 1, 4),
	0.999
);
SinOsc(peak * 1500 + 200, 0) * 0.1
```

Mouse controls decay:

```
let x = MouseX(0.99, 1.00001, 0, 0.1);
let peak = PeakFollower(
	Dust(20) * Line(0, 1, 4),
	x.Min(1)
);
SinOsc(peak * 1500 + 200, 0) * 0.1
```

Follow a sine lfo, decay controlled by mouse:

```
let decay = MouseX(0, 1.1, 0, 0.1).Min(1);
let peak = PeakFollower(
	SinOsc(0.2, 0),
	decay
);
SinOsc(peak * 200 + 500, 0) * 0.1
```

* * *

See also: Amplitude, Peak

References:
_SuperCollider_
[1](https://doc.sccode.org/Classes/PeakFollower.html)

Categories: Ugen
