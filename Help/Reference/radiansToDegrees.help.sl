# radiansToDegrees

- _radiansToDegrees(aNumber)_

Convert from radians to degrees.

```
>>> 2.pi.radiansToDegrees
360
```

* * *

See also: Angle, degrees, degreesToRadians, radians

References:
_Smalltalk_
5.6.7.12

Categories: Converting, Math, Geometry
