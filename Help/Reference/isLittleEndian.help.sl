# isLittleEndian

- _isLittleEndian(aSystem)_

Answer `true` if _aSystem_ is Little-endian.

```
>>> system.isLittleEndian
true
```

* * *

See also: byteOrdering, isBigEndian

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/ByteOrdering.html),
_W_
[1](https://en.wikipedia.org/wiki/Endianness)
