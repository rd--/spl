# LsCat

- _LsCat(list)_

Apply _LsOnce_ at each element in _list_ and sequence the answers.

```
>>> LsCat(
>>> 	[
>>> 		LsGeom(1, 3, 3),
>>> 		-1,
>>> 		LsSeries(1, 3, 3)
>>> 	]
>>> ).upToEnd
[1 3 9 -1 1 4 7]
```

* * *

See also: LsSeq

Guides: Patterns and Streams

Categories: Stream
