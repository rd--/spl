# sampleStandardDeviation

- _sampleStandardDeviation(aCollection)_

Form of standard deviation where _aCollection_ represents the _entire_ population, not a subset.

```
>>> [2 4 4 4 5 5 7 9].sampleStandardDeviation
(32 / 8).sqrt

>>> [2 4 4 4 5 5 7 9].standardDeviation
(32 / 7).sqrt
```

* * *

See also: mean, meanDeviation, moment, standardDeviation, standardizedMoment, variance
