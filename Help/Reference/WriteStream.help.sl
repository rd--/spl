# WriteStream

`WriteStream` is a `Trait` that requires the method `nextPut`, which puts an item onto the stream.

* * *

See also: ReadStream, Stream

Categories: Collection, Trait
