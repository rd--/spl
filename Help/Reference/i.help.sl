# i

- _i(aNumber)_

Answer a _Complex_ number with a real part of `zero` and an imaginary part of _aNumber_.

```
>>> 1.i
0J1
```

At `List`:

```
>>> [3 -4 6].i
[0J3 0J-4 0J6]
```

* * *

See also: Complex, imaginary, j, real

Guides: Mathematical Constants

References:
_Apl_
[1](https://aplwiki.com/wiki/Imaginary)

Categories: Converting
