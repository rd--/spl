# alpha

- _alpha(aColour)_

Answer the value of the _alpha_ (transparency) channel of _aColour_.
An opaque colour has _alpha_ equal to `one`,
a completely transparent colour has _alpha_ equal to `zero`.

Where supported `alpha` is displayed as α.

* * *

See also: blue, Colour, green, red

Guides: Colour Functions

Unicode: U+03B1 α Greek Small Letter Alpha
