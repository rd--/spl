# rotatedRight

- _rotatedRight(aSequence, anInteger)_

Rotate _aSequence_ by _anInteger_ places to the right,
i.e. _anInteger_ elements from the end are moved to the start.
The rotation is not in place, a new sequence is answered.

Rotate right by three places:

```
>>> 1:7.rotatedRight(3)
[5 6 7 1 2 3 4]
```

Rotate left by four places:

```
>>> 1:7.rotatedRight(-4)
[5 6 7 1 2 3 4]
```

* * *

See also: atWrap, rotatedLeft

References:
_Mathematica_
[1](https://reference.wolfram.com/language/ref/RotateRight.html)

Categories: Ordering
