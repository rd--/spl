# indexOfSubstringStartingAtIfAbsent

- _indexOf(aSequence, aSubstring, anIndex, exceptionBlock:/0)_

Answer the index of the first element of _aSequence_,
such that this element equals the first element of _aSubstring_,
and the next elements equal the rest of the elements of _aSubstring_.
Begin the search at element _anIndex_ of _aSequence_.
If no such match is found, answer the result of evaluating _exceptionBlock_.

* * *

See also: indexOfIfAbsent

References:
_Smalltalk_
5.7.8.23

Categories: Accessing
