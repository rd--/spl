# transpose

- _transpose(aMatrix)_

Transpose the rows and columns of _aMatrix_ in place.

```
>>> let m = Matrix22(1, 2, 3, 4);
>>> m.transpose;
>>> m.asList
[1 3 2 4]
```

* * *

See also: transposed

Unicode: U+1D40 ᵀ Modifier Letter Capital T

Categories: Ordering
