# Mix

- _Mix(inList)_

Mix an array of arrays of signals to an array of signals, as configured in `Preferences`.

```
SinOsc([333 555; 444 777], 0).Mix * 0.1
```

* * *

See also: EqPan, Splay, Sum, sum

Categories: Ugen
