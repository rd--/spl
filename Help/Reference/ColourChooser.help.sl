# ColourChooser

A simple colour selector.

When opened from the _WorldMenu_ it changes the background colour of _SmallKansas_.

* * *

See also: SmallKansas, WorldMenu

Categories: Kansas
