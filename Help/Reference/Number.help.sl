# Number

`Number` is the `Trait` for numerical values.

Methods for arithmetic: `+`, `-`, `*`, `/`, `%`, `quotient`, `remainder`, `abs`, `negated`, `reciprocal`

Methods implementing mathematical functions: `exp`, `log`, `floorLog`, `^`, `raisedToInteger`, `sqrt`, `squared`

Methods for testing: `isEven`, `isOdd`, `isNegative`, `isNonNegative`, `isPositive`, `isZero`, `sign`

Methods for truncating and rounding: `ceiling`, `floor`, `truncated`, `truncateTo`, `rounded`, `roundTo`, `roundUpTo`

Methods for trigonometry: `sin`, `cos`, `tan`, `degreeSin`, `degreeCos`, `arcSin`, `arcCos`, `arcTan`, `degreesToRadians`, `radiansToDegrees`

* * *

References:
_Smalltalk_
5.6.2

Categories: Numeric, Trait
