# Power

- _Power(aNumber, anotherNumber)_

Answer _aNumber_ raised to the power _anotherNumber_,
with the same meaning as `symmetricPower`,
which is the meaning of the `Power` operator unit generator of the SuperCollider synthesiser.

```
>>> Power(2, 3)
8

>>> -0.25.Power(0.75)
-0.353553
```

* * *

See also: ^, Exp, factorialPower, Log, powerMod, powerRange, Sqrt, symmetricPower

Categories: Math operator
