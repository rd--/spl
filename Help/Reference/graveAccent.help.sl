# ` (graveAccent)

` is a syntax token, it is not an operator.

It is part of `String Syntax`.

```
>>> `x`.isSymbol
true

>>> `x` == Symbol('x')
true
```

The name of this syntax token is `graveAccent`.

* * *

See also: apostrophe, quotationMark, Symbol

Guides: String Syntax

Unicode: U+0060 Grave Accent

Categories: Syntax
