# Exp

- _Exp(aNumber)_

Exponential.
Answer the value of `e` (the base of natural logarithms) raised to the power of _aNumber_.

At `SmallFloat`:

```
>>> Exp(2)
1.e ^ 2
```

* * *

See also: ^, e, Log, Power

References:
_Csound_
[1](https://csound.com/docs/manual/exp.html)

Categories: Math, Ugen
