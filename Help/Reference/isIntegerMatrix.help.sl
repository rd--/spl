# isIntegerMatrix

- _isIntegerMatrix(aMatrix)_

Answer `true` if _aMatrix_ is an integer matrix.

```
>>> [1 2 3; 4 5 6; 7 8 9].isIntegerMatrix
true

>>> [3.sqrt 0; 1 5.sqrt].isIntegerMatrix
false
```

* * *

See also: isMatrix, isVector

Categories: Testing, Matrix
