# Open Sound Control

Open Sound Control is an open, transport-independent, message-based protocol.
It is designed for communication among computers, sound synthesizers, and other media devices.

* * *

References:
_Stanford_
[1](https://opensoundcontrol.stanford.edu/)

Further Reading: Wright 1997
