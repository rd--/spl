# List Syntax

Lists are written between square brackets.
List items are separated by commas.

A list of integers:

```
>>> [1, 2, 3]
[1, 2, 3]
```

A list of expressions:

```
>>> [1 + 2, 3 * 4, 25.sqrt]
[3, 12, 5]
```

The empty list:

```
>>> []
[]
```

A list of the empty list:

```
>>> [[]]
[[]]
```

* * *

See also: List

Guides: Array Syntax, List Assignment Syntax, Tuple Syntax, Vector Syntax

Categories: Syntax
