# Patterns and Streams

SuperCollider names:

- `LsBeta`: Pbeta
- `LsBind`: Pbind
- `LsBrown`: Pbrown
- `LsCauchy`: Pcauchy
- `LsClump`: Pclump
- `LsClutch`: Pclutch
- `LsDiff`: Pdiff
- `LsDrop`: Pdrop
- `LsDupEach`: Pdup
- `LsFin`: Pfin
- `LsGeom`: Pgeom
- `LsLace`: Place
- `LsN`: Pn
- `LsRand`: Prand
- `LsSet`: Pset
- `LsSeq`: Pseq
- `LsSer`: Pser
- `LsSeries`: Pseries
- `LsSwitch`: Pswitch
- `LsSwitch1`: Pswitch1
- `LsTrace`: Ptrace
- `LsTuple`: Ptuple
- `LsWalk`: Pwalk
- `LsWhite`: Pwhite
- `LsXRand`: Pxrand

Further:

- `LsAccum`
- `LsAdjacent`
- `LsAtFold`
- `LsAtWrap`
- `LsAt`
- `LsCat`
- `LsCollect`
- `LsCons`
- `LsConstant`
- `LsCyc`
- `LsFold`
- `LsForever`
- `LsIBrown`
- `LsLast`
- `LsOnce`
- `LsPermutations`
- `LsRemDup`
- `LsScan`
- `LsSelect`
- `LsSlidingWindows`
- `LsTake`
- `LsUnfold`

* * *

Further Reading: Taube 1994
