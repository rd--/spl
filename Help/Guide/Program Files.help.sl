# Program Files

Program files consist of a set of one or more program fragments,
separated by empty lines,
and optionally followed by explanatory notes written in _Markdown_.

Program files have the file extension _.sp_.

Program files distributed with Spl are stored at _Program_.

* * *

Guides: File Extensions, Help Files
