# Elementwise Functions

There are two main kinds of elementwise,
or pointwise or componentwise,
functions.

Unary functions map an operator over a collection,
and are applicable to any `Collection`.

Binary functions map an operator over two collection that are in some way commesurate,
and are applicable to any `Sequencable` collection.

* * *

See also: adaptToCollectionAndApply, collect, Collection, Sequence, withCollect
