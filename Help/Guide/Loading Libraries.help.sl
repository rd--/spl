# Loading Libraries

Initially the only method in the system is `primitiveLoadPackageSequence`.
The interpreter calls this with at least _Kernel_ and ordinarily _StandardLibrary_ as parameters.
These together define the standard environment.
