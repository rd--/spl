# SuperCollider Synthesiser

SuperCollider is a family of languages for real-time audio synthesis.
The language and synthesiser are implemented as separate processes,
communicating using the `Open Sound Control` protocol.

* * *

See also: Open Sound Control

References:
_AudioSynth_
[1](https://audiosynth.com/)

Further Reading: McCartney 2002
