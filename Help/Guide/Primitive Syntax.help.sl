# Primitive Syntax

Primitives are written as the first statement of a `Method`.

A primitive is written between the syntax tokens _<primitive:_ and _>_.

The text between these tokens is written in the _host language_.

* * *

See also: Method

Guides: Primitive Semantics
