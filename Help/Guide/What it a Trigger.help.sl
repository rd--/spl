# What is a Trigger?

A trigger is registered when a signal changes from non-positive (<= 0) to positive (> 0).

This occurs at positive zero crossings in bi-polar signals, as well as on each impulse in a clock signal.

Monitoring an input signal for a trigger requires one memory location to store the previous input value,
so that it can be compared with the current input.

* * *

See also: What is a Clock, What is a Gate
