# Mathematical Functions

## Arithmetic Functions

- `accumulate`: cumulative sums in a list
- `differences`: successive differences in a list
- `mean`: mean of a list
- `product`: product of elements in a list
- `ratios`: successive ratios in a list
- `sqrt`: square root
- `sum`: sum of elements in a list

## Arithmetic Functions - Binary Operators

- `*`: multiplication
- `+`: addition
- `-`: subtraction
- `/`: division
- `^`: raised to

## Array Bounds

- `coordinateBoundingBox`: min and max coordinates
- `coordinateBounds`: min and max coordinates
- `minMax`: `min` and `max` values

## Base-Like Representations

- `integerDigits`: gives a list of the decimal digits in the integer n.
- `mixedRadixDecode`: mixed radix decoding
- `mixedRadixEncode`: mixed radix encoding

## Bitwise Functions - Logic

- `bitAnd`: bitwise and
- `bitNot`: bitwise not
- `bitOr`: bitwise or
- `bitXor`: bitwise exclusive or

## Bitwise Functions - Single-Bit

- `bitAt`: read bit
- `bitAtPut`: write bit

## Bitwise Functions - Structural

- `bitShiftLeft`: bit shift right
- `bitShiftRight`: bit shift left
- `highBit`: bit length

## Combinatorial Functions

- `binomial`: binomial coefficients
- `catalanNumber`: Catalan numbers
- `deBruijnSequence`: de Bruijn sequence
- `doubleFactorial`: double factorial
- `factorialPower`: factorial power
- `factorial`: factorial function (total arrangements of n objects)
- `hyperfactorial`: hyperfactorial function
- `multinomial`: multinomial coefficients
- `partitionFunctionP`: unrestricted partitions
- `partitionFunctionQ`: restricted partitions
- `partitionsP`: unrestricted partition count
- `partitionsQ`: restricted partition count
- `polygonalNumber`: triangular and other polygonal numbers
- `subfactorial`: number of derangements of  objects, leaving none unchanged

## Comparison Functions

- `compare`: lexicographical ordering
- `precedes`: lexicographical comparison

## Comparison Functions - Binary Operators

- `<=>`: -1, 0, +1 for less than, equal to, greater than
- `<=`: less than or equal to
- `<`: less than
- `>=`: greater than or equal to
- `>`: greater than

## Complex Number Functions

- `absArg`: absolute value and argument
- `abs`: absolute value
- `arg`: argument
- `conjugated`: complex conjugate
- `imaginary`: imaginary component of a complex number
- `real`: real component of a complex number
- `sign`: sign of

## Congruence Functions

- `%`: modulo
- `chineseRemainder`: chinese remainder theorem
- `multiplicativeOrder`: multiplicative order
- `powerMod`: power modulo

## Continued Fraction and Rational Approximation Functions

- `asFraction`: find rational approximations
- `continuedFraction`: continued fraction expansion
- `convergents`: a list of successive convergents of a continued fraction
- `fromContinuedFraction`: construct exact or inexact numbers from continued fractions

## Data Transform and Smoothing Functions

- `clip`: clip between
- `exponentialMovingAverage`: find the exponential moving average with damping
- `movingAverage`: find the simple moving average with any span
- `movingMap`: map a function over a moving window with any span
- `movingMedian`: find the moving median with any span
- `normalize`: normalize vector
- `rescale`: rescale between
- `standardize`: zero mean and unit sample variance

## Distance and Similarity Measures

- `binaryDistance`:  binary distance
- `brayCurtisDistance`: Bray–Curtis distance
- `canberraDistance`: Canberra or Lance-Williams distance
- `chessboardDistance`: chessboard or Chebyshev or sup norm distance
- `correlationDistance`: correlation distance
- `cosineDistance`: angular cosine distance
- `distanceMatrix`: distance matrix
- `editDistance`: edit or Levenshtein distance
- `euclideanDistance`: Euclidean distance
- `hammingDistance`: Hamming distance
- `manhattanDistance`: Manhattan or city block distance
- `minkowskiDistance`: Minkowski distance
- `normalizedSquaredEuclideanDistance`: nomralized squared Euclidean distance
- `squaredEuclideanDistance`: square of the Euclidean distance

## Divisibility Functions

- `divisible`: is divisible predicate
- `euclideanAlgorithm`: greatest common divisor
- `extendedEuclideanAlgorithm`: greatest common divisor and Bézout coefficients
- `gcd`: greatest common divisor
- `isCoprime`: is coprime predicate
- `isEven`: is even predicate
- `isOdd`: is odd predicate
- `isPrime`: is prime predicate
- `lcm`: least common multiple
- `modularInverse`: modular inverse

## Division Functions

- `%`: modulo (remainder on division)
- `//`: quotient
- `\\`: remainder
- `quotientRemainder`: integer quotient and remainder
- `quotient`: integer quotient
- `remainder`: integer remainder

## Divisor Functions

- `divisorSigma`: the divisor function
- `divisorSum`: sum of divisors
- `divisors`: list of integers that divide

## Error Functions

- `erf`: error function
- `erfc`: complementary error function
- `inverseErf`: inverse error function

## Exponential Functions

- `^`: raised to
- `cubeRoot`: real-number fractional powers
- `exp`: exponential
- `log10`: base-10 logarithms
- `log2`: base-2 logarithms
- `log`: logarithm
- `nthRoot`: surd
- `sqrt`: square root

## Factoring

- `factorInteger`: find the factors of an integer
- `integerExponent`: highest divisble power of

## Fourier Analysis

- `fft`: fast Fourier transform
- `ifft`: inverse fast Fourier transform

## Fractal Functions

- `blancmangeFunction`: blancmange function
- `minkowskiQuestionMark`: Minkowski ? function
- `minkowskiQuestionMarkInverse`: inverse Minkowski ? function
- `weierstrassFunction`: Weierstrass function

## Integer Decompositions

- `divisors`: list of integer divisors
- `factorInteger`: list of prime factors
- `fareySequence`: the Farey sequence
- `integerDigits`: list of the digits in integer

## Integer Partitions

- `integerPartitions`: find partitions of integers
- `partitionFunctionP`: unrestricted partitions
- `partitionFunctionQ`: restricted partitions
- `partitionsP`: count unrestricted partitions
- `partitionsQ`: count restricted partitions

## Interpolation Functions

- `bilinearInterpolation`
- `catmullRomInterpolation`
- `cosineInterpolation`
- `cubicInterpolation`
- `hermiteInterpolation
- `linearInterpolation`
- `listInterpolation`
- `matrixInterpolation`

## Numerical Functions

- `boole`: 1 for true, 0 for false
- `ceiling`: round towards +∞
- `floor`: round towards -∞
- `fractionPart`: fractional part
- `integerPart`: integer part
- `mixedFractionParts`: integer and fractional parts
- `round`: round to nearest integer
- `sign`: sign of

## Order Statistics

- `max`: maximun
- `minMax`: minimum and maximun
- `min`: minimum
- `ordering`: grade
- `quantile`: quantile estimate
- `quartiles`: quartile estimate
- `sort`: sort

## One-Dimensional Lists

- `Range`: range of values in equal steps
- `subdivide`: list of values from equal subdivisions

## Permutations

- `lexicographicPermutations`: permutations in lexicographic sequence
- `minimumChangePermutations`: permutations in minimum change sequence (Heap’s algorithm)
- `permutations`: permutations
- `plainChanges`: permutations in plain changes sequence (Steinhaus-Johnson-Trotter)

## Permutations - Lists

- `ordering`: inverse of a permutation list
- `randomSample`: random generation of permutation lists

## Permutations - Operations

- `findPermutation`: permutation linking two expressions
- `inversePermutation`: inverse of permutation
- `permutationPower`: _n_-th power of permutation
- `permutationProduct`: product of permutations
- `permutationReplace`: standard action of a permutation on other objects
- `permutations`: all permutations of arguments of an expression
- `permute`: permute arguments of an expression

## Permutations - Properties

- `permutationLength`: number of integers moved by permutation
- `permutationMax`: largest integer moved by permutation
- `permutationMin`: smallest integer moved by permutation
- `permutationOrder`: order of a permutation
- `permutationSupport`: integers that are not fixed by permutation

## Permutations - Representation

- `cycles`: cyclic permutation representation
- `isPermutationCycles`: test validity
- `isPermutationList`: test validity
- `permutationCycles`: convert to cyclic representation
- `permutationList`: convert to permutation list representation
- `randomPermutation`: random generation of permutations

## Piecewise Functions

- `Piecewise`: general piecewise function
- `abs`: absolute value
- `max`: maximum value
- `min`: minimum value

## Prime Numbers

- `indexOfPrime`: index of prime number in list of primes
- `leastPrimeGreaterThanOrEqualTo`: next prime or identity
- `nextPrime`: next prime
- `nthPrimeGap`: the nth prime gap
- `nthPrime`: the nth prime number
- `previousPrime`: previous prime
- `primeDivisors`: prime divisors
- `primeExponents`: prime exponents
- `primeFactorization`:  prime factors
- `primeFactors`: prime factors
- `primeLimit`: largest prime factor
- `primeNu`: number of distinct prime factors
- `primeOmega`: number of prime factors
- `primePi`: number of primes less than or equal to
- `primesBetweenAnd`: prime numbers in range
- `primesList`: list of primes
- `primesUpToDo`: iterate over primes
- `primesUpTo`: list of primes
- `randomPrime`: pick a random prime

## Prime Numbers - Testing

- `isAlmostPrime`: is _k_-almost prime
- `isCoprime`: are coprime
- `isGaussianPrime`: is Gaussian prime
- `isLesserTwinPrime`: is lesser of twin primes
- `isPrime`: is prime
- `isPrimePower`: is integer power of a prime

## Prime Numbers - Algorithms

- `cachedPrimesList`: cached list of primes
- `isCachedPrime`: is in cached list of primes
- `isPrimeTrialDivision`: is prime by trial division
- `isPrimeWilson`: is prime by Wilson
- `millerRabinPrimalityTest`: is likely prime by Miller-Rabin
- `primeFactorsTrialDivision`: prime factors by trial division
- `primesListWheelSieve`: list of primes by wheel sieve

## Rational Number Functions

- `denominator`: denominator of a fraction
- `numerator`: numerator of a fraction

## Resampling Functions

- `downsample`
- `downsampleSteinarsson`
- `matrixResample`
- `resample`
- `upsample`

## Set Functions

- `complement`: find the complement with respect to a universal set
- `groupings`: find all possible groupings of elements
- `intersection`: find the intersection of any number of sets
- `subsets`: find all possible subsets of a set (the powerset)
- `symmetricDifference`: find the symmetric difference of any number of sets
- `tuples`: find all possible tuples of n elements from a set
- `union`: find the union of any number of sets

## Special Functions

- `besselJ`: Bessel function of the first kind

## Statistics Functions

- `centralMoment`: order r central moment
- `moment`: order r moment
- `rootMeanSquare`: root mean square

## Statistics Functions - Dispersion

- `interquartileRange`: difference between the upper and lower quartiles
- `meanDeviation`: mean absolute deviation
- `standardDeviation`: standard deviation
- `variance`: unbiased estimate of variance

## Statistics Functions - Location

- `commonest`: most common items
- `contraharmonicMean`: contraharmonic mean
- `geometricMean`: geometric mean
- `harmonicMean`: harmonic mean
- `mean`: mean of collection
- `median`: median of sequence

## Statistics Functions - Shape

- `kurtosis`: coefficient of kurtosis
- `skewness`: coefficient of skewness

## Trigonometric Functions

- `arcCos`: arc cosine
- `arcSin`: arc sine
- `arcTan`: arc tangent
- `cos`: cosine
- `sin`: sine
- `sinc`: sinc
- `tan`: tangent

## Trigonometric Functions - Hyperbolic

- `arcCosh`: arc hyperbolic cosine
- `arcSinh`: arc hyperbolic sine
- `arcTanh`: arc hyperbolic tangent
- `cosh`: hyperbolic cosine
- `sinh`: hyperbolic sine
- `tanh`: hyperbolic tangent

## Type Predicates

- `isComplex`: is complex number predicate
- `isFraction`: is fraction predicate
- `isInteger`: is small integer predicate
- `isLargeInteger`: is large integer predicate
- `isNumber`: is number predicate
- `isSmallFloat`: is small float predicate

## Vector Functions

- `at`: the _i_-th element of a vector
- `dot`: dot product of two vectors
- `norm`: the norm of a vector
- `normalize`: scale to be unit vector
- `standardize`: shift to have zero mean and unit sample variance
- `projection`: orthogonal projection
- `orthogonalize`: orthonormal to vectors

## Vector Functions - Paths

- `anglePath`: form a path from a sequence steps
- `angleVector`: create a vector at a specified angle
- `circlePoints`: equally distributed points around a circle

## Window Functions

- `bartlettWindow`: Bartlett apodization function
- `blackmanWindow`: Blackman apodization function
- `blackmanHarrisWindow`: Blackman-Harris apodization function
- `dirichletWindow`: Dirichlet apodization function
- `gaussianWindow`: Gaussian apodization function
- `hammingWindow`: Hamming apodization function
- `hannWindow`: Hann apodization function
- `welchWindow`: Welch  apodization function

* * *

Guides: Geometry Functions, Mathematical Constants, Matrix Functions, Window Functions
