# Apl Perspective

Operators:

- [&](https://aplwiki.com/wiki/And) And ∧,
  [|](https://aplwiki.com/wiki/Or) Or ∨
- [+](https://aplwiki.com/wiki/Add) Add,
  [-](https://aplwiki.com/wiki/Subtract) Subtract,
  [-](https://aplwiki.com/wiki/Negate) Negate,
  [*](https://aplwiki.com/wiki/Times) Times ×,
  [/](https://aplwiki.com/wiki/Divide) Divide ÷,
  [/](https://aplwiki.com/wiki/Reciprocal) Reciprocal ÷,
  [%](https://aplwiki.com/wiki/Residue) Residue |,
  [^](https://aplwiki.com/wiki/Power_(function)) Power,
  [!](https://aplwiki.com/wiki/Factorial) Factorial !
- [@*](https://aplwiki.com/wiki/From) From,
  [@>](https://aplwiki.com/wiki/Pick) Pick ⊃
- [=](https://aplwiki.com/wiki/Equal_to) Equal to,
  [~=](https://aplwiki.com/wiki/Not_Equal_to) Not Equal to,
  [~](https://aplwiki.com/wiki/Match) Match ≡,
  [!~](https://aplwiki.com/wiki/Not_Match) Not Match ≢
- [<=](https://aplwiki.com/wiki/Less_than_or_Equal_to) Less than or Equal to,
  [<](https://aplwiki.com/wiki/Less_than) Less than,
  [>=](https://aplwiki.com/wiki/Greater_than_or_Equal_to) Greater than or Equal to,
  [>](https://aplwiki.com/wiki/Greater_than) Greater than
- [#](https://aplwiki.com/wiki/Replicate) Replicate /
- [:=](https://aplwiki.com/wiki/Assignment) Assignment ←

Types:

- [Complex](https://aplwiki.com/wiki/Complex_number) Complex number
- [Matrix](https://aplwiki.com/wiki/Matrix) Matrix

Methods:

- [abs](https://aplwiki.com/wiki/Magnitude) Magnitude |
- [atop](https://aplwiki.com/wiki/Atop_(operator)) Atop
- [bindLeft](https://aplwiki.com/wiki/Bind) Bind ∘
- [bindRight](https://aplwiki.com/wiki/Bind) Bind ∘
- [binomial](https://aplwiki.com/wiki/Binomial) Binomial !
- [ceiling](https://aplwiki.com/wiki/Ceiling) Ceiling ⌈
- [isCloseTo](https://aplwiki.com/wiki/Match) Match ≡
- [composeLeft](https://aplwiki.com/wiki/Compose) Compose
- [conjugated](https://aplwiki.com/wiki/Conjugate) Conjugate
- [constant](https://aplwiki.com/wiki/Constant) Constant ⍨
- [drop](http://aplwiki.com/wiki/Drop) Drop ↓
- [each](https://aplwiki.com/wiki/Each) Each ¨
- [enclose](https://aplwiki.com/wiki/Enclose) Enclose ⊂
- [exp](https://aplwiki.com/wiki/Exponential) Exponential *
- [flattenTo](https://aplwiki.com/wiki/Ravel) Ravel
- [flatten](https://aplwiki.com/wiki/Ravel) Ravel
- [floor](https://aplwiki.com/wiki/Floor) Floor ⌊
- [gcd](https://aplwiki.com/wiki/GCD) GCD ∨
- [i](https://aplwiki.com/wiki/Imaginary) Imaginary ⌾
- [identity](https://aplwiki.com/wiki/Identity) Identity ⊣
- [log](https://aplwiki.com/wiki/Logarithm) Logarithm ⍟
- [includes](https://aplwiki.com/wiki/Membership) Membership ∊
- [inner](https://aplwiki.com/wiki/Inner_Product) Inner Product .
- [iota](https://aplwiki.com/wiki/Index_Generator) Index Generator ⍳
- [intersection](https://aplwiki.com/wiki/Intersection) Intersection ∩
- [iterate](https://aplwiki.com/wiki/Power_(operator)) Power *
- [j](https://aplwiki.com/wiki/Complex_(function)) Complex
- [lcm](https://aplwiki.com/wiki/LCM) LCM ∧
- [leftIdentity](https://aplwiki.com/wiki/Identity) Identity ⊣
- [max](https://aplwiki.com/wiki/Maximum) Maximum ⌈
- [min](https://aplwiki.com/wiki/Minimum) Minimum ⌊
- [mixedRadixEncode](https://aplwiki.com/wiki/Encode) Encode
- [nest](https://aplwiki.com/wiki/Nest) Nest ⊆
- [ordering](https://aplwiki.com/wiki/Grade) Grade ⍋
- [nthRoot](https://aplwiki.com/wiki/Root) Root √
- [nub](https://aplwiki.com/wiki/Unique) Unique ∪
- [nubSieve](https://aplwiki.com/wiki/Nub_Sieve) Nub Sieve ≠
- [outer](https://aplwiki.com/wiki/Outer_Product) Outer Product ∘.
- [over](https://aplwiki.com/wiki/Over) Over ⍥
- [rank](https://aplwiki.com/wiki/Rank) Rank
- [reduce](https://aplwiki.com/wiki/Reduce) Reduce /
- [replicateEach](https://aplwiki.com/wiki/Replicate) Replicate /
- [reshape](https://aplwiki.com/wiki/Reshape) Reshape ⍴
- [reversed](https://aplwiki.com/wiki/Reverse) Reverse ⌽
- [rotatedLeft](https://aplwiki.com/wiki/Rotate) Rotate ⌽
- [rightIdentity](https://aplwiki.com/wiki/Identity) Identity ⊢
- [scanLeftAssociatingRight](https://aplwiki.com/wiki/Scan) Scan
- [scan](https://aplwiki.com/wiki/Scan) Scan \
- [shape](https://aplwiki.com/wiki/Shape) Shape ⍴
- [sign](https://aplwiki.com/wiki/Signum) Signum ×
- [size](https://aplwiki.com/wiki/Tally) Tally ≢
- [swap](https://aplwiki.com/wiki/Commute) Commute ⍨
- [take](http://aplwiki.com/wiki/Take) Take ↑
- [transposed](https://aplwiki.com/wiki/Transpose) Transpose ⍉
- [windowedReduce](https://aplwiki.com/wiki/Windowed_Reduce) Windowed Reduce /
- [withLevelCollect](https://aplwiki.com/wiki/Depth_(operator)) Depth ¨
