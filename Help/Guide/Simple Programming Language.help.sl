# Simple Programming Language

Simple Programming Language (Sᴘʟ) is a notation for working with a minimal procedural language.

The language consists of lexically scoped closures with mutable environments, record types, and a non-local return mechanism.

<!-- Notes from a [SuperCollider Perspective], a [Smalltalk Perspective], and a [Scheme Perspective]. -->

* * *

Unicode: U+01D18 ᴘ Latin Letter Small Capital P, U+0029F ʟ Latin Letter Small Capital L
