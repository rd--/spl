# Message Cascades

In Smalltalk there is a _cascade_ operator for sending several messages to and object in a single expression.

Writing _x p; q; r_ sends _p_ then _q_ then _r_ to _x_ in turn.

The methods `also`, `in` and `with` allow a similar idiom.

* * *

See also: also, in, with
