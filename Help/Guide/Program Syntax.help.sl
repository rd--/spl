# Program Syntax

A _program_ is an expression that can be evaluated to give an _answer_.

The general form is

> _temporaries? statements?_

Temporaries are written using either `Var Syntax` or `Let Syntax`.

Statements are separated by a semicolon (`;`).

A valid program can be enclosed within braces to define a no-argument `Block`.

* * *

See also: ;, let, var

Guides: Block Syntax, Let Syntax, Var Syntax

Categories: Syntax
