# Small Hours

Small Hours is an experimental interpreter for the experimental `Simple Programming Language`.

It includes a library for communicating with the `SuperCollider Synthesiser`.

To play a synthesiser program select it,
or place the caret anywhere within it,
and type _Ctrl-Enter_.

```
/* https://sccode.org/1-4Qy ; f0 ; 0283 */
let b = 1 / 2:6;
let o1 = SinOscFb(
	SinOscFb(b, 1) < b * 500 + 99,
	0
) / 5;
let o2 = SinOscFb(
	999 * b,
	SinOscFb(
		SinOscFb(b, 1) < 0.1 + 1,
		1
	) % b
);
let o3 = SinOscFb(
	0.1 - b,
	1
).Min(0);
(o1 + (o2 * o3)).Splay / 2
```

To reset the synthesiser type _Ctrl-FullStop_.

To evaluate an expression without playing it type _Ctrl-Shift-Enter_.

To get help about a word, for instance `SinOscFb`, select it and type _Ctrl-QuestionMark_.

Text in brackets, for instance `Simple Editor`, indicates that the bracketed text is the name of a help document,
which can be visited the same way.

There is a language `Help Index` and a `Unit Generator Index` where terms are sorted alphabetically.
