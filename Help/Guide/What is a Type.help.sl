# What is a Type?

A `Type` is the abstract description a set of values.
Each value in Spl is an _instance_ of a type.
`typeOf` answers the name of the type of a value.

```
>>> 3.141.typeOf
'SmallFloat'
```

* * *

See also: Type, typeOf

Guides: Primitive Types, What is a Trait
