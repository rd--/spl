# Control Structures

## Boolean Choice

- `ifEmpty`
- `ifFalse`
- `ifNil`
- `ifNotNil`
- `ifTrue`
- `if`

## Branch Choice

- `caseOf`
- `caseOfOtherwise`
- `which`

## Conditional Iteration

- `doWhileFalse`
- `doWhileTrue`
- `whileTrue`
- `whileFalse`

## Collection Iteration

- `associationsDo`
- `do`
- `indicesDo`
- `keysAndValuesDo`
- `keysDo`
- `valuesDo`
- `withDo`
- `withIndexDo`

* * *

References:
_W_
[1](https://en.wikipedia.org/wiki/Control_flow)
