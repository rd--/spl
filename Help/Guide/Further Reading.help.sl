# Further Reading

1
:   M. Abramowitz and I.A. Stegun, editors. *Handbook of Mathematical
    Functions with Formulas, Graphs, and Mathematical Tables*. National
    Bureau of Standards, Washington, DC, 1964. ISBN 0-486-61272-4.

2
:   D. Aiylam and T. Khovanova. Stern-Brocot Trees from Weighted
    Mediants. 2016.
    [doi:10.48550/arXiv.1502.00682](https://doi.org/10.48550/arXiv.1502.00682).

3
:   W.S. Andrews and others. *Magic Squares and Cubes*. Open Court,
    Chicago, 1908.

4
:   J.M. Barbour. *Tuning and Temperament: A Historical Survey*.
    Michigan State College Press, 1953.

5
:   C.A. Barlow. Two Essays on Theory. *Computer Music Journal*,
    11(1):44--60, 1987. Translated by Henning Lohner.
    [doi:10.2307/3680177](https://doi.org/10.2307/3680177).

6
:   S. Beatty and others. Problems for Solutions: 3173-3180. *The
    American Mathematical Monthly*, 33(3):159--159, 1926.
    [doi:10.2307/2300153](https://doi.org/10.2307/2300153).

7
:   T.J. Berners-Lee, R. Cailliau, A. Luotonen, H.F. Nielsen, and
    A. Secret. The World-Wide Web. *Commun. ACM*, 37(8):76--82,
    Aug. 1994.
    [doi:10.1145/179606.179671](https://doi.org/10.1145/179606.179671).

8
:   E. Bjorklund. Algorithms for Optimally Distributed Timing Pulses.
    Technical Report LA-UR-89-3558, Los Alamos National Laboratory,
    1989.

9
:   N. Calkin and H.S. Wilf. Recounting the Rationals. *Amer. Math.
    Monthly*, 107(4):360--363, 2000.
    [doi:10.1080/00029890.2000.12005205](https://doi.org/10.1080/00029890.2000.12005205).

10
:   H. Cannon. Flavors: A Non-hierarchical Approach to Object-oriented
    Programming. Technical Report, Symbolics Inc., 1982.

11
:   J.H. Chalmers. *Divisions of the Tetrachord: A Prolegomenon to the
    Construction of Musical Scales*. Frog Peak Music, NH, 1993.
    ISBN 9780945996040. Edited by L. Polansky and C. Scholz.

12
:   A.R. Clarke. Just Intonation. *Nature*, 15:253, 1877.
    [doi:10.1038/015253b0](https://doi.org/10.1038/015253b0).

13
:   G. Curry, L. Baer, D. Lipkie, and B. Lee. Traits: An Approach to
    Multiple-inheritance Subclassing. In *Proceedings of the SIGOA
    Conference on Office Information Systems*, 1--9. 1982.
    [doi:10.1145/800210.806468](https://doi.org/10.1145/800210.806468).

14
:   E.D. Demaine, F. Gomez-Martin, H. Meijer, D. Rappaport,
    P. Taslakian, G.T. Toussaint, T. Winograd, and D.R. Wood. The
    Distance Geometry of Deep Rhythms and Scales. In *Proc. Canadian
    Conference on Computational Geometry*, 163--166. Aug. 2005.

15
:   E. Deutsch. Dyck Path Enumeration. *Discrete Mathematics*,
    204(1):167--202, 1999. Selected papers in honor of Henry W. Gould.
    [doi:10.1016/S0012-365X(98)00371-9](https://doi.org/10.1016/S0012-365X(98)00371-9).

16
:   P. Duchon. On the Enumeration and Generation of Generalized Dyck
    Words. *Discrete Mathematics*, 225(1):121--135, 2000. FPSAC\'98.
    [doi:10.1016/S0012-365X(00)00150-3](https://doi.org/10.1016/S0012-365X(00)00150-3).

17
:   D.D. Dunn, editor. *Harry Partch: An Anthology of Critical
    Perspectives*. Number 19 in Contemporary Music Studies. Harwood,
    Amsterdam, 2000. ISBN 9789057550652.

18
:   R. Durstenfeld. Algorithm 235: Random permutation. *Commun. ACM*,
    7(7):420, Jul. 1964.
    [doi:10.1145/364520.364540](https://doi.org/10.1145/364520.364540).

19
:   P. Erdös and G. Szckeres. A Combinatorial Problem in Geometry.
    *Compositio Mathematica*, 2:463--470, 1987.
    [doi:10.1007/978-0-8176-4842-8_3](https://doi.org/10.1007/978-0-8176-4842-8_3).

20
:   L. Euler. *Tentamen Novae Theoriae Musicae*. Imperial Academy of
    Sciences, St. Petersburg, 1739.

21
:   A.S. Fraenkel and Clark Kimberling. Generalized Wythoff arrays,
    shuffles and interspersions. *Discrete Mathematics*,
    126(1):137--149, 1994.
    [doi:10.1016/0012-365X(94)90259-3](https://doi.org/10.1016/0012-365X(94)90259-3).

22
:   M.L. Fredman. On Computing the Length of Longest Increasing
    Subsequences. *Discrete Mathematics*, 11(1):29--35, 1975.
    [doi:10.1016/0012-365X(75)90103-X](https://doi.org/10.1016/0012-365X(75)90103-X).

23
:   S.W. Golomb. *Shift Register Sequences*. Aegean Park Press, Laguna
    Hills, CA, 1967. ISBN 978-0894120480.

24
:   J.M. Hammersley. A Few Seedlings of Research. In *Berkeley Symp. on
    Math. Statist. and Prob.*, volume 6, 345--394. 1972.

25
:   R.W. Hamming. Error detecting and error correcting codes. *The Bell
    System Technical Journal*, 29(2):147--160, 1950.
    [doi:10.1002/j.1538-7305.1950.tb00463.x](https://doi.org/10.1002/j.1538-7305.1950.tb00463.x).

26
:   G.H. Hardy. Weierstrass\'s Non-Differentiable Function.
    *Transactions of the American Mathematical Society*, 17(3):301,
    Jul. 1916. [doi:10.2307/1989005](https://doi.org/10.2307/1989005).

27
:   B.R. Heap. Permutations by Interchanges. *The Computer Journal*,
    6(3):293--294, 1963.
    [doi:10.1093/comjnl/6.3.293](https://doi.org/10.1093/comjnl/6.3.293).

28
:   D.S. Hirschberg. A Linear Space Algorithm for Computing Maximal
    Common Subsequences. *Commun. ACM*, 18(6):341--343, Jun. 1975.
    [doi:10.1145/360825.360861](https://doi.org/10.1145/360825.360861).

29
:   A.F. Horadam. Wythoff Pairs. *The Fibonacci Quarterly*, Apr. 1978.

30
:   J.W. Hunt and T.G. Szymanski. A Fast Algorithm for Computing Longest
    Common Subsequences. *Commun. ACM*, 20(5):350--353, May 1977.
    [doi:10.1145/359581.359603](https://doi.org/10.1145/359581.359603).

31
:   D.H.H. Ingalls. A Simple Technique for Handling Multiple
    Polymorphism. In *OOPLSA \'86*. 1986.
    [doi:10.1145/28697.28732](https://doi.org/10.1145/28697.28732).

32
:   S.M. Johnson. Generation of Permutations by Adjacent Transposition.
    *Math. Comp.*, 17:282--285, 1963.
    [doi:10.1090/S0025-5718-1963-0159764-2](https://doi.org/10.1090/S0025-5718-1963-0159764-2).

33
:   K. Karplus and A. Strong. Digital Synthesis of Plucked-String and
    Drum Timbres. *Computer Music Journal*, 7(2):43--55, 1983.
    [doi:10.2307/3680062](https://doi.org/10.2307/3680062).

34
:   J. Kepler. *Harmonices Mundi*. Johann Planck, Linz, 1619.

35
:   D.E. Knuth. *The Art of Computer Programming: Volume 1: Fundamental
    Algorithms*. Addison-Wesley, Reading, MA, 1968.

36
:   D.E. Knuth. *The Art of Computer Programming: Volume 3: Sorting and
    Searching*. Addison-Wesley, Reading, MA, 1973.

37
:   E. Kurenniemi. Chords, Scales, and Divisor Lattices \[2003\]. In
    Joasia Krysa and Jussi Parikka, editors, *Writing and Unwriting
    (Media) Art History: Erkki Kurenniemi in 2048*. MIT Press, 09 2015.
    [doi:10.7551/mitpress/10014.003.0026](https://doi.org/10.7551/mitpress/10014.003.0026).

38
:   Z. Kása. Generating and Ranking of Dyck Words. *Acta Univ.
    Sapientiae, Informatica*, 1(1):109--118, 2009. URL:
    <https://arxiv.org/pdf/1002.2625.pdf>.

39
:   P. L\'Ecuyer and R. Simard. TestU01: A C library for Empirical
    Testing of Random Number Generators. *ACM Trans. Math. Softw.*,
    Aug. 2007.
    [doi:10.1145/1268776.1268777](https://doi.org/10.1145/1268776.1268777).

40
:   G.N. Lance and W.T. Williams. Computer Programs for Hierarchical
    Polythetic Classification ("Similarity
    Analyses"). *The Computer Journal*, 9(1):60--64,
    05 1966.
    [doi:10.1093/comjnl/9.1.60](https://doi.org/10.1093/comjnl/9.1.60).

41
:   M. Lassalle. Two Integer Sequences Related to Catalan Numbers.
    *Journal of Combinatorial Theory, Series A*, 119(4):923--935, 2012.
    [doi:10.1016/j.jcta.2012.01.002](https://doi.org/10.1016/j.jcta.2012.01.002).

42
:   D.H. Lehmer. On Stern\'s Diatomic Series. *The American Mathematical
    Monthly*, 36(2):59--67, 1929.
    [doi:10.1080/00029890.1929.11986912](https://doi.org/10.1080/00029890.1929.11986912).

43
:   V.I. Levenshtein. Binary Codes Capable of Correcting Deletions,
    Insertions, and Reversals. *Soviet Physics Doklady*, 10(8):707--710,
    Feb. 1966.

44
:   C.L. Mallows. Patience Sorting. *SIAM Rev.*, 4(2):148--149,
    Apr. 1962. [doi:10.1137/1004036](https://doi.org/10.1137/1004036).

45
:   M. Matsumoto and T. Nishimura. Mersenne Twister: A 623-dimensionally
    Equidistributed Uniform Pseudo-random Number Generator. *ACM Trans.
    Model. Comput. Simul.*, 8(1):3--30, Jan. 1998.
    [doi:10.1145/272991.272995](https://doi.org/10.1145/272991.272995).

46
:   J. McCartney. SuperCollider: a New Real-time Synthesis Language. In
    *Proc. ICMC*. 1996. URL:
    <http://hdl.handle.net/2027/spo.bbp2372.1996.078>.

47
:   J. McCartney. Continued evolution of the SuperCollider real time
    synthesis environment. In *Proc. ICMC*, 133--136. 1998. URL:
    <http://hdl.handle.net/2027/spo.bbp2372.1998.262>.

48
:   J. McCartney. Rethinking the Computer Music Language: SuperCollider.
    *Computer Music Journal*, 26(4):61--68, 2002.
    [doi:10.1162/014892602320991383](https://doi.org/10.1162/014892602320991383).

49
:   E.E. McDonnell. A Notation for the GCD and LCM Functions. In
    *Proceedings of Seventh International Conference on APL*,
    240--243. 1975.
    [doi:10.1145/800117.803810](https://doi.org/10.1145/800117.803810).

50
:   A.L.F. Meister. Generalia de genesi figurarum planarum et inde
    pendentibus earum affectionibus. *Novi Commentarii Societatis Regiae
    Scientiarum Gottingensis*, pages 144--180, 1769.

51
:   M. Meyer. *The Musician\'s Arithmetic: Drill Problems for an
    Introduction to the Scientific Study of Musical Composition*. The
    University of Missouri, 1929.

52
:   D.A. Moon. Object-Oriented Programming with Flavors. *SIGPLAN Not.*,
    21(11):1--8, Jun. 1986.
    [doi:10.1145/960112.28698](https://doi.org/10.1145/960112.28698).

53
:   K. Moreland. Diverging Color Maps for Scientific Visualization. In
    *Advances in Visual Computing*, 92--103. 2009.
    [doi:10.1007/978-3-642-10520-3_9](https://doi.org/10.1007/978-3-642-10520-3_9).

54
:   D.R. Morrison. A Stolarsky array of Wythoff pairs. In *A Collection
    of Manuscripts Related to the Fibonacci Sequence*, pages 134--136.
    Fibonacci Association, Santa Clara, 1980.

55
:   H. Partch. *Genesis of a Music*. University of Wisconsin Press,
    Madison, 1949.

56
:   T. Porter and T. Duff. Compositing Digital Images. *SIGGRAPH Comput.
    Graph.*, 18(3):253--259, Jan. 1984.
    [doi:10.1145/964965.808606](https://doi.org/10.1145/964965.808606).

57
:   R. Rasch. A Word or Two on the Tunings of Harry Partch. In *Harry
    Partch: An Anthology of Critical Perspectives*. 2000.

58
:   D. Rotem. Stack sortable permutations. *Discrete Mathematics*,
    33(2):185--196, 1981.
    [doi:10.1016/0012-365X(81)90165-5](https://doi.org/10.1016/0012-365X(81)90165-5).

59
:   F. Ruskey. Transposition Generation of Alternating Permutations.
    *Order*, 6(3):227--233, 1989.
    [doi:10.1007/BF00563523](https://doi.org/10.1007/BF00563523).

60
:   S. Sattolo. An Algorithm to Generate a Random Cyclic Permutation.
    *Information Processing Letters*, 22(6):315--317, 1986.
    [doi:10.1016/0020-0190(86)90073-6](https://doi.org/10.1016/0020-0190(86)90073-6).

61
:   C. Schensted. Longest Increasing and Decreasing Subsequences.
    *Canadian Journal of Mathematics*, 13:179--191, 1961.
    [doi:10.4153/CJM-1961-015-3](https://doi.org/10.4153/CJM-1961-015-3).

62
:   R. Sedgewick. Permutation Generation Methods. *ACM Comput. Surv.*,
    9(2):137--164, Jun. 1977.
    [doi:10.1145/356689.356692](https://doi.org/10.1145/356689.356692).

63
:   V.W. de Spinadel and A. Redondo Buitrago. Towards van der Laan\'s
    Plastic Number in the Plane. *Journal for Geometry and Graphics*,
    13(2):163--175, 2009. URL:
    <https://www.heldermann.de/JGG/JGG13/JGG132/jgg13014.htm>.

64
:   G.L. Steele, D. Lea, and C. Flood. Fast Splittable Pseudorandom
    Number Generators. In *Proc. OOPSLA*. 2014.
    [doi:10.1145/2660193.2660195](https://doi.org/10.1145/2660193.2660195).

65
:   S. Steinarsson. Downsampling Time Series for Visual Representation.
    Master\'s thesis, University of Iceland, 2013.

66
:   M.A. Stern. Uber eine zahlentheoretische Funktion. *J. ReineAngew.
    Math.*, 55:193--220, 1858.

67
:   T. Takagi. A Simple Example of the Continuous Function without
    Derivative. *Proc. Phys.-Math. Soc. Jpn.*, 1:176--177, 1901.
    [doi:10.11429/subutsuhokoku1901.1.F176](https://doi.org/10.11429/subutsuhokoku1901.1.F176).

68
:   H.K. Taube. Complex Musical Pattern Description in Common Music. In
    *Proc. ICMC*. 1994. URL:
    <https://hdl.handle.net/2027/spo.bbp2372.1994.069>.

69
:   R.C. Tausworthe. Random numbers generated by linear recurrence
    modulo two. *Math. Comp.*, 1965.
    [doi:10.1090/S0025-5718-1965-0184406-1](https://doi.org/10.1090/S0025-5718-1965-0184406-1).

70
:   B.E. Tenner. Reduced Word Manipulation: Patterns and Enumeration.
    *Journal of Algebraic Combinatorics*, 46(1):189--217, 2017.
    [doi:10.1007/s10801-017-0752-8](https://doi.org/10.1007/s10801-017-0752-8).

71
:   W.E. Thomson. A Modified Congruence Method of Generating
    Pseudo-random Numbers. *The Computer Journal*,
    1(2):83--83, 1958.
    [doi:10.1093/comjnl/1.2.83](https://doi.org/10.1093/comjnl/1.2.83).

72
:   H.F. Trotter. Algorithm 115: Perm. *Commun. ACM*, 5(8):434--435,
    Aug. 1962.
    [doi:10.1145/368637.368660](https://doi.org/10.1145/368637.368660).

73
:   J.W. Tukey. *Exploratory Data Analysis*. Pearson, 1977. ISBN
    0-201-07616-0.

74
:   M.D. Vose. A linear algorithm for generating random numbers with a
    given distribution. *IEEE Transactions on Software Engineering*,
    17(9):972--975, 1991.
    [doi:10.1109/32.92917](https://doi.org/10.1109/32.92917).

75
:   A.J. Walker. New fast method for generating discrete random numbers
    with arbitrary frequency distributions. *Electronics Letters*,
    April 1974.
    [doi:10.1049/el:19740097](https://doi.org/10.1049/el:19740097).

76
:   D. Weinreb and D.A. Moon. Flavors: Message Passing in the Lisp
    Machine. Technical Report, MIT AI Lab, 1980. URL:
    <http://hdl.handle.net/1721.1/5700>.

77
:   M.J. Wright and A. Freed. Open Sound Control: A New Protocol for
    Communicating with Sound Synthesizers. In *Proc. ICMC*,
    101--104. 1997. URL:
    <http://hdl.handle.net/2027/spo.bbp2372.1997.033>.

78
:   Unicode Consortium. *Unicode Standard:
    Worldwide Character Encoding, Version 1.0*. Addison-Wesley, Reading,
    MA, 1992. ISBN 978-0201608458. URL:
    <https://www.unicode.org/versions/Unicode1.0.0/>.
