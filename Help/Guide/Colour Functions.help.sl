# Colour Functions

## Traits and Types

- `Colour`
- `ColourGradient`
- `ColourPalette`
- `RgbColour`

## Constructors

- `HsvColour`

## Accessing

- `alpha`
- `blue`
- `green`
- `hsv`
- `hue`
- `red`
- `rgb`
- `rgba`
- `srgb`

## Catalogues

- `svgColourCatalogue`

## Converting

- `asColour`: as Colour
- `cmyToRgb`: Cmy to Rgb
- `hslToHsv`: Hsl to Hsv
- `hslToRgb`: Hsl to Rgb
- `hsvToHsl`: Hsv to Hsl
- `hsvToRgb`: Hsv to Rgb
- `labToXyz`: Lab to Xyz
- `lchToLuv`: Lch to Luv
- `luvToXyz`: Luc to Xyz
- `oklabToXyz`: Oklab to Xyz
- `rgbToHsl`: Rgb to Hsl
- `rgbToHsv`: Rgb to Hsv
- `rgbToXyz`: Rgb to Xyz
- `xyyToXyz`: Xyy to Xyz
- `xyzToOklab`: Xyz to Oklab
- `oklabToXyz`: Oklab to Xyz
- `xyzToRgb`: Xyz to Rgb
- `xyzToXyy`: Xyz to Xyy

## Drawing

- `arrayPlot`
- `asGreyscaleSvg`
- `asColourSvg`

## Encoding and Decoding

- `adobeRgbDecode`
- `adobeRgbEncode`
- `srgbDecode`
- `srgbEncode`

## Gradients and Palettes

- `asColourGradient`
- `asContinuousColourGradient`
- `asDiscreteColourGradient`
- `colourGradients`
- `colourPalettes`
- `cubeHelix`
- `namedColourGradient`
- `namedColourPalette`
- `parula`
- `quilezGradient`

## Parsers and Printers

- `parseHexColour`
- `parseHexTriplet`
- `parseRgbColour`
- `parseRgbTriplet`

## Physics

- `planckRadiationLaw`
- `plancksRadiationFunction`

## Standards

- `lightnessCie`
