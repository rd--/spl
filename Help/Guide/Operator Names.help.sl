# Operator Names

The methods that implement operators are named according to the operator names.
Defining `*` defines _asterisk_, and defining `++` defines _plusSignPlusSign_.

Help files are named according to the operator name.
(Many operator tokens are not allowed in file names of many systems.)

Operator token names:

- `!`: exclamationMark
- `#`: numberSign
- `$`: dollarSign
- `%`: percentSign
- `&`: ampersand
- `*`: asterisk
- `+`: plusSign
- `-`: hyphenMinus
- `<`: lessThanSign
- `=`: equalsSign
- `>`: greaterThanSign
- `/`: solidus
- `?`: questionMark
- `@`: commercialAt
- `\\`: reverseSolidus
- `^`: circumflexAccent
- `|`: verticalLine
- `~`: tilde

Syntax token names:

- `,`: comma
- `;`: semicolon
- `.`: fullStop
- `(`: leftParenthesis
- `)`: rightParenthesis
- `\[`: leftSquareBracket
- `\]`: rightSquareBracket
- `{`: leftCurlyBracket
- `}`: rightCurlyBracket
- `"`: quotationMark
- `'`: apostrophe
- `\``: graveAccent

* * *

See also: Operator List, Operator Syntax
