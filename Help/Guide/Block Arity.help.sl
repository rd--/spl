# Block Arity

Blocks have a fixed arity, as in Smalltalk.
There are no optional parameters or _rest_ arguments.

* * *

See also: cull, numArgs

Guides: Smalltalk Arity
