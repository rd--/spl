# Primitive Types

The primitive types are `List`, `Boolean`, `Nil`, `Number`, `Block`, and `String`.

The following types are implemented as primitives if the underlying system implements them:
`ByteArray`, `Error`, `Float64Array`, `LargeInteger`, `PriorityQueue`, `Promise`, `Record`, `RegExp` and `IdentitySet`.

* * *

See also: Primitive Semantics, Primitive Syntax
