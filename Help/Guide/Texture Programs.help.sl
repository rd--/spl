# Texture Programs

Texture programs generate sound by scheduling overlapping instances of a synthesiser program.

This program generates random panning three note chords.

~~~spl texture
let dur = 12;
{
	{
		Pan2(
			SinOsc(
				IRand(48, 72).MidiCps,
				0
			),
			Line(
				Rand(-1, 1),
				Rand(-1, 1),
				dur
			),
			Rand(0.01, 0.05)
		)
	} !+ 3
}.overlapTextureProgram(
	dur // 3,
	dur // 3,
	4
)
~~~

To stop a texture program the clock on which it is scheduled must be cleared.
The keybinding _Ctrl-Shift->_ clears the `system` `Clock`.

* * *

See also: collectTextureProgram, overlapTextureProgram, playEvery, spawnTextureProgram, TextureProgram, xFadeTextureProgram

Categories: Scheduling
