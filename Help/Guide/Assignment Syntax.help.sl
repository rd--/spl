# Assignment Syntax

- _variableName := expression_

Assign a value to a variable.
Variables must be declared.

```
>>> var x;
>>> x := 3;
>>> x * x
9
```

Variables defined using `let` may be mutated:

```
>>> let x = 1;
>>> x := 3;
>>> x * x
9
```

* * *

See also: :=, let, var

Guides: Let Syntax, Var Syntax

Categories: Syntax
