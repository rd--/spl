# Matrix Functions

## Types

- `List`: list type
- `Matrix`: matrix type

## Converting

- `asList`: convert matrix to list of lists
- `asMatrix`: convert list of lists to matrix

## Querying

- `elementType`: type of elements
- `numberOfColumns`: number of columns
- `numberOfRows`: number of rows
- `shape`: number of rows and columns

## Predicates

- `isIntegerMatrix`: is integer matrix
- `isLowerTriangularMatrix`: is lower triangular matrix
- `isMatrix`: is matrix
- `isOrthogonalMatrix`: is orthogonal matrix
- `isSquareMatrix`: is square matrix
- `isSymmetricMatrix`: is symmetric matrix
- `isUnitaryMatrix`: is unitary matrix
- `isUpperTriangularMatrix`: is upper triangular matrix

## Constructing

- `antidiagonalMatrix`: matrix with list items on antidiagonal
- `blockDiagonalMatrix`: matrix with matrices on block diagonal
- `boxMatrix`: square matrix
- `circulantMatrix`: circulant (rotational) matrix
- `crossMatrix`: cross matrix
- `diagonalMatrix`: matrix with list items on diagonal
- `diamondMatrix`: diamond matrix
- `diskMatrix`: disk matrix
- `distanceMatrix`: distance matrix
- `exchangeMatrix`: exchange matrix
- `fourierMatrix`: square Fourier matrix
- `frobeniusCompanionMatrix`: Frobenius companion matrix
- `hadamardMatrix`: Hadamard matrix
- `hilbertMatrix`: Hilbert matrix
- `identityMatrix`: identity matrix
- `permutationMatrix`: permutation matrix
- `sylvesterMatrix`: Sylvester matrix
- `toeplitzMatrix`: Toeplitz matrix
- `walshMatrix`: Walsh matrix
- `zeroMatrix`: zero matrix

## General

- `adjugate`: adjugate or adjoint
- `arrayFlatten`: flatten a matrix of matrices
- `arrayRules`: positions and values of non-zero elements
- `choleskyDecomposition`: the Cholesky decomposition
- `conjugateTranspose`: conjugate transpose (Hermitian conjugate)
- `determinant`: determinant
- `dot`: outer product
- `gramSchmidtProcess`: Gram-Schmidt
- `gaussJordanInverse`: inverse
- `inner`: outer product
- `inverse`: inverse of a square matrix
- `luDecomposition`: the LU decomposition
- `matrixPower`: matrix power
- `matrixPrintString`: two dimensional matrix text format
- `matrixRank`: matrix rank, the number of linearly independent rows or columns
- `matrixRotate`: matrix rotation
- `minor`: the indicated minor of a matrix
- `minors`: the minors of a matrix
- `outer`: outer product
- `permanent`: permanent of a square matrix
- `qrDecomposition`: the QR decomposition
- `ravel`: ravel order list
- `reducedRowEchelonForm`: in place row reduce
- `rowReduce`: simplified matrix obtained by making linear combinations of rows
- `shape`: list the dimensions of a matrix
- `singularValueDecomposition`
- `trace`: trace
- `transposed`: transposition

## Graph

- `adjacencyMatrix`: vertex–vertex adjacency matrix of a graph
- `connectionMatrix`: vertex–vertex connection matrix of a graph
- `incidenceMatrix`: vertex-edge incidence matrix of a graph

## Geometry

- `eulerMatrix`: Euler rotation matrix
- `reflectionMatrix`: reflection matrix
- `rotationMatrix`: rotation matrix
- `scalingMatrix`: scaling matrix

## Tuning

- `intervalMatrix`: interval matrix

* * *

Guides: Mathematical Functions
