# Csound Gen Routines

## Sine and Cosine Generators

- `gen09`: Composite waveforms made up of weighted sums of simple sinusoids.
- `gen10`: Composite waveforms made up of weighted sums of simple sinusoids.
- `gen11`: Additive set of cosine partials.
- `gen19`: Composite waveforms made up of weighted sums of simple sinusoids.

## Line and Exponential Segment Generators

- `gen05`: Constructs functions from segments of exponential curves.
- `gen07`: Constructs functions from segments of straight lines.
- `gen16`: Creates a table from a starting value to an ending value.

## Waveshaping Gen Routines

- `gen03`: Generates a stored function table by evaluating a polynomial.
- `gen13`: Stores a polynomial whose coefficients derive from the Chebyshev polynomials of the first kind.

* * *

See also: Osc, Shaper

References:
_Csound_
[1](https://www.csounds.com/manual/html/ScoreGenRef.html)
