# Operator List

Math operators:

- `+`: add
- `-`: subtract
- `*`: multiply (×)
- `/`: divide (÷)
- `^`: raise to (ˆ)
- `%`: modulo
- `//`: quotient (⫽)
- `*+`: multiply add

Comparison operators:

- `=`: equal to
- `~=`: not equal to (≠)
- `==`: identical to (≡)
- `~~`: not identical to (≢)
- `>`: greater than
- `>=`: greater than or equal to (≥)
- `>~`: greater than or similar to (⪆)
- `<`: less than
- `<=`: less than or equal to (≤)
- `<~`: less than or similar to (⪅)
- `~`: similar to
- `!~`: not similar to (≉)
- `<=>`: collation order

Logical operators:

- `&`: and
- `|`: or
- `&&`: evaluting and
- `||`: evaluating or (‖)

Conditional operators:

- `?`: if nil

Collection operators:

- `++`: append (⧺)
- `->`: associate with (→)
- `@`: at
- `@*`: at all
- `@>`: at path

Evaluating operators:

- `$`: apply

Iteration operators:

- `!`: duplicate
- `#`: replicate

* * *

Guides: Binary Operators, Operator Names, Operator Syntax
