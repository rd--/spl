# System Dictionary

The system dictionary is of type _System_ and is stored in the global variable _system_.

* * *

See also: system, System
