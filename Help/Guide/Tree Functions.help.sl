# Tree Functions

## Traits and Types

- `Tree`

## Enumerating

- `collect`
- `do`
- `deepDo`
- `leavesDo`
- `levelOrderDo`
- `levelOrderValues`
- `postOrderDo`
- `postOrderValues`
- `reverseDo`

## Predicates

- `isBinary`
- `isLeaf`
- `isTree`

## Querying

- `depth`
- `leafCount`
- `leafIndices`
- `leaves`
- `size`

## Converting

- `asGraph`
- `asList`
- `expressionTree`
- `rulesTree`

## Drawing Functions

- `graphPlot`
- `treePlot`

* * *

See also: Tree

Guides: Graph Functions
