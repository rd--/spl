# Assignment and Equality

The `Assignment Syntax` is _p := q_.

```
>>> var x;
>>> x := 1;
>>> x
1
```

The `Initialised Temporaries Syntax` is _let p = q_.
It both declares a variable and assigns a value.

```
>>> let x = 1;
>>> x
1
```

The standard library defines the equality predicate as _p = q_ and the identity predicate as _p == q_.

```
>>> 3 + 4 = 7
true

>>> 3 + 4 == 7
true
```

* * *

See also: =, :=, ==, let, var

Guides: Assignment Syntax, Comparison Functions, Initialised Temporaries Syntax
