# File Functions

_Files_:

- `readBinaryFile`: read binary file
- `readTextFile`: read text file
- `removeFile`: remove file
- `writeBinaryFile`: write binary file
- `writeTextFile`: write text file

_Directories_:

- `makeDirectory`: make directory
- `readDirectory`: read directory entries
- `readDirectoryFileNames`: file entries
- `removeDirectory`: remove directory

_File Information_:

- `fileInformation`: file information
- `modificationTime`: modification time

_File Paths_:

- `pathBasename`: basename of file path
- `pathDirectory`: directory of file path
- `pathExtension`: extension of file path
- `pathIsAbsolute`: is file path absolute
- `pathJoin`: join list of file paths
- `pathNormalize`: normalize file path

* * *

Guides: Network Functions, System Functions

Categories: Files, System
