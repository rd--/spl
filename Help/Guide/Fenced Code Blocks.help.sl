# Fenced Code Blocks

In _Markdown_,
and _CommonMark_ in particular,
a _fenced code block_ is a section of the document written between matching _code fences_.

A code fence is a line that begins with three consecutive _fence characters_.

There are two kinds of fences,
grave accent fences _```_,
and tilde fences _~~~_.
The begin and fences must be of the same kind.

Opening code fences may contain _attributes_,
which are written directly following the fence characters.

* * *

See also: isCodeFence

Guides: Documentation Tests

References:
_CommonMark_
[1](https://spec.commonmark.org/0.31.2/#fenced-code-blocks)

Categories: Help
