# Class Methods

Since there are no classes, there are no class variables and no class methods.

As in Smalltalk types can define a `species` method to decide result types.

_new(f, n)_ is defined as _f(n)_.

* * *

See also: new, species

Categories: Reflection
