# Experimental Initialised Temporaries Syntax

Rewrite rules:

- _|( p = x, q = y ... )|_ ⟹ _var p, q ...; p := x; q := y; ..._

* * *

See also: Let Syntax, Var Syntax

Categories: Syntax
