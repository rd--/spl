# Notational Conventions

Type constructors are capitalised,
as are _pseudo-constructors_ such as `SinOsc`.

The standard library writes _F(x)_ where _F_ is a constructor, and _x.f_ otherwise.
