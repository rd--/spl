# String Functions

## Traits and Types

- `AsciiString`
- `BacktickQuotedString`
- `DoubleQuotedString`
- `Character`
- `String`

## Ascii

- `ascii`
- `asciiByteArray`
- `asciiString`

## Converting

- `asAsciiString`
- `asByteArray`
- `asCharacter`
- `asCodePoint`
- `asHexDigit`
- `asHexString`
- `asList`
- `asString`
- `characterList`
- `codePoints`
- `utf16List`
- `utf8ByteArray`
- `removeDiacritics`

## Decoding and Encoding

- `base64Decoded`
- `base64Encoded`

## Parsing

- `parseDecimal`
- `parseDecimalInteger`
- `parseFraction`
- `parseHexString`
- `parseJson`
- `parseLargeInteger`
- `parseNumber`
- `parseSmallInteger`

## Parse Colours

- `parseHexColour`
- `parseHexTriplet`
- `parseRgbColour`
- `parseRgbTriplet`

## Parse Durations and Times

- `parseDate`
- `parseDuration`

## Parse Documents

- `parseHtml`
- `parseSvg`

## Predicates

- `isAscii`
- `isString`
- `isUppercase`

## Printing

- `asString`
- `concisePrintString`
- `printString`
- `storeString`

## Sets

- `alphabet`
- `characterRange`

## Syntax

- `apostrophe`
- `graveAccent`
- `quotationMark`

* * *

Guides: Literals Syntax, String Syntax
