# One Indexing

Lists and other seqenceable collections are one-indexed, as in Fortran, Apl, Algol68, Pascal, Smalltalk, Icon, Excel, Mathematica, Lua, R, Matlab and Julia.

The first element is at index one, _c[1]_ reads the first element of the the collection _c_.

The index of the last element of a collection is the size of the collection, _c[c.size]_ reads the last element of the collection _c_.
