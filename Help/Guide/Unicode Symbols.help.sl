# Unicode Symbols

If unicode symbol replacement is enabled, the expression typed as:

```
valueWithReturn { :return:/1 | 2.pi.return } == 2.pi
```

will display as:

```
↓ { :↑ | 2.π.↑ } ≡ 2.π
```

* * *

See also: valueWithReturn

Guides: Emacs Editor
