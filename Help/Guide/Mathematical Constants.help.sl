# Mathematical Constants

- `arcMinute`: one sixtieth of a degree
- `arcSecond`: one sixtieth of an arc second
- `berahaConstant`: Beraha constants
- `degree`: one one-hundred and eightieth of pi
- `e`: Euler’s number, base of natural logarithm
- `epsilon`: epsilon
- `eulerGamma`: Euler’s constant
- `goldenAngle`: golden angle
- `goldenRatio`: golden ratio
- `i`: square root of negative one
- `pi`: ratio of circle circumference to diameter
- `plasticRatio`: limiting ratio of Padovan sequence
- `silverRatio`: silver ratio
- `smallFloatEpsilon`: the smallest interval above one
- `supergoldenRatio`: supergolden ratio
- `supersilverRatio`: supersilver ratio

Degree:

```
>>> 1.degree
2.pi / 360
```

E:

```
>>> 1.e
2.71828

>>> 2.e
5.43656
```

Euler Gamma:

```
>>> 1.eulerGamma
0.57722
```

Golden ratio:

```
>>> 1.goldenRatio
1.61803

>>> 2.goldenRatio
5.sqrt + 1
```

Pi:

```
>>> 1.pi
3.14159

>>> 2.pi
6.28319
```

* * *

See also: degree, e, eulerGamma, goldenRatio, pi

Guides: Mathematical Functions, Mathematical Sequences

References:
_Mathematica_
[1](https://reference.wolfram.com/language/guide/MathematicalConstants.html)

Categories: Math, Constant
