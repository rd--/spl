# Primitive Semantics

Primitives have _Smalltalk_ semantics and are written inline using a Smalltalk-like notation.

If a primitive succeeds it returns from the block,
if it fails the block continues with the remainder of the definition.

* * *

See also: Block

Guides: Primitive Syntax
