# Reference Help Files

The _Reference_ help files follow a few simple rules.

The main heading is the name of the entry and is written using the _#-prefix_ notation.

If the entry has any methods they are listed in the paragraph immediately following.

Examples are written in _Fenced Code Blocks_.

Where appropriate examples are written as `Documentation Tests`.

Below the horizontal rule there may be the following entries:

- _See also_: indicates a list of related help files to consult
- _References_: indicates external references (Apl, Haskell, J, Maple, Mathematica, SuperCollider)
- _Unicode_: glyphs related to the entry
- _Categories_: indicates which categories the entry should be listed in

* * *

See also: Documentation Tests, Help Files
