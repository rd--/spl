# Plotting Functions

- `arrayPlot`
- `densityPlot`
- `discretePlot`
- `functionPlot`
- `graphPlot`
- `linePlot`
- `matrixPlot`
- `parametricPlot`
- `polarPlot`
- `scatterPlot`
- `soundPlot`
- `stemLeafPlot`
- `stepPlot`
- `surfacePlot`
- `treePlot`
