# Network Functions

_Accessing_:

- `fileName`: file name of Url
- `fragment`: fragment, or hash, of Url
- `host`: host of Url
- `hostName`: hot name of Url
- `href`: string of entire Url
- `pathName`: path name of Url
- `port`: port of Url
- `protocol`: procotol, or scheme, of Url
- `query`: query, or search, of Url
- `queryParameters`: UrlQueryParameters
- `userName`: user name of Url

_Converting_:

- `asUrl`: construct a Url
- `asUrlQueryParameters`: converting

_Encoding & Decoding_:

- `decodeUri`: decode Uri
- `encodeUri`: encode Uri

_Fetching_:

- `fetch`: fetch resource from network
- `fetchText`: fetch text resource
- `fetchJson`: fetch Json resource
- `fetchByteArray`: fetch binary resource

_Types & Traits_:

- `Location`: location of an object
- `Request`: resource request
- `Response`: answer of fetch
- `Url`: uniform resource locator
- `UrlQueryParameters`: query parameters

* * *

Guides: File Functions, System Functions
