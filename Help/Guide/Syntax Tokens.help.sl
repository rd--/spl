# Syntax Tokens

## Separators

- `colonColon`: ::
- `colonEqualsSign`: :=
- `colonSemicolon`: :;
- `colon`: :
- `comma`: ,
- `equalsSign`: =
- `fullStop`: .
- `semicolon`: ;
- `verticalLine`: |

## Brackets

- `leftCurlyBracket`: {
- `rightCurlyBracket`: }
- `leftParenthesis`: (
- `rightParenthesis`: )
- `leftSquareBracket`: [
- `rightSquareBracket`: ]

## Naming

- `lowLine`: _

## Quotes

- `apostrophe`: single quote
- `quotationMark`: double quote
- `graveAccent`: backtick

## Comments

- `solidusAsterisk`: /\*
- `asteriskSolidus`: */

## Reserved Words

- `nil`
- `true`
- `false`
- `let`
- `var`

## Documentation

- `greaterThanSignGreaterThanSignGreaterThanSign`: >>>

* * *

See also: Syntax Guides

Categories: Syntax
