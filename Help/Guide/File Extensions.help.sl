# File Extensions

`Package` files have the extension _.sl_.
Packages are stored in the directory _Package_.

`Help` files,
both _Guide_ and _Reference_,
have the file extension _.help.sl_.
Help files follow a format described in `Help Files`.
Help files are stored in the directory _Help_.

`Program` files have the extension _.sp_.
Programs are stored in the directory _Program_.

* * *

See also: HelpFile, HelpIndex, Package

Guides: Help Files, Program Files
