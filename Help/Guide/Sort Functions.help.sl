# Sort Functions

## Sort

- `mergeInPlace`
- `mergeSortBy`
- `mergeSortFromToBy`
- `mergeSort`
- `patienceSort`
- `quickSortFromToBy`
- `quickSortBy`
- `quickSort`
- `sortBy`
- `sortOnBy`
- `sortOn`
- `sort`
- `sorted`
- `sortedWithIndices`

## Order

- `deepMax`
- `deepMin`
- `maxBy`
- `max`
- `minBy`
- `min`
- `ordering`

## Predicates

- `<=`: less than or equal to
- `<`: less than
- `>=`: greater than or equal to
- `>`: greater than
- `isMonotonicallyIncreasing`
- `isSortedBetweenAnd`
- `isSortedByBetweenAnd`
- `isSortedBy`
- `isSorted`
- `isStrictlyIncreasing`

## Querying

- `longestIncreasingSubsequence`
- `longestIncreasingSubsequenceList`
