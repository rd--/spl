# Mathematical Sequences

Sequences indicate the OEIS number where applicable.

Sequences:

- `bellNumber`: A000110
- `bernoulliSequence`:  A027641 & A027642
- `calkinWilfSequence`: A002487
- `catalanNumber`: A000108
- `collatzSequence`: A070156
- `fareySequence`: A006842 & A006843
- `fibonacciSequence`: A000045
- `harmonicNumber`: A001008 & A002805
- `inventorySequence`: A342585
- `lassalleNumber`: A180874
- `lobbNumber`: A039599
- `lucasNumbers`: A000032
- `narayanaSequence`: A000930
- `noergaardInfinitySequence`: A004718
- `padovanSequence`: A000931
- `pellNumbers`: A000129
- `pellLucasNumbers`: A002203
- `perrinSequence`: A001608
- `polygonalNumber`: A000217
- `recamanSequence`: A005132
- `sternBrocotSequence`: A002487
- `thueMorseSequence`: A010060
- `vanDerCorputNumber`: A030101 & A030102
- `vanDerLaanSequence`: A182097
- `wythoffLower`: A000201
- `wythoffUpper`: A001950

Trees:

- `keplerTree`: A294442
- `sternBrocotTree`: Stern-Brocot tree

Sequence generators:

- `deBruijnSequence`: de Bruijn sequence of given order on given alphabet
- `linearRecurrence`: linear recurrence of given length on given kernel
- `shiftRegisterSequence`: Linear-feedback shift register

Arrays:

- `wythoffArray`: Wythoff array

* * *

Guides: Integer Sequences, Magic Squares, Mathematical Constants, Mathematical Functions
