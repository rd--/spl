# Graph Functions

## Traits and Types

- `DelaunayTriangulation`
- `DirectedEdge`: directed edge
- `Graph`: graph
- `UndirectedEdge`: undirected edge

## Constructor Functions

- `adjacencyGraph`: adjacency graph
- `circulantGraph`: circulant graph
- `complementGraph`: complement
- `completeBipartiteGraph`: bipartite
- `completeGraph`: complete graph
- `cubeGraph`: cube graph
- `cycleGraph`: cycle graph
- `gridGraph`: grid graph
- `hararyGraph`: Harary graph
- `incidenceGraph`: incidence graph
- `lineGraph`: line graph
- `neighbourhoodGraph`: neighbourhood
- `pathGraph`: path graph
- `permutationGraph`: permutation graph
- `relationGraph`: relation graph
- `starGraph`: star graph
- `subgraph`: subgraph
- `sumGraph`: sum graph
- `wheelGraph`: wheel graph

## Operators

- `-->`: directed edge
- `---`: undirected edge

## Accessing (Query) Functions

- `adjacencyList`: adjacency list
- `adjacencyMatrix`: adjacency matrix
- `connectionMatrix`: connection matrix
- `degreeSequence`: degree sequence
- `dijkstrasAlgorithm`: Dijkstras algorithm
- `edgeCount`: edge count
- `edgeLabels`: edge labels
- `edgeList`: edge list
- `findShortestPath`: shortest path
- `graphDistance`: distance
- `graphDistanceMatrix`: distance matrix
- `incidenceList`: incidence list
- `incidenceMatrix`: incidence matrix
- `labeledVertexList`: labeled vertex list
- `neighbours`: neighbours
- `outEdgeListOf`
- `vertexCoordinates`: vertex coordinates
- `vertexCount`: vertex count
- `vertexDegree`: vertex degree
- `vertexInDegree`: vertex in degree
- `vertexInNeighbours`
- `vertexOutDegree`: vertex out degree
- `vertexOutNeighbours`
- `vertexLabels`: vertex labels
- `vertexList`: vertex list

## Predicate (Query) Functions

- `includesEdge`: includes edge
- `isDirected`: is directed
- `isDirectedEdge`: is directed edge
- `isEmpty`: is empty
- `isLoopFree`: is loop free
- `isMixed`: is mixed
- `isRegular`: is regular
- `isSingleton`: is singleton
- `isUndirected`: is undirected
- `isUndirectedEdge`: is undirected edge
- `isValid`: is valid
- `matchesEdge`: matches edge

## Converting Functions

- `asDot`: as dot
- `asEdge`: as edge
- `asEdgeList`: as edge list
- `asGraph`: as graph
- `undirectedGraph`: undirected graph

## Drawing Functions

- `asGeometryCollection`
- `asLineDrawing`
- `asPerspectiveDrawing`
- `completeGraphDrawing`
- `dotLayout`
- `graphPlot`
- `treePlot`

## Catalogs

- `sageSmallGraphCatalogue`

* * *

See also: Graph

Guides: Tree Functions
