# Method Dictionary

The method dictionary is a `Dictionary`.
The keys are method names and the values are _arity tables_.

An arity table is a `Dictionary`.
The keys are integers and the values are _type tables_.

A type table is a `Dictionary`.
The keys are type names and the values are `Method` values.

The method dictionary is reflected in the `system` value.

* * *

See also: Dictionary, methodDictionary, Method, System
