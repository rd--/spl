# AtPutDelegateTo Syntax

Rewrite rule:

- _c:.k := v_ ⟹ _atPutDelegateTo(c, k, v, 'parent')_

Syntax for the `atPutDelegateTo` protocol.

Where supported `:.` is displayed as ‣.

* * *

See also: AtPut Syntax, MessageSend Syntax

Unicode: ‣ U+2023 Triangular Bullet

Categories: Syntax
