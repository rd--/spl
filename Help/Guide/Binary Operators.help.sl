# Binary Operators

The one-character binary operators,
in alphabetical order:

- `ampersand`: &
- `asterisk`: *
- `circumflexAccent`: ^
- `commercialAt`: @
- `dollarSign`: $
- `equalsSign`: =
- `exclamationMark`: !
- `greaterThanSign`: >
- `hyphenMinus`: -
- `lessThanSign`: <
- `numberSign`: #
- `percentSign`: %
- `plusSign`: +
- `questionMark`: ?
- `reverseSolidus`: \
- `solidus`: /
- `tilde`: ~
- `verticalLine`: |

Two-character binary operators,
in alphabetical order:

- `ampersandAmpersand`: &&
- `asteriskPlusSign`: *+
- `commercialAtAsterisk`: @*
- `commercialAtGreaterThanSign`: @>
- `equalsSignEqualsSign`: ==
- `exclamationMarkCircumflexAccent`: !^
- `exclamationMarkPlusSign`: !+
- `exclamationMarkTilde`: !~
- `greaterThanSignEqualsSign`: >=
- `greaterThanSignGreaterThanSign`: >>
- `greaterThanSignTilde`: >~
- `hyphenMinusGreaterThanSign`: ->
- `hyphenMinusHyphenMinus`: --
- `lessThanSignEqualsSign`: <=
- `lessThanSignExclamationMark`: <!
- `lessThanSignHyphenMinus`: <-
- `lessThanSignLessThanSign`: <<
- `lessThanSignTilde`: <~
- `plusSignPlusSign`: ++
- `reverseSolidusReverseSolidus`: \\\\
- `solidusSolidus`: //
- `tildeEqualsSign`: ~=
- `tildeTilde`: ~~
- `verticalLineVerticalLine`: ||

Three-character binary operators,
in alphabetical order:

- `>>>`: greaterThanSign...
- `<=>`: lessThanSignEqualsSign...
- `+++`: plusSign...

* * *

See also: isOperatorToken, operatorCharacters, operatorTokenName, operatorNameTable

Guides: Binary Operator Syntax, Syntax Tokens
