# Numeric Types

The primitive numeric types are `SmallFloat`:

```
>>> 3.141.isSmallFloat
true
```

and `LargeInteger`:

```
>>> 23L.isLargeInteger
true
```

The non-primitive numeric types are `Complex`:

```
>>> 1J1.isComplex
true
```

and `Fraction`:

```
>>> 3/4.isFraction
true
```

* * *

See also: Binary, Complex, Fraction, Integer, isImmediate, LargeInteger, Number, SmallFloat

Guides: Number Literals, Numerical Precision
