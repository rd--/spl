# System Functions

_Environment Variables_:

- `environmentVariable`
- `environmentVariables`

_Local Host_:

- `currentWorkingDirectory`
- `hostName`
- `instructionSetArchitecture`
- `operatingSystem`

_Subprocess_:

- `systemCommand`: begin a subprocess

* * *

Guides: File Functions, Network Functions

Categories: System
