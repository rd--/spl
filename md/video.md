# Small Kansas

Small Kansas - 2023-03-20 ; Crystal Lattice Structure Browser

<iframe src="https://player.vimeo.com/video/809592857" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-12 ; Program Oracle

<iframe src="https://player.vimeo.com/video/807122251" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-11 ; Mark Not Transient, Synthesiser Status

<iframe src="https://player.vimeo.com/video/806969608" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-10 ; References To It

<iframe src="https://player.vimeo.com/video/806504803" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-10 ; Vector Graphics, Accept It

<iframe src="https://player.vimeo.com/video/806504624" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-10 ; Fonts, Tunings

<iframe src="https://player.vimeo.com/video/806504967" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-10 ; Colourful Windows, Font Menus

<iframe src="https://player.vimeo.com/video/806504336" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-03-10 ; Inspector, Ugen

<iframe src="https://player.vimeo.com/video/806505157" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-17 ; Context Menus, Windows at Hand

<iframe src="https://player.vimeo.com/video/799338692" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

SmallKansas - 2023-02-16 ; BrowseIt

<iframe src="https://player.vimeo.com/video/799338692" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-15 ; Help For, Text Editor

<iframe src="https://player.vimeo.com/video/798977439" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-15 ; Textures, Help Lookup

<iframe src="https://player.vimeo.com/video/798998346" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-15 ; Workspace, ImplementorsOf

<iframe src="https://player.vimeo.com/video/799060721" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-14 ; JMcC Sequencer, Help Browser

<iframe src="https://player.vimeo.com/video/798643896" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-14 ; F0, World Menu, Method Browser

<iframe src="https://player.vimeo.com/video/798618896" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>

Small Kansas - 2023-02-14 ; F0, Texture

<iframe src="https://player.vimeo.com/video/798264280" frameborder="0" allowfullscreen="true" width="640" height="360" ></iframe>
