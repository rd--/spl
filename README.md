# Spl

Simple Programming Language (Sᴘʟ):
a notation for working with a minimal procedural language.

The language consists of lexically scoped closures with mutable environments, product types, and a non-local return mechanism.

Online editors:
[Small Hours](http://smallhours.rohandrape.net/),
[Small Kansas](http://smallkansas.rohandrape.net/)

Videos:
[Small Kansas](?t=spl&e=md/video.md)


© [Rohan Drape](http://rohandrape.net/), 2022-2025, [Gpl](http://gnu.org/copyleft/)
