[
	/* Kernel Traits */
	'Object'
	'Cache'
	'Binary' /* > Cache */
	'Integer' /* > Cache */
	'Json'
	'Magnitude' /* > Object */
	'Number'
	'RandomNumberGenerator' /* > Object */

	/* Collection Traits */
	'PrimitiveSequence'
	'ArithmeticProgression'
	'Collection'
	'Extensible'
	'Indexable'
	'Iterable'
	'Ordered'
	'Removable'
	'Sequenceable'
	'Unordered'
	'Dictionary' /* Collection */

	/* Kernel Types */
	'Boolean'
	'Error'
	'Meta'
	'Nil'
	'Block'
	'Adverb' /* Block */
	'Promise'
	'Request'
	'SmallFloat'
	'Character'
	'String' /* Character */
	'System'

	/* Collection Types */
	'Association'
	'List'
	'Range'
	'Record'
	'Map' /* Record */

	'Package' /* System, Record */

	'ArrayBuffer'
	'ByteArray'
	'Float32Array'
	'Float64Array'
	'Blob' /* ByteArray, Float64Array */

	'Response'

].primitiveLoadPackageSequence
