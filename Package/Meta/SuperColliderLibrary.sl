[
	/* Music */
	'Meter'
	'Pitch'
	'Scale'
	'Tuning'
	'Xenharmonic'
	'ScalaTuning' /* Tuning */
	'CentsTuning' /* Tuning, ScalaTuning */
	'RatioTuning' /* Tuning, ScalaTuning */
	'TuningLattice' /* RatioTuning */

	/* Sound */
	'Wavetable'

	/* Protocol */
	'OpenSoundControl'

	/* SuperCollider */
	'Ugen'
	'ContinuousEvent'
	'Env'
	'J'
	'LocalControl'
	'Ls'
	'Mix' /* < Ugen */
	'KeywordUgens' /* < Ugen */
	'PseudoUgens' /* < Ugen */
	'ScLang'
	'ScProgram'
	'ScSynth' /* < Ugen */
	'ScSynthOptions'
	'ScSynthStatus'
	'ScUgen'
	'Texture' /* < Ugen */
	'UgenBindings' /* < Ugen */
	'UgenGraph'
].primitiveLoadPackageSequence
