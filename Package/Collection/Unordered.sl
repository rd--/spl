@Unordered {

	at { :self :index |
		self.errorNotIndexed
	}

	atPut { :self :index :value |
		self.errorNotIndexed
	}

}
