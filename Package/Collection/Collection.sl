@Collection {

	# { :self |
		self.size
	}

	+ { :self :anObject |
		self.collect(+.bindRight(anObject))
	}

	- { :self :anObject |
		self.collect(-.bindRight(anObject))
	}

	* { :self :anObject |
		self.collect(*.bindRight(anObject))
	}

	/ { :self :anObject |
		self.collect(/.bindRight(anObject))
	}

	^ { :self :anObject |
		self.collect(^.bindRight(anObject))
	}

	% { :self :anObject |
		self.collect(%.bindRight(anObject))
	}

	\ { :self :aCollection |
		self.difference(aCollection)
	}

	++ { :self :aCollection |
		let answer = self.copy;
		answer.addAll(aCollection.assertIsCollection);
		answer
	}

	adaptToNumberAndApply { :self :aNumber :aBlock:/2 |
		self.collect { :each |
			aBlock(aNumber, each)
		}
	}

	allEqualBy { :self :aBlock:/2 |
		self.isEmpty.if {
			true
		} {
			let item = self.anyOne;
			self.allSatisfy { :each |
				aBlock(each, item)
			}
		}
	}

	allEqual { :self |
		self.allEqualBy(=)
	}

	any { :self :numberOfElements |
		self.anyAs(numberOfElements, self.species)
	}

	anyAs { :self :numberOfElements :aBlock:/1 |
		let index = 0;
		let result = numberOfElements.aBlock;
		valueWithReturn { :return:/1 |
			result.fillFromWith(self) { :each |
				index := index + 1;
				(index > numberOfElements).if {
					result.return
				} {
					each
				}
			};
			(index = numberOfElements).ifFalse {
				self.error('@Collection>>any: Not enough elements in collection')
			};
			result
		}
	}

	arithmeticMean { :self |
		self.sum / self.size
	}

	asList { :self |
		let array = List(self.size);
		let index = 0;
		self.do { :each |
			index := index + 1;
			array[index] := each
		};
		array
	}

	asCollection { :self |
		self
	}

	assertIsCollection { :self |
		self
	}

	associationsDo { :self :aBlock:/1 |
		self.do(aBlock:/1)
	}

	atLevelCollect { :self :level :aBlock:/1 |
		let levelPredicate:/1 = level.isCollection.if {
			{ :each | level.includes(each) }
		} {
			{ :each | each = level }
		};
		self.withLevelCollect { :each :thisLevel |
			levelPredicate(thisLevel).if {
				aBlock(each)
			} {
				each
			}
		}
	}

	atRandom { :self :shape :r |
		{
			let randomIndex = r.randomInteger(1, self.size, []);
			let index = 1;
			valueWithReturn { :return:/1 |
				self.do { :each |
					(index = randomIndex).ifTrue {
						each.return
					};
					index := index + 1
				}
			}
		} ! shape
	}

	atRandom { :self :shape |
		self.atRandom(shape, system)
	}

	atRandom { :self |
		self.atRandom([], system)
	}

	average { :self |
		self.mean
	}

	binCounts { :self :b |
		self.binLists(b).collect(size:/1)
	}

	binCounts { :self :b1 :b2 |
		self.binLists(b1, b2).collect { :each |
			each.collect(size:/1)
		}
	}

	binListsFor { :self :b |
		let n = b.size;
		let c = { [] } ! (n - 1);
		self.do { :e |
			(e >= b[1] & { e <= b[n] }).ifTrue {
				let i = b.binarySearchLeftmost(e).min(n - 1);
				c[i].add(e)
			}
		};
		c
	}

	binListsFor { :self :b1 :b2 |
		let [m, n] = [b1.size, b2.size];
		let c = { [] } ! [m - 1, n - 1];
		self.do { :e |
			let [e1, e2] = e;
			(e1 >= b1[1] & { e1 <= b1[m] & { e2 >= b2[1] & { e2 <= b2[n] } } }).ifTrue {
				let i = b1.binarySearchLeftmost(e1).min(m - 1);
				let j = b2.binarySearchLeftmost(e2).min(n - 1);
				c[i][j].add(e)
			}
		};
		c
	}

	binLists { :self :b |
		let [start, stop, step] = b;
		self.binListsFor(
			Range(start, stop, step).asList
		)
	}

	binLists { :self :b1 :b2 |
		let [start1, stop1, step1] = b1;
		let [start2, stop2, step2] = b2;
		self.binListsFor(
			Range(start1, stop1, step1).asList,
			Range(start2, stop2, step2).asList
		)
	}

	capacity { :self |
		self.size
	}

	cartesianProductDo { :self :aCollection :aBlock:/2 |
		self.do { :x |
			aCollection.do { :y |
				aBlock(x, y)
			}
		}
	}

	cartesianProduct { :self :aCollection |
		let answer = [];
		self.cartesianProductDo(aCollection) { :i :j |
			answer.add([i, j])
		};
		answer
	}

	centralMoment { :self :r |
		let mean = self.mean;
		(1 / self.size) * ((self - mean) ^ r).sum
	}

	collect { :self :aBlock:/1 |
		let answer = self.species.new;
		self.do { :each |
			answer.add(aBlock(each))
		};
		answer
	}

	collectInto { :self :aBlock :aCollection |
		aCollection.fillFromWith(self, aBlock)
	}

	collectThenDo { :self :collectBlock:/1 :doBlock:/1 |
		self.collect(collectBlock:/1).do(doBlock:/1)
	}

	collectThenSelect { :self :collectBlock:/1 :selectBlock:/1 |
		let answer = self.species.new;
		self.do { :each |
			let item = collectBlock(each);
			selectBlock(item).ifTrue {
				answer.add(item)
			}
		};
		answer
	}

	concisePrintString { :self |
		'% (size: %)'.format([
			self.typeOf.withIndefiniteArticle,
			self.size
		])
	}

	contraharmonicMean { :self |
		self.squared.sum / self.sum
	}

	copyWith { :self :newElement |
		let answer = self.copy;
		answer.add(newElement);
		answer
	}

	copyWithout { :self :oldElement |
		self.reject { :each |
			each = oldElement
		}
	}

	copyWithoutAll { :self :aCollection |
		self.reject { :each |
			aCollection.includes(each)
		}
	}

	copyWithoutIdenticalElements { :self |
		let seen = IdentitySet();
		self.select { :each |
			seen.includes(each).if {
				false
			} {
				seen.basicInclude(each);
				true
			}
		}
	}

	clip { :self :min :max |
		self.collect { :each |
			each.clip(min, max)
		}
	}

	clip { :self |
		self.clip(-1, 1)
	}

	cubeRoot { :self |
		self.collect(cubeRoot:/1)
	}

	deepAllSatisfy { :self :aBlock:/1 |
		let type = self.typeOf;
		self.allSatisfy { :each |
			(each.typeOf = type).if {
				each.deepAllSatisfy(aBlock:/1)
			} {
				aBlock(each)
			}
		}
	}

	deepCollect { :self :aBlock:/1 |
		let type = self.typeOf;
		self.collect { :each |
			(each.typeOf = type).if {
				each.deepCollect(aBlock:/1)
			} {
				aBlock(each)
			}
		}
	}

	deleteDuplicates { :self :aBlock:/2 |
		self.nubBy(aBlock:/2)
	}

	deleteDuplicates { :self |
		self.nub
	}

	deleteMissing { :self |
		self.reject(isMissing:/1)
	}

	depth { :self |
		self.isEmpty.if {
			2
		} {
			1 + self.collect(depth:/1).max
		}
	}

	difference { :self :aCollection |
		self.reject { :each |
			aCollection.includes(each)
		}
	}

	differenceAll { :self :aCollection |
		self.reject { :each |
			aCollection.anySatisfy { :subCollection |
				subCollection.includes(each)
			}
		}
	}

	discreteDelta { :self |
		self.allSatisfy(isZero:/1).if {
			1
		} {
			0
		}
	}

	doesNotInclude { :self :anObject |
		self.includes(anObject).not
	}

	elementType { :self |
		self.elementTypeIfAbsent {
			nil
		}
	}

	elementTypeIfAbsent { :self :aBlock:/0 |
		let types = self.elementTypes;
		(types.size = 1).if {
			types.anyOne
		} {
			aBlock()
		}
	}

	elementTypes { :self |
		let answer = IdentitySet();
		self.do { :each |
			answer.include(each.typeOf)
		};
		answer
	}

	emptyCheck { :self |
		self.isEmpty.ifTrue {
			self.errorEmptyCollection
		}
	}

	errorEmptyCollection { :self |
		self.error('errorEmptyCollection')
	}

	errorNotFound { :self :anObject |
		self.error('errorNotFound: ' ++ anObject)
	}

	errorNotIndexed { :self |
		self.error('is not indexed/keyed')
	}

	extendedGcd { :self |
		(self.size < 2).if {
			self.error('@Collection>>extendedGcd')
		} {
			let [g, c] = self[1].extendedGcd(self[2]);
			3.toDo(self.size) { :each |
				let [nextG, nextC] = g.extendedGcd(self[each]);
				g := nextG;
				c.add(nextC.last)
			};
			[g, c]
		}
	}

	fillFrom { :self :aCollection |
		self.fillFromWith(aCollection, identity:/1)
	}

	gather { :self :aBlock:/1 |
		let answer = self.species.new;
		self.do { :each |
			answer.addAll(aBlock(each))
		};
		answer
	}

	gcd { :self |
		self.reduce(gcd:/2)
	}

	geometricMean { :self |
		self.product ^ (1 / self.size)
	}

	groupBy { :self :keyBlock:/1 |
		let result = Map();
		self.do { :each |
			let key = keyBlock(each);
			result.atIfAbsentPut(key) {
				[]
			}.add(each)
		};
		result
	}

	harmonicMean { :self |
		self.size / self.reciprocal.sum
	}

	histogramOf { :self |
		self.histogramOf { :each |
			each
		}
	}

	histogramListFor { :self :b |
		[b, self.binListsFor(b).collect(size:/1)]
	}

	histogramList { :self :b |
		let [start, stop, step] = b;
		self.histogramListFor(
			Range(start, stop, step).asList
		)
	}

	histogramList { :self |
		let k = self.size.sqrt.ceiling + 1;
		let [min, max] = self.minMax;
		let b = (min -- max).findDivisions(k);
		self.histogramListFor(b.asList)
	}

	include { :self :anObject |
		self.typeResponsibility('@Collection>>include')
	}

	includeAll { :self :aCollection |
		aCollection.do { :each |
			self.include(each)
		}
	}

	indices { :self |
		nil
	}

	intersection { :self :aCollection |
		self.select { :each |
			aCollection.includes(each)
		}
	}

	ifEmpty { :self :aBlock:/0 |
		self.isEmpty.if {
			aBlock()
		} {
			self
		}
	}

	ifEmpty { :self :emptyBlock:/0 :notEmptyBlock |
		self.isEmpty.if {
			emptyBlock()
		} {
			notEmptyBlock.cull(self)
		}
	}

	ifEmptyIfNotEmptyDo { :self :emptyBlock:/0 :notEmptyBlock:/1 |
		self.isEmpty.if {
			emptyBlock()
		} {
			notEmptyBlock(self)
		}
	}

	ifNotEmpty { :self :aBlock |
		self.isEmpty.ifFalse {
			aBlock.cull(self)
		}
	}

	ifNotEmptyDo { :self :aBlock:/1 |
		self.isEmpty.ifFalse {
			aBlock(self)
		}
	}

	interquartileRange { :self :a :b :c :d |
		let [q1, q2, q3] = self.quartiles(a, b, c, d);
		q3 - q1
	}

	interquartileRange { :self |
		let [q1, q2, q3] = self.quartiles;
		q3 - q1
	}

	isAtom { :self |
		false
	}

	isCollection { :self |
		true
	}

	isDisjoint { :self :aCollection |
		self.noneSatisfy { :each |
			aCollection.includes(each)
		}
	}

	isEmpty { :self |
		self.size = 0
	}

	isOfSameSizeCheck { :self :otherCollection |
		(otherCollection.size = self.size).ifFalse {
			self.error('@Collection>>isOfSameSizeCheck')
		}
	}

	isZero { :self |
		self.allSatisfy(isZero:/1)
	}

	kroneckerDelta { :self |
		self.allEqual.boole
	}

	kurtosis { :self |
		self.isVector.if {
			self.centralMoment(4) / (self.centralMoment(2) ^ 2)
		} {
			self.isMatrix.if {
				self.transposed.collect(kurtosis:/1)
			} {
				'@Collection>>kurtosis: not vector or matrix'.error
			}
		}
	}

	leafCount { :self |
		self.collect { :each |
			each.isCollection.if {
				each.leafCount
			} {
				1
			}
		}.sum
	}

	maxIfEmpty { :self :aBlock:/0 |
		self.ifEmpty {
			aBlock()
		} {
			self.max
		}
	}

	minIfEmpty { :self :aBlock:/0 |
		self.ifEmpty {
			aBlock()
		} {
			self.min
		}
	}

	mean { :self |
		self.sum / self.size
	}

	meanDeviation { :self |
		(self - self.mean).abs.sum / self.size
	}

	moment { :self :r |
		(1 / self.size) * (self ^ r).sum
	}

	nearest { :self :anObject :aBlock:/2 |
		let leastDistance = self.collect { :each |
			aBlock(each, anObject).abs
		}.min;
		self.select { :each |
			aBlock(each, anObject).abs = leastDistance
		}
	}

	normalizeRange { :self :minima :maxima |
		self.rescale(self.min, self.max, minima, maxima)
	}

	normalizeSignal { :self :minima :maxima |
		let x = self.min.abs.max(self.max.abs);
		self.rescale(x.negated, x, minima, maxima)
	}

	normalizeSignal { :self |
		self.normalizeSignal(-1, 1)
	}

	normalizeSum { :self |
		self / self.sum
	}

	not { :self |
		self.collect(not:/1)
	}

	notEmpty { :self |
		self.isEmpty.not
	}

	nub { :self |
		self.nubBy(=)
	}

	nubBy { :self :aBlock:/2 |
		(aBlock:/2 == equalsSignEqualsSign:/2).if {
			self.nubIdentical
		} {
			let seen = [];
			self.select { :each |
				seen.includesBy(each, aBlock:/2).if {
					false
				} {
					seen.add(each);
					true
				}
			}
		}
	}

	nubIdentical { :self |
		let seen = IdentitySet();
		self.select { :each |
			seen.includes(each).if {
				false
			} {
				seen.include(each);
				true
			}
		}
	}

	ofSize { :self :aNumber |
		(self.size = aNumber).ifFalse {
			self.error('@Collection>>ofSize')
		};
		self
	}

	one { :self |
		self.collect { :each |
			each.one
		}
	}

	powerSetDo { :self :aBlock:/1 |
		let size = 2 ^ self.size;
		let powersOfTwo = 2 ^ (0 .. (self.size - 1));
		let list = self.asList;
		0.toDo(size - 1) { :i |
			let subset = self.species.new;
			powersOfTwo.withIndexDo { :each :j |
				(i // each % 2 ~= 0).ifTrue {
					subset.include(list[j])
				}
			};
			aBlock(subset)
		};
		self
	}

	powerSet { :self |
		let answer = [];
		self.powerSetDo { :each |
			answer.add(each)
		};
		answer
	}

	pseudoSlotNameList { :self |
		['size']
	}

	quantile { :self :p :a :b :c :d |
		self.isVector.if {
			self.asSortedList.quantile(p, a, b, c, d)
		} {
			self.isMatrix.if {
				self.transposed.collect { :each |
					each.asSortedList.quantile(p, a, b, c, d)
				}
			} {
				'Collection>>quantile: not vector or matrix'
			}
		}
	}

	quantile { :self :p |
		self.quantile(p, 0, 0, 1, 0)
	}

	quartiles { :self :a :b :c :d |
		self.quantile([1 2 3] / 4, a, b, c, d)
	}

	quartiles { :self |
		self.quartiles(1 / 2, 0, 0, 1)
	}

	rankedMax { :self :n |
		(n < 0).if {
			self.rankedMin(n.negated)
		} {
			let m = self.size;
			self.quantile((m - n + 1) / m)
		}
	}

	rankedMin { :self :n |
		(n < 0).if {
			self.rankedMax(n.negated)
		} {
			self.quantile(n / self.size)
		}
	}

	reject { :self :aBlock:/1 |
		self.select { :element |
			aBlock(element).not
		}
	}

	rescale { :self :min :max :ymin :ymax |
		self.collect { :each |
			each.rescale(min, max, ymin, ymax)
		}
	}

	rescale { :self :min :max |
		self.rescale(min, max, 0, 1)
	}

	rescale { :self |
		self.rescale(self.deepMin, self.deepMax, 0, 1)
	}

	rootMeanSquare { :self |
		self.squared.sum.sqrt / 2
	}

	sampleStandardDeviation { :self |
		(self - self.mean).squared.mean.sqrt
	}

	select { :self :aBlock:/1 |
		let answer = self.species.new;
		self.do { :each |
			aBlock(each).ifTrue {
				answer.include(each)
			}
		};
		answer
	}

	selectThenCollect { :self :selectBlock:/1 :collectBlock:/1 |
		let answer = self.species.new;
		self.selectThenDo(selectBlock:/1) { :each |
			answer.add(collectBlock(each))
		};
		answer
	}

	skewness { :self |
		self.isVector.if {
			self.centralMoment(3) / (self.centralMoment(2) ^ (3 / 2))
		} {
			self.isMatrix.if {
				self.transposed.collect(skewness:/1)
			} {
				'@Collection>>skewness: not vector or matrix'.error
			}
		}
	}

	sorted { :self |
		self.asList.sort
	}

	sorted { :self :sortBlock:/2 |
		self.asList.sortBy(sortBlock:/2)
	}

	sortedCounts { :self |
		self.asIdentityBag.sortedCounts
	}

	sortedElements { :self |
		self.asIdentityBag.sortedElements
	}

	stemLeafPlot { :self |
		let negative = Map();
		let positive = Map();
		self.collect { :each |
			let d = each.integerDigits;
			let rhs = d.last;
			let lhsList = (d.size > 1).if { d.allButLast } { [0] };
			let lhs = each.copySignTo(lhsList.fromDigits(10));
			let map = each.isNegative.if { negative } { positive };
			map.atIfPresentIfAbsent(lhs) { :entry |
				entry.add(rhs)
			} {
				map.atPut(lhs, [rhs])
			}
		};
		negative.associations.sort ++ positive.associations.sort
	}

	subsets { :self :aBlock:/1 |
		let answer = [];
		self.powerSetDo { :each |
			aBlock(each).ifTrue {
				answer.add(each)
			}
		};
		answer
	}

	sum { :self |
		self.isEmpty.if {
			0
		} {
			self.reduce(+)
		}
	}

	standardDeviation { :self |
		self.variance.sqrt
	}

	standardizedMoment { :self :r |
		self.centralMoment(r) / (self.sampleStandardDeviation ^ r)
	}

	symmetricDifference { :self :aCollection |
		self.difference(aCollection).union(
			aCollection.difference(self)
		)
	}

	take { :self :maxNumberOfElements |
		self.any(maxNumberOfElements.min(self.size))
	}

	tally { :self :aBlock:/2 |
		let answer = [];
		self.do { :each |
			answer.detectIndexIfFoundIfNone { :item |
				aBlock(item.key,each)
			} { :index |
				let association = answer[index];
				association.value := association.value + 1
			} {
				answer.add(each -> 1)
			}
		};
		answer
	}

	tally { :self |
		self.tally(=)
	}

	threshold { :self :epsilon |
		self.deepCollect { :each |
			(each.abs < epsilon).if {
				each.zero
			} {
				each
			}
		}
	}

	unionBy { :self :aCollection :aBlock:/2 |
		let answer = self.asSet(aBlock:/2);
		answer.includeAll(aCollection);
		answer
	}

	union { :self :aCollection |
		self.unionBy(aCollection, =)
	}

	variance { :self |
		((self - self.mean) ^ 2).sum / (self.size - 1)
	}

	withLevelCollect { :self :aBlock:/2 :level |
		let type = self.typeOf;
		self.collect { :each |
			(each.typeOf = type).if {
				aBlock(each.withLevelCollect(aBlock:/2, level + 1), level)
			} {
				aBlock(each, level)
			}
		}
	}

	withLevelCollect { :self :aBlock:/2 |
		aBlock(self.withLevelCollect(aBlock:/2, 1), 0)
	}

	zero { :self |
		self.collect { :each |
			each.zero
		}
	}

}

+@Object {

	asCollection { :self |
		[self]
	}

	depth { :self |
		1
	}

	isAtom { :self |
		true
	}

	isCollection { :self |
		false
	}

}
