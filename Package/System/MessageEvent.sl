MessageEvent! : [Object, Event] {

	data { :self |
		<primitive: return _self.data;>
	}

	origin { :self |
		<primitive: return _self.origin;>
	}

	lastEventId { :self |
		<primitive: return _self.lastEventId;>
	}

	source { :self |
		<primitive: return _self.source;>
	}

	ports { :self |
		<primitive: return _self.ports;>
	}

}
