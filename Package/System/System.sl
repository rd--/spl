System! : [Object, Cache, RandomNumberGenerator] {

	= { :self :anObject |
		self == anObject
	}

	basicNextRandomFloat { :self |
		<primitive: return Math.random();>
	}

	byteOrdering { :self |
		self.isLittleEndian.if {
			-1
		} {
			1
		}
	}

	cache { :self |
		<primitive: return _self.cache;>
	}

	caches { :self |
		<primitive: return _self.window.caches;>
	}

	consoleClear { :unused |
		<primitive:
		console.clear;
		return null;
		>
	}

	consoleError { :unused :message |
		<primitive:
		console.error(_message);
		return null;
		>
	}

	consoleNotification { :unused :message |
		<primitive:
		console.log(_message);
		return null;
		>
	}

	consoleWarning { :unused :message |
		<primitive:
		console.warn(_message);
		return null;
		>
	}

	evaluateOrSignalError { :unused :aString |
		<primitive: return sl.evaluateForSignalling('*Interactive*', _aString);>
	}

	evaluateNotifying { :self :aString :aBlock:/1 |
		{
			self.evaluateOrSignalError(aString)
		}.ifError { :err |
			aBlock(err)
		}
	}

	evaluate { :self :aString |
		self.evaluateNotifying(aString) { :err |
			self.consoleError(err)
		}
	}

	homeDirectory { :self |
		self.environmentVariable('HOME').ifNil {
			self.error('homeDirectory: not set')
		}
	}

	isBigEndian { :unused |
		1.unsigned32BitWordList[1] = 16r3FF00000
	}

	isLittleEndian { :self |
		self.isBigEndian.not
	}

	loadFile { :unused :fileName |
		<primitive: evaluateFile(_fileName);>
	}

	loadUrl { :unused :url |
		<primitive: evaluateUrl(_url);>
	}

	localStorage { :self |
		<primitive: return _self.window.localStorage;>
	}

	localTimeZoneOffsetInMinutes { :unused |
		<primitive:
		const aDate = new Date(0);
		return aDate.getTimezoneOffset();
		>
	}

	methodDictionary { :self |
		<primitive: return _self.methodDictionary;>
	}

	millisecondsToRun { :self :aBlock:/0 |
		let beginTime = self.systemTimeInMilliseconds;
		aBlock();
		self.systemTimeInMilliseconds - beginTime
	}

	operatorCharacters { :self |
		self.cached('operatorCharacters') {
			'&*^@$=!>-<#%+?\\/~|'.contents
		}
	}

	operatorNameTable { :self |
		let table = self.punctuationCharacterNameTable;
		self.cached('operatorNameTable') {
			[
				'& * ^ @ $ = ! > - < # % + ? \\ / ~ |'
				'&& @* @> == !^ !> !+ !~ >= >> >~ -> - <= <! <- << <~ ++ \\\\ // ~= ~~ ||'
				'>>> <=> +++'
			].collect(words:/1).++.collect { :each |
				each -> each.operatorTokenName(table)
			}.asRecord
		}
	}

	packageDictionary { :self |
		<primitive: return _self.packageDictionary;>
	}

	postLine { :self :aString |
		self.consoleNotification(aString)
	}

	preference { :self :path :defaultValue |
		self.preferencesReadPath(path.splitBy('/'), defaultValue)
	}

	preferences { :self |
		self.cached('preferences') {
			()
		}
	}

	preferencesRead { :self :key |
		<primitive: return sl.preferencesRead(_preferences_1(_self), _key);>
	}

	preferencesReadPath { :self :path :defaultValue |
		path.isEmpty.if {
			self.error('System>>preferencesReadPath: empty path')
		} {
			let item = self.preferencesRead(path[1]);
			let index = 2;
			{
				item.notNil & {
					index <= path.size
				}
			}.whileTrue {
				item := item.atIfAbsent(path[index]) { nil };
				index := index + 1
			};
			item ? {
				defaultValue
			}
		}
	}

	pseudoSlotNameList { :unused |
		[
			'cache',
			'methodDictionary',
			'traitDictionary',
			'typeDictionary',
			'packageDictionary',
			'window',
			'library', /* Package */
			'transcript' /* Package */
		]
	}

	punctuationCharacterNameTable { :unused |
		<primitive: return sl.punctuationCharacterNameTable;>
	}

	punctuationCharacters { :unused |
		<primitive: return sl.punctuationCharacters.split('');>
	}

	nextRandomFloat { :self |
		self.randomNumberGenerator.next
	}

	randomByteArray { :unused :anInteger |
		<primitive:
		let bytes = new Uint8Array(_anInteger);
		crypto.getRandomValues(bytes);
		return bytes;
		>
	}

	randomNumberGenerator { :self |
		self.cached('randomNumberGenerator') {
			Sfc32(self.unixTimeInMilliseconds)
		}
	}

	seedRandom { :self :anInteger |
		self.randomNumberGenerator.initialize(anInteger)
	}

	sessionStorage { :self |
		self.window.sessionStorage
	}

	smallFloatEpsilon { :unused |
		<primitive: return Number.EPSILON;>
	}

	splDirectory { :self |
		self.environmentVariable('SPL_DIR').ifNil {
			self.error('splDirectory: not set')
		}
	}

	splFileName { :self :aString |
		'%/%'.format([
			self.splDirectory,
			aString
		])
	}

	splUrl { :self :aString |
		('https://rohandrape.net/sw/spl/' ++ aString).asUrl
	}

	traitDictionary { :self |
		<primitive: return _self.traitDictionary;>
	}

	typeDictionary { :self |
		<primitive: return _self.typeDictionary;>
	}

	systemTimeInMilliseconds { :unused |
		<primitive: return performance.now();>
	}

	systemTimeInSeconds { :unused |
		<primitive: return performance.now() * 0.001;>
	}

	uniqueId { :self |
		let answer = self.cached('uniqueId') {
			1
		};
		self.cache['uniqueId'] := answer + 1;
		answer
	}

	uniqueId { :self :anInteger |
		self.cache['uniqueId'] := anInteger
	}

	unixTimeInMilliseconds { :unused |
		<primitive: return Date.now();>
	}

	window { :self |
		<primitive: return _self.window;>
	}

	workspace { :self |
		self.cached('workspace') {
			Record()
		}
	}

}

+Block {

	once { :self |
		let cache = system.cached('onceCache') {
			WeakMap()
		};
		cache.atIfAbsentPut(self) {
			self.value
		}
	}

	once { :self :where :key |
		where.cached(key, self)
	}

}

+Block {

	benchForMilliseconds { :self:/0 :interval |
		let t0 = system.systemTimeInMilliseconds;
		let t1 = nil;
		let t2 = t0 + interval;
		let count = 1;
		self();
		{
			t1 := system.systemTimeInMilliseconds;
			t1 < t2
		}.whileTrue {
			self();
			count := count + 1
		};
		[count, t1 - t0]
	}


	benchFor { :self :aDuration |
		let [count, elapsedTime] = self.benchForMilliseconds(aDuration.milliseconds);
		[
			(count / (elapsedTime / 1000)).roundTo(0.001), ' per second; ',
			((elapsedTime / 1000) / count).roundTo(0.001), ' per count'
		].stringJoin
	}

	millisecondsToRun { :self:/0 |
		let startTime = system.systemTimeInMilliseconds;
		self();
		system.systemTimeInMilliseconds - startTime
	}

}

+[SmallFloat, String] {

	seedRandom { :self |
		system.seedRandom(self)
	}

}

+String {

	isOperatorCharacter { :self |
		<primitive: return sl.isOperatorCharacter(_self);>
	}

	isOperatorToken { :self |
		<primitive: return sl.isOperatorToken(_self);>
	}

	isPunctuationCharacter { :self |
		<primitive: return sl.isPunctuationCharacter(_self);>
	}

	isPunctuationToken { :self |
		<primitive: return sl.isPunctuationToken(_self);>
	}

	isSyntaxCharacter { :self |
		<primitive: return sl.isSyntaxCharacter(_self);>
	}

	isSyntaxToken { :self |
		<primitive: return sl.isSyntaxToken(_self);>
	}

	operatorNameToken { :self |
		valueWithReturn { :return:/1 |
			system.operatorNameTable.associationsDo { :each |
				(each.value = self).ifTrue {
					each.key.return
				}
			};
			nil
		}
	}

	operatorTokenName { :self :table |
		self.isOperatorToken.if {
			self.punctuationTokenName(table)
		} {
			self.error('operatorTokenName: not operator token')
		}
	}

	operatorTokenName { :self |
		self.operatorTokenName(system.punctuationCharacterNameTable)
	}

	punctuationTokenName { :self :table |
		self.contents.collect { :letter |
			table[letter]
		}.camelCase.stringCatenate
	}

	punctuationTokenName { :self |
		self.punctuationTokenName(system.punctuationCharacterNameTable)
	}

	splParseExpression { :self |
		let tree = self.splParseTree;
		let f = { :e |
			let o = e.first;
			let p = e.allButFirst;
			o.caseOfOtherwise(
				[
					{ 'Apply' } -> {
						SymbolicExpression(f(p[1]), p.allButFirst.collect(f:/1))
					},
					{ 'Arguments' } -> {
						SymbolicExpression('𝓐', p.collect(f:/1))
					},
					{ 'Assignment' } -> {
						SymbolicExpression('←', p.collect(f:/1))
					},
					{ 'Block' } -> {
						SymbolicExpression('𝜆', p.collect(f:/1))
					},
					{ 'Identifier' } -> {
						Symbol(p[1])
					},
					{ 'LargeInteger' } -> {
						p[1].allButLast.parseLargeInteger
					},
					{ 'Let' } -> {
						SymbolicExpression('≔', p.collect(f:/1))
					},
					{ 'List' } -> {
						SymbolicExpression('𝓛', p.collect(f:/1))
					},
					{ 'Operator' } -> {
						Symbol(p[1])
					},
					{ 'Program' } -> {
						SymbolicExpression('𝒫', p.collect(f:/1))
					},
					{ 'ReservedIdentifier' } -> {
						p[1].caseOf(
							[
								{ 'false' } -> { false },
								{ 'nil' } -> { nil },
								{ 'true' } -> { true }
							]
						)
					},
					{ 'SmallFloat' } -> {
						p[1].parseNumber
					},
					{ 'SmallInteger' } -> {
						p[1].parseSmallInteger(10)
					}
				]
			) {
				self.error('String>>splParseExpression: ' ++ o)
			}
		};
		f(tree)
	}

	splParseTree { :self |
		<primitive: return sl.rewriteSlToAst(_self);>
	}

	splSimplify { :self |
		<primitive: return sl.rewriteSlToCore(_self);>
	}

}
