+SmallKansas {

	ScalaScaleBrowser { :self :scalaModenam |
		let sizes = scalaModenam.collect(size:/1).copyWithoutIdenticalElements.sort.collect(asString:/1);
		let selectedSize = nil;
		let selectedTuningSize = nil;
		self.ColumnBrowser('Scala Scale Browser', 'text/plain', false, true, [1, 1, 4], nil, nil) { :browser :path |
			path.size.caseOf([
				{ 0 } -> {
					browser.setStatus('Size/TuningSize/Name');
					sizes
				},
				{ 1 } -> {
					browser.setStatus('Size = ' ++ path[1]);
					selectedSize := path[1].parseSmallInteger(10);
					scalaModenam.select { :each |
						each.size = selectedSize
					}.collect { :each |
						each.tuningSize
					}.copyWithoutIdenticalElements.sort.collect(asString:/1)
				},
				{ 2 } -> {
					browser.setStatus(
						[
							'Size = ', path[1], ', ',
							'TuningSize = ', path[2]
						].stringJoin
					);
					selectedTuningSize := path[2].parseSmallInteger(10);
					scalaModenam.select { :each |
						each.size = selectedSize & {
							each.tuningSize = selectedTuningSize
						}
					}.collect(description:/1)
				},
				{ 3 } -> {
					let modenam = scalaModenam.detect { :each |
						each.description = path[3]
					};
					browser.setStatus(path[3]);
					[
						modenam.printString,
						modenam.tuningIndices.printString
					].unlines
				}
			])
		}
	}

}

ScalaScaleBrowser : [Object, SmallKansan] {

	openIn { :self :smallKansas :event |
		system.requestLibraryItem(
			'ScalaScaleArchive'
		).then { :answer |
			smallKansas.addFrame(
				smallKansas.ScalaScaleBrowser(answer),
				event
			)
		}
	}

}
