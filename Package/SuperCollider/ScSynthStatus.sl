+Record {
	ugenCount { :self | self['ugenCount'] }
	synthCount { :self | self['synthCount'] }
	groupCount { :self | self['groupCount'] }
	synthdefCount { :self | self['synthdefCount'] }
	cpuAverage { :self | self['cpuAverage'] }
	cpuPeak { :self | self['cpuPeak'] }
	sampleRateNominal { :self | self['sampleRateNominal'] }
	sampleRateActual { :self | self['sampleRateActual'] }
}
