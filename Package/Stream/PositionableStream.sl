@PositionableStream {

	atEnd { :self |
		self.typeResponsibility('@PositionableStream>>atEnd')
	}

	back { :self |
		(self.position = 0).ifTrue {
			self.error('@PositionableStream>>back: cannot go back')
		};
		self.skip(-1);
		self.peek
	}

	isEmpty { :self |
		self.atEnd & {
			self.position = 0
		}
	}

	last { :self |
		self.collection.at(self.position)
	}

	nextMatchAll { :self :aCollection |
		valueWithReturn { :return:/1 |
			let savedPosition = self.position;
			aCollection.do { :each |
				(self.next = each).ifFalse {
					self.position := savedPosition;
					false.return
				}
			};
			true
		}
	}

	on { :self :aCollection |
		self.collection := aCollection;
		self.position := 0;
		self.isReadStream.if {
			self.readLimit := aCollection.size
		};
		self.reset
	}

	originalContents { :self |
		self.collection
	}

	peek { :self |
		self.atEnd.if {
			nil
		} {
			let nextObject = self.next;
			self.position := self.position - 1;
			nextObject
		}
	}

	peekFor { :self :anObject |
		valueWithReturn { :return:/1 |
			self.atEnd.ifTrue {
				false.return
			};
			(self.next = anObject).ifTrue {
				true.return
			};
			self.position := self.position - 1;
			false
		}
	}

	position { :self |
		self.positionIndex
	}

	position { :self :anInteger |
		anInteger.isNegative.if {
			self.positionError
		} {
			self.positionIndex := anInteger
		}
	}

	positionError { :self |
		self.error('@PositionableStream>>positionError: position out of bounds')
	}

	reset { :self |
		self.position := 0
	}

	skip { :self :anInteger |
		self.position := self.position + anInteger
	}

	skipTo { :self :anObject |
		valueWithReturn { :return:/1 |
			{
				self.atEnd
			}.whileFalse {
				(self.next = anObject).ifTrue {
					true.return
				}
			};
			false
		}
	}

	upTo { :self :anObject |
		self.withWriteStream { :aStream |
			let element = nil;
			{
				self.atEnd | {
					element := self.next;
					element = anObject
				}
			}.whileFalse {
				aStream.nextPut(element)
			}
		}
	}

	upToEnd { :self |
		self.withWriteStream { :aStream |
			{
				self.atEnd
			}.whileFalse {
				aStream.nextPut(self.next)
			}
		}
	}

	upToPosition { :self :anInteger |
		self.next(anInteger - self.position)
	}

	withWriteStream { :self :aBlock:/1 |
		let aStream = self.collection.species.new(100).asWriteStream;
		aBlock(aStream);
		aStream.contents
	}

}
