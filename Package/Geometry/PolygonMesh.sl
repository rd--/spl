@PolygonMesh {

	boundingBox { :self |
		self.vertexCoordinates.coordinateBoundingBox
	}

	dimension { :self |
		self.embeddingDimension
	}

	edgeCount { :self |
		self.faceIndices.collect(size:/1).sum / 2
	}

	edgeLengths { :self |
		self.edgeCoordinates.collect { :each |
			each.Line.arcLength
		}
	}

	edgeList { :self |
		let answer = [];
		self.faceIndices.do { :each |
			1.toDo(each.size) { :i |
				answer.add(
					[
						each.at(i),
						each.atWrap(i + 1)
					].sort
				)
			}
		};
		answer.nub
	}

	edgeCoordinates { :self |
		let vertexCoordinates = self.vertexCoordinates;
		self.edgeList.collect { :each |
			vertexCoordinates.atAll(each)
		}
	}

	embeddingDimension { :self |
		self.vertexCoordinates.anyOne.size
	}

	faceCount { :self |
		self.faceIndices.size
	}

	faceDegreeCounts { :self |
		self.faceIndices.collect(size:/1).sortedElements
	}

	graph { :self |
		let answer = Graph(self.vertexList, self.edgeList);
		answer.vertexCoordinates := self.vertexCoordinates;
		answer
	}

	vertexCount { :self |
		self.vertexCoordinates.size
	}

	vertexList { :self |
		[1 .. self.vertexCount]
	}

}

PolygonMesh : [Object, PolygonMesh] { | vertexCoordinates faceIndices |

	canonicalForm { :self |
		let v = self.vertexCoordinates;
		let w = v.nub.sort;
		PolygonMesh(
			w,
			self.faceIndices.collect { :each |
				each.collect { :i |
					w.indexOf(v[i])
				}.lexicographicallyLeastRotation.deleteAdjacentDuplicates
			}.reject { :each |
				each.size <= 2
			}.nub.sort
		)
	}

	forSvg { :self :options |
		let vertexCoordinates = self.vertexCoordinates;
		self.faceIndices.collect { :each |
			'<polygon points="%" />'.format([
				self.vertexCoordinates.atAll(each).asSvgPointList(options)
			])
		}.unlines
	}

}

+List {

	PolygonMesh { :vertexCoordinates :faceIndices |
		newPolygonMesh().initializeSlots(vertexCoordinates, faceIndices)
	}

}

+System {

	planarConvexPolytopeGraphCatalogue { :self |
		self.requireLibraryItem(
			'PlanarConvexPolytopeGraphCatalogue'
		)
	}

	planarGraphCatalogue { :self |
		self.cached('planarGraphCatalogue') {
			[
				self.planarConvexPolytopeGraphCatalogue,
				self.planarNonHamiltonianGraphCatalogue,
				self.planarQuadrangulationsCatalogue,
				self.planarRegularGraphCatalogue
			].dictionaryJoin
		}
	}

	planarNonHamiltonianGraphCatalogue { :self |
		self.requireLibraryItem(
			'PlanarNonHamiltonianGraphCatalogue'
		)
	}

	planarQuadrangulationsCatalogue { :self |
		self.requireLibraryItem(
			'PlanarQuadrangulationsCatalogue'
		)
	}

	planarRegularGraphCatalogue { :self |
		self.requireLibraryItem(
			'PlanarRegularGraphCatalogue'
		)
	}

}

LibraryItem(
	name: 'PlanarRegularGraphCatalogue',
	category: 'Geometry/Graph',
	url: 'https://rohandrape.net/sw/hsc3-data/data/geometry/planar/PlanarRegularGraphCatalogue.json',
	mimeType: 'application/json',
	parser: { :libraryItem |
		libraryItem.collect { :each |
			PolygonMesh(
				each['vertexCoordinates'],
				each['faceIndices'] + 1
			)
		}
	}
)

LibraryItem(
	name: 'PlanarNonHamiltonianGraphCatalogue',
	category: 'Geometry/Graph',
	url: 'https://rohandrape.net/sw/hsc3-data/data/geometry/planar/PlanarNonHamiltonianGraphCatalogue.json',
	mimeType: 'application/json',
	parser: { :libraryItem |
		libraryItem.collect { :each |
			PolygonMesh(
				each['vertexCoordinates'],
				each['faceIndices'] + 1
			)
		}
	}
)

LibraryItem(
	name: 'PlanarConvexPolytopeGraphCatalogue',
	category: 'Geometry/Graph',
	url: 'https://rohandrape.net/sw/hsc3-data/data/geometry/planar/PlanarConvexPolytopeGraphCatalogue.json',
	mimeType: 'application/json',
	parser: { :libraryItem |
		libraryItem.collect { :each |
			PolygonMesh(
				each['vertexCoordinates'],
				each['faceIndices'] + 1
			)
		}
	}
)

LibraryItem(
	name: 'PlanarQuadrangulationsCatalogue',
	category: 'Geometry/Graph',
	url: 'https://rohandrape.net/sw/hsc3-data/data/geometry/planar/PlanarQuadrangulationsCatalogue.json',
	mimeType: 'application/json',
	parser: { :libraryItem |
		libraryItem.collect { :each |
			PolygonMesh(
				each['vertexCoordinates'],
				each['faceIndices'] + 1
			)
		}
	}
)
