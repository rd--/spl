/* LfPulse ; phase value = (0, 1), offset to lowest point */
LfPulse(110, 1 * 0.5, 0.5) * 0.1

/* LfPulse ; as envelope */
SinOsc(200, 0) * Lag(LfPulse(7.83, 0, 0.5) > 0, 0.05) * 0.2

/* LfPulse ; as envelope */
SinOsc(230, 0) * Lag(LfPulse(MouseX(2.3, 23, 1, 0.2), 0, 0.5).Max(0), 0.01) * 0.2

/* LfPulse ; 50 Hz wave */
LfPulse(50, 0, 0.5) * 0.05

/* LfPulse ; modulating frequency */
LfPulse(XLine(1, 200, 10), 0, 0.2) * 0.05

/* LfPulse ; amplitude modulation */
LfPulse(XLine(1, 200, 10), 0, 0.2) * SinOsc(440, 0) * 0.1

/* LfPulse ; used as both oscillator and lfo */
LfPulse(LfPulse(3, 0, 0.3) * 200 + 200, 0, 0.2) * 0.05

/* LfPulse ; humm */
let freqBass = 50;
let freq = 50;
let pan = 0;
let amp = 0.1;
let snd = LfPulse(freq, 0, 0.5);
20.timesRepeat {
	snd := MidEq(snd, ExpRand(300, 12000), 0.1, -20)
};
snd := Hpf(snd * 3, MouseX(5000, 7000, 1, 0.2));
snd := Lpf(snd, MouseY(9000, 11000, 1, 0.2));
snd := snd + SinOsc(freqBass, 0);
EqPan2(snd, pan) * amp

/* https://github.com/redFrik/udk08-Soft_and_Hard/tree/master/121220soft */
let p = { :freq :lo :hi |
	LfPulse(freq, 0, 0.5).LinLin(0, 1, lo, hi)
};
p(
	p(
		p(3, 4, 20),
		p(0.75, 50, 70),
		p(0.5, 100, 200)
	),
	-0.1,
	0.1
)
