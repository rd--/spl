/* http://earslap.com/weblog/music-release-laconicism.html */
let k = Duty(6.4, 0, Dseq(Infinity, [0.05, Drand(1, [0.04, 0.08])]));
Integrator((LfNoise0([5, 5, 5]) * k).RoundTo(k / 10), 1).Sin.Sqrt.Tanh.Splay2(0.3,1, 0, true)

/* http://earslap.com/weblog/music-release-laconicism.html ; wait */
let x = LfNoise1(0.5 * LocalIn(1, 0) + 0.1) * 0.5 + 0.5;
let s = PitchShift(
	PitchShift(
		Pulse([90, 90.01], x),
		10,
		x * 4,
		x,
		0
	),
	10,
	4 - (x * 4),
	1 - x,
	0
);
s <! LocalOut(x)

/* http://earslap.com/weblog/music-release-laconicism.html ; wait */
let q = [0 3 5 7 10];
let t = Impulse(4, 0) * LfNoise0(500) > 0;
let f = Demand(t, 0, Drand(Infinity, (q + 12 ++ q + 33).MidiCps));
PitchShift(Saw(f) * Decay(t, 3), 7, 2, 0, 0)

/* http://earslap.com/weblog/music-release-laconicism.html */
let snd = Hpf(
	Pulse([0.1 0.11; 0.12 0.13], 0.6) * 0.005,
	99
);
Hpf(
	MidEq(
		Limiter(GVerb(snd, [1 1.25; 1.5 2], 99, 0.5, 0.5, 15, 1, 0,7, 0.5) * 300, 1, 0.01) * 0.1,
		9000,
		0.9,
		9
	),
	200
).Sum.transposed.Mix

/* http://earslap.com/weblog/music-release-laconicism.html ; wait */
let f = LocalIn(2, 0).Tanh;
let k = Latch(f.first.Abs, Impulse(0.5, 0));
f <! LocalOut(f + AllpassN(Pulse([2, 3], k * 0.01 + 0.000001) * 0.9, 1, k * 0.3, 100 * k))

/* http://earslap.com/weblog/music-release-laconicism.html */
let a = Bpf(Saw([40, 40.001]), LfNoise0(128) + 1 * 4000 + 146, LfNoise1(1) + 1 * 0.05 + 0.01).Tanh;
CombC(a, 9, a.Abs.Lag(2) * 9, a.Abs.Lag(1) * 100)

/* http://earslap.com/weblog/music-release-laconicism.html */
let f = LocalIn(2, 0).Tanh;
let k = Latch(f.first.Abs, Impulse(1 / 4, 0));
f <! LocalOut(f + CombC(Blip([4, 6], 100 * k + 50) * 0.9, 1, k * 0.3, 50 * f))

/* http://earslap.com/weblog/music-release-laconicism.html */
let tr = Impulse(8, 0) * LfNoise1(2);
CombL(
	(Saw([3, 4]) * Decay(tr, 0.1)).Tanh,
	1,
	TRand(0, 0.01, tr).RoundTo(0.00015),
	TRand(-30, 30, tr)
)

/* http://earslap.com/weblog/music-release-laconicism.html */
let f = [60, 61];
let l = LfNoise0(6);
let o = (LfNoise0(4).Max(l).Max(SinOsc(f * (l * 9).Ceiling.Lag(0.1), 0) * 0.7));
(BBandPass(o, f, LfNoise0(1).Abs / 2) * 700 * l.Lag(1)).Tanh

/* http://earslap.com/weblog/music-release-laconicism.html */
let t = [0 0 0 1 5 7 10 12 12 12] + 30;
let a = Duty(1 / 8, 0, Drand(Infinity, t + 24 ++ t ++ t));
(BHiPass(LfNoise1(8) ^ 6, [a, a + 7].MidiCps, a / 3000) * (67 - a)).Tanh

/* http://earslap.com/weblog/music-release-laconicism.html */
AllpassL(SinOsc(55,0).Tanh, 0.4, TExpRand(0.0002, 0.4, Impulse(8, 0)).RoundTo([0.002, 0.004]), 2)

/* http://earslap.com/weblog/music-release-laconicism.html */
let i = { :freq | Impulse(freq, 0) };
let ph = Integrator(Integrator(i(64).Lag(LfNoise1([2, 2]) * 2 + 2) * 99, 0.9), 0.99).Fold2(1.pi);
SinOsc(LagUd(Impulse(2, 0), 0, 0.4) * 360, ph) / 3

/* http://earslap.com/weblog/music-release-laconicism.html */
let t = [0 3 5 7 10 12] + 40;
let p = Duty(1 / 4, 0, Drand(Infinity, (t + 12 ++ t).MidiCps));
let b = TRand(1500, 2000, Impulse(16, 0)).Lag(0.1);
Blip([b, b + p], 1).mean ^ 2

/* http://earslap.com/weblog/music-release-laconicism.html */
let i = Impulse(8, 0).Lag(0.3);
10.timesRepeat {
	i := LeakDc(AllpassC(i, 1, LfNoise0(8).LinLin(-1, 1, 0.00001, 0.2), -0.15) * LfNoise0(8).LinLin(-1, 1, 1, 3), 0.995).Tanh
};
i

/* http://earslap.com/weblog/music-release-laconicism.html */
let v = Blip([20000, 20000 - 9], 1) * (LfNoise0(16) * 0.5 + 0.5 ^ 9);
42.timesRepeat {
	v := LeakDc(AllpassC(v, 1, LfNoise0(5) * 0.05 + (0.05 + 0.001), 100), 0.995)
};
(v * 99).Tanh

/* http://earslap.com/weblog/music-release-laconicism.html */
(Hpf(LfNoise1(2), [10, 10.1]) * 100).Tanh

/* http://earslap.com/weblog/music-release-laconicism.html ; requires=kr */
let x = Duty(1 / 8, 0, Drand(Infinity, [0, Drand(1, [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])]));
LeakDc(Brf(Saw(8) * Decay2(x, 0.01, 0.3).kr ^ 1.5, x * 20 + [45.1, 45], 0.1), 0.995).Tanh

/* http://earslap.com/weblog/music-release-laconicism.html ; wait */
let v = Blip([60, 61], 5) * (LfNoise0(4).MulAdd(1, 0) ^ 8);
12.timesRepeat {
	v := LeakDc(CombC(v, 1, LfNoise0(1).MulAdd(0.05, 0.06).Lag(5000), 9), 0.995)
};
Limiter(v, 0.9, 1)

/* http://earslap.com/article/recreating-the-thx-deep-note.html ; 30 oscillators together, distributed across the stereo field */
let numVoices = 30;
let fundamentals = { Rand(200, 400) } ! numVoices;
fundamentals.collect { :freq |
	EqPan(
		Saw(freq),
		Rand(-1, 1)
	) * numVoices.reciprocal
}.Sum

/* http://earslap.com/article/recreating-the-thx-deep-note.html ; adding random wobbling to freqs, sorting randoms, lowpassing ; fundamentals are sorted, so higher frequencies drift more */
let numVoices = 30;
let fundamentals = system.randomReal(200, 400, [numVoices]).sorted;
fundamentals.withIndexCollect { :freq0 :index |
	let freq = freq0 + (LfNoise2(0.5) * 3 * index);
	EqPan(
		BLowPass(Saw(freq), freq * 5, 0.5),
		Rand(-1, 1)
	) * numVoices.reciprocal
}.Sum

/* http://earslap.com/article/recreating-the-thx-deep-note.html ; inverting init sort, louder bass, final volume envelope, some little tweaks ; requires=CurveGen */
let numVoices = 30;
let fundamentals = system.randomReal(200, 400, [numVoices]).sorted.reversed;
let finalPitches = (1:numVoices.collect { :each |
	(each / (numVoices / 6)).RoundTo(1) * 12
} + 14.5).MidiCps;
let outerEnv = CurveGen(1, [0 0.1 1], [8 4], [2 4]);
let ampEnvelope = CurveGen(1, [0 1 1 0], [3 21 3], [2 0 -4]);
let snd = 1:numVoices.collect { :numTone |
	let initRandomFreq = fundamentals[numTone] + (LfNoise2(0.5) * 6 * (numVoices - numTone));
	let destinationFreq = finalPitches[numTone] + (LfNoise2(0.1) * numTone / 3);
	let sweepEnv = CurveGen(1, [0, Rand(0.1, 0.2), 1], [Rand(5.5, 6), Rand(8.5, 9)], [Rand(2, 3), Rand(4, 5)]);
	let freq = ((1 - sweepEnv) * initRandomFreq) + (sweepEnv * destinationFreq);
	EqPan(
		BLowPass(Saw(freq), freq * 6, 0.6),
		Rand(-1, 1)
	) * (1 - (1 / numTone)) * 1.5 / numVoices
}.Sum;
Limiter(BLowPass(snd, 2000 + (outerEnv * 18000), 0.5) * (2 + outerEnv) * ampEnvelope, 1, 0.01)

/* http://earslap.com/article/sctweeting.html */
let a = LocalIn(1, 0);
let x = SinOsc((Decay(Impulse([4, 4.005], 0), 1000 * a.Abs) * 50), a).Distort;
x <! LocalOut(x.Sum)

/* http://earslap.com/article/sctweeting.html ; wait to start */
let f = LocalIn(2, 0).Tanh;
let k = Latch(f.first.Abs, Impulse(0.5, 0));
f <! LocalOut(f + AllpassN(Pulse([2, 3], k * 0.01 + 0.000001) * 0.9, 1, k * 0.3, 100 * k))

/* http://earslap.com/article/sctweeting.html ; requires=kr */
let f = LocalIn(2, 0).Tanh;
let k = Latch(f.first.Abs.A2K, Impulse(1 / 4, 0).kr);
f <! LocalOut(f + CombC(Blip([4, 6], 100 * k + 50) * 0.9, 1, k * 0.3, 50 * f))

/* http://earslap.com/article/sctweeting.html */
let f = LocalIn(2, 0).Tanh;
let k = Latch(f.first.Abs, Impulse(1 / 4, 0));
f <! LocalOut(f + CombC(Blip([4, 6], 100 * k + 50) * 0.9, 1, k * 0.3, 50 * f))

/* http://earslap.com/article/sctweeting.html */
{
	let a = LfNoise1(Rand(0, 0.2));
	DelayC(
		Bpf(
			WhiteNoise() * Dust2(a * a * 4 ^ 2).Lag(0.008),
			IRand(0, 10000) + 300,
			0.09
		),
		3,
		a * 1.5 + 1.5
	) * 45
} !+ 80

/* http://earslap.com/article/sctweeting.html */
AllpassC(
	SinOsc(55, 0).Tanh,
	0.4,
	TExpRand(0.0002, 0.4, Impulse(8, 0)).RoundTo([0.002, 0.0004]),
	2
)
