/* https://scsynth.org/t/insect-ambience/94/ (dcm) ; backgrdhiss ; requires=kr */
{
	Hpf(Crackle(1.96), 900) * Vibrato(50.0, 1, 0.02, 0, 0, 0.04, 0.1, 0, 0).kr * 0.0001
} ! 2

/* bugs */
let amp = 1.2; let ampLagTime = 20; let cutFreq = 600; let nse = 0.1; let nFreq = 800;
let src = { Lpf(PinkNoise() * nse, nFreq) * SinOsc(1750, 0) * SinOsc(160, 0) } ! 2;
let bugs = PitchShift(src, 0.2, LinLin(LfTri(20, 0), -1, 1, 0.9, 5), 0.01, 0.0001);
Hpf(Limiter(bugs, 0.9, 0.01) * Line(0, amp, ampLagTime), cutFreq)

/* frog */
Blip([10.001,10], 200) * LfPulse(0.1, 0, 0.5) * 0.02

/* thestart (modified) */
let amp = 0.2; let ampLagTime = 5; let freq = 1000; let pos = 1; let posLagTime = 30;
let src = {
	Hpf(
		Bpf(PinkNoise() * Line(0, amp, ampLagTime), [450, 250, 150], 0.2),
		LinLin(LfNoise2([0.1, 0.11, 0.14]), -1, 1, 100,800)
	)
} !+ 3;
XFade2(src.Splay, Lpf(src, freq), Line(0, pos, posLagTime), 1)

/* StaticLoss ; requries=WaveLoss */
let amp = 1; let freq = 1200; let rq = 1; let db = -45; let gate = 1; let pan = -0.5;
let src = WaveLoss(PinkNoise() * 0.1, 39, 40, 1);
Pan2(BPeakEq(src, freq, rq, db), pan, amp)

/* Storm ; very slow fade-in */
let amp = 0.2; let ampLagTime = 102; let cut = 165; let hicut = 440;
let src = { Rlpf(Crackle(1.9 + 0.05.Rand0), cut, 1) } !+ 6;
src = Hpf(LeakDc(Decay2(src, 0.01, 0.1), 0.995), hicut);
Pan2(Limiter(src, 0.9, 0.1), 0, Line(0, amp, ampLagTime))

/* Rumble ; very slow fade-in */
let amp = 0.2; let lagTime = 120; let freq = 240; let frq = 490;
let src = LeakDc(Decay2(ClipNoise() * 0.2, 0.01, 0.1), 0.995);
Pan2(LeakDc(Lpf(Rlpf(src, freq, 1), frq), 0.995), 0, Line(0, amp, lagTime))
